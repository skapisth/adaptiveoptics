% Code for plotting the retinal location data and fixation behavior provided by:

% Reiniger JL, Domdei N, Holz FG, Harmening WM (2021)
% 'Human gaze is precisely aligned with the foveolar cone topography of both eyes'
% bioRxiv, March 21, 2021, https://doi.org/10.1101/2021.03.19.436115

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Adapted by SJ April 2022
%Originally based on the script Plot_data_Reiniger_etal_2021. 


%%% The purpose of this script is to load the reiniger data from the
%%% downloaded repo and convert transfer the data into a more usable form
%%% in the .mat file named ReinigerData_subject information

% as a result you get the CDC, PRL and PCD infromation along with the cone
% locations (cone_data (X is first y is second (as noted in their repo)))

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Reference Image
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%

clear;clc;%close all;
imagepath = 'C:\Users\sjenks\Desktop\research\rochester\AO\adaptiveoptics\ConeDesity\ReinigerConeDensity\raw\FovealMosaicImages\';
imageList = dir(imagepath);
pname_1 = 'C:\Users\sjenks\Desktop\research\rochester\AO\adaptiveoptics\ConeDesity\ReinigerConeDensity\raw\Data_txt_files\';
txtFileList = dir(pname_1);

imageList = imageList(3:end);
txtFileList_PRL = txtFileList(3:2:end);
txtFileList_coneLoc = txtFileList(4:2:end);


for iEye = 1: length(imageList) 
clearvars -except imagepath imageList pname_1 txtFileList imageList txtFileList_PRL txtFileList_coneLoc iEye

imagename = imageList(iEye).name;
fname_1 = txtFileList_coneLoc(iEye).name;
fname_2 = txtFileList_PRL(iEye).name;

%[imagename,imagepath] = uigetfile('*.tif','Select a reference image.');

refImage = imread([imagepath imagename]);

figure, imshow(refImage); hold on;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Cone Locations
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%[fname_1,pname_1] = uigetfile('*.txt','Select .txt file containing cone locations.', imagepath);

    fid = fopen([pname_1 fname_1],'r');%
    cone_data = textscan(fid, '%f %f', 'HeaderLines',1);
    cone_data = [cone_data{1} cone_data{2}];
    fclose(fid);
    if size(cone_data,1)>0 % do not plot the empty cone location files. 
        plot(cone_data(:,1), cone_data(:,2),'.','MarkerEdgeColor','y','MarkerFaceColor','y','MarkerSize',3); hold on;
%         [pathstr, name, ext] = fileparts(fname{filenum});
%         text(mean(xy_data(2,:)), mean(xy_data(1,:)),name,'Interpreter','none');
    end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Retinal Locations and Fixation Behavior
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%[fname_2,pname_2] = uigetfile('*.txt','Select .txt file containing retinal locations and fixation ellipse.', pname_1);

    fid = fopen([pname_1 fname_2],'r');%
    CDC_PRL_data = textscan(fid, '%f %f %f %f %f %f %f %f %f', 'HeaderLines',1);
    fclose(fid);
        
    if isempty(CDC_PRL_data) == 0
        CDC_x = CDC_PRL_data{1};
        CDC_y = CDC_PRL_data{2};
        PCD_x = CDC_PRL_data{3};
        PCD_y = CDC_PRL_data{4};
        PRL_x = CDC_PRL_data{5};
        PRL_y = CDC_PRL_data{6};
        
        % plot retinal locations ans fixational data
        plot(CDC_x, CDC_y,'o','MarkerEdgeColor',[0.9 0 0.1],'MarkerFaceColor',[1 1 1],'Linewidth',3,'MarkerSize',8); hold on;
        plot(PCD_x, PCD_y,'d','MarkerEdgeColor',[0.1 0.1 0.1],'MarkerFaceColor','none','Linewidth',2,'MarkerSize',8); hold on;
        plot(PRL_x, PRL_y,'s','MarkerEdgeColor',[0 0 0],'MarkerFaceColor','none','Linewidth',2,'MarkerSize',8); hold on;
    end
    cd ('C:\Users\sjenks\Desktop\research\rochester\AO\adaptiveoptics\ConeDesity\ReinigerConeDensity\ConeLocations\');
    save(sprintf('Reingerdata_%s.mat',imagename(1:end -4)))
% close all;
end

            
            
            
            
            