
%%
clear;clc;close all

%%
areaPath = 'C:\Users\ruccilab\Documents\AO\coneDensity\ReinigerConeDensity\AvgConesPerPix\';
areaList = dir(areaPath);
areaList = areaList(3:end-2);

locPath = 'C:\Users\ruccilab\Documents\AO\CNN-Cone-Detection\Data_txt_files\';
locList = dir(locPath);
locList = locList(3:2:end);
indivPlots = 0;

pixPerdeg = 600;


for nfiles = 1:length(areaList)-2
    if nfiles > 1
        clear avgAreaPerCone CDC_PRL_data
    end
    load([areaPath areaList(nfiles).name])
    avgAreaPerCone_all{nfiles} = avgAreaPerCone;

    fid = fopen([locPath locList(nfiles).name],'r');%
    CDC_PRL_data = textscan(fid, '%f %f %f %f %f %f %f %f %f', 'HeaderLines',1);
    fclose(fid);

    if isempty(CDC_PRL_data) == 0
        CDC_x_all(nfiles) = CDC_PRL_data{1};
        CDC_y_all(nfiles) = CDC_PRL_data{2};
        PCD_x_all(nfiles) = CDC_PRL_data{3};
        PCD_y_all(nfiles) = CDC_PRL_data{4};
        PRL_x_all(nfiles) = CDC_PRL_data{5};
        PRL_y_all(nfiles) = CDC_PRL_data{6};
    end

    %temp(find(temp > 1.3)) = 0;

    CD_conePerPixsq{nfiles} = 1./avgAreaPerCone;
    CD_conePerDegsq{nfiles} = CD_conePerPixsq{nfiles} .* (pixPerdeg).^2;
    CD_conePerDegsq = flipud(CD_conePerDegsq);


    %remove inf and nan
    trimCD_CpDeg_all{nfiles} = CD_conePerDegsq{nfiles};


    PCD_valGT(nfiles) = CD_conePerDegsq{nfiles}(PCD_x_all(nfiles), PCD_y_all(nfiles));



end

%%
if indivPlots
    load('C:\Users\ruccilab\Documents\AO\coneDensity\ReinigerConeDensity\m_RenigierColorMap_Sam.mat')
    set(gcf, 'Colormap', m_RenigierColorMap_Sam)
    figure;
    for nfiles = 1:length(areaList)-2
     subplot(7,6,nfiles+1)

        % eventuallly trim the frame before ploting.

        set(gcf, 'Colormap', m_RenigierColorMap_Sam)
        imagesc(CD_conePerDegsq{nfiles})
        colorbar
        caxis([3000 17000])
        xlim([142 580])
        xlabel('Pixels')
        ylabel('Pixels')
        ylim([112 590])
        title('Cone Density (cones/deg^2)')
        %legend(sprintf('%s',areaList(nfiles).name))


        %         %remove inf and nan
        %         trimCD_CpDeg = trimCD_CpDeg_all{nfiles};
        %         trimCD_CpDeg(isinf( trimCD_CpDeg)) = 0;
        %         trimCD_CpDeg(isnan( trimCD_CpDeg)) = 0;
        %
        %         figure;
        %         %plot cone density across eccentricity
        %         plot(trimCD_CpDeg);% plot the data per pixel.

    end
end

figure;
for nfiles = 1:length(areaList)-2

    %remove inf and nan
    trimCD_CpDeg = trimCD_CpDeg_all{nfiles};
    trimCD_CpDeg(isinf( trimCD_CpDeg)) = 0;
    trimCD_CpDeg(isnan( trimCD_CpDeg)) = 0;

    %getting the maximum cones to compare to reinigers peak cone densitys
    [PCD_reiniger(nfiles), index_PCD_reiniger(nfiles)] = max(max(trimCD_CpDeg));
    [PCD_idxRow(nfiles), PCD_idxCol(nfiles)] = find(trimCD_CpDeg == PCD_reiniger(nfiles));

    PCD_x = PCD_x_all(nfiles);
    PCD_y = PCD_y_all(nfiles);

    %trim the edges of the matrix
    xPCD_row = trimCD_CpDeg(PCD_x,:);
    yPCD_col = trimCD_CpDeg(:,PCD_y);
    %convert the pixels to degrees (base off image)
    pixX_trimed = [1:length(xPCD_row)];
    degX_trimed = pixX_trimed/pixPerdeg;
    pixY_trimed = [1:length(yPCD_col)];
    degY_trimed = pixX_trimed/pixPerdeg;

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% PCD
    %center the data on peak cone density
    PCD_centerDeg_x = degX_trimed - (PCD_x/pixPerdeg);
    PCD_centerDeg_y = degY_trimed - (PCD_y/pixPerdeg);

    subplot(3,1,1)
    %Plot Cone density around peak cone density
    hold on;
    plot(PCD_centerDeg_y, xPCD_row, 'r'); %horizontal meridian
    hold on;
    plot(PCD_centerDeg_y,yPCD_col, 'g'); %vertical meridian
    title('Cone Density centered around PCD')
    xlabel('Degrees Eccentricity')
    ylabel('Cone Density (cones/degree^2)')
    ylim([6000, 18000])


    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% CDC
    CDC_x = CDC_x_all(nfiles);
    CDC_y = CDC_y_all(nfiles);

    CDC_centerDeg_x = degX_trimed - (CDC_x/pixPerdeg);
    CDC_centerDeg_y = degY_trimed - (CDC_y/pixPerdeg);

    subplot(3,1,2)
    %Plot Cone density around peak cone density
    plot(CDC_centerDeg_y, xPCD_row, 'r'); %horizontal meridian
    hold on;
    plot(CDC_centerDeg_x,yPCD_col, 'g'); %vertical meridian
    title('Cone Density centered around the CDC')
    xlabel('Degrees Eccentricity')
    ylabel('Cone Density (cones/degree^2)')
    ylim([6000, 18000])

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% PRL
    PRL_x = PRL_x_all(nfiles);
    PRL_y = PRL_y_all(nfiles);
    PRL_centerDeg_x = degX_trimed - (PRL_x/pixPerdeg);
    PRL_centerDeg_y = degY_trimed - (PRL_y/pixPerdeg);

    %Plot Cone density around peak cone density
    subplot(3,1,3)
    plot(PRL_centerDeg_y, xPCD_row, 'r'); %horizontal meridian
    hold on;
    plot(PRL_centerDeg_x,yPCD_col, 'g'); %vertical meridian
    title('Cone Density centered around the PRL')
    xlabel('Degrees Eccentricity')
    ylabel('Cone Density (cones/degree^2)')
    ylim([6000, 18000])
end


%% checking the indices of the peak cone density.

figure;
cmap = hsv(length(areaList));

subplot(1,3,1)
scatter(PCD_reiniger,PCD_valGT, 25);%,cmap, 'filled');% (our calc, groundtruth from reiniger)
refline(1,0)
xlabel('Our Peak Cone Density')
ylabel('Reiniger Peak Cone Density')


subplot(1,3,2)
scatter(PCD_idxRow,PCD_x_all, 25);%, cmap,'filled');hold on; % (our calc, groundtruth from reiniger)

xlabel('Our row index for PCD ')
ylabel('Reiniger row index for PCD')
xlim([300 500])
ylim([300 500])
refline(1,0)

% figure;
subplot(1,3,3)
scatter(PCD_idxCol,PCD_y_all, 25);%, cmap,'filled');hold on; % (our calc, groundtruth from reiniger)
xlabel('Our row index for PCD ')
ylabel('Reiniger row index for PCD')
xlim([300 500])
ylim([300 500])
refline(1,0)
% legend(sprintf('%i', n))


offcount = [2 10 12 29 33 36 37 39];

figure;
diff_CD = PCD_valGT - PCD_reiniger; %Reiniger - our data
histogram(diff_CD);
xlabel('Peak Cone densitiy difference (Reinger - Our data)')
ylabel('Frequency ( number of subjects)')
title('Peak cone density difference')

figure;
subplot(1,2,1)
diff_CD = PCD_x_all - PCD_idxRow; %Reiniger - our data
histogram(diff_CD);
xlabel('PCD index Horizontal difference (Reinger - Our data)')
ylabel('Frequency ( number of subjects)')
title('Peak cone density difference')

subplot(1,2,2)
diff_CD = PCD_y_all - PCD_idxCol; %Reiniger - our data
histogram(diff_CD);
xlabel('PCD index Vertical difference (Reinger - Our data)')
ylabel('Frequency ( number of subjects)')
title('Peak cone density difference')


% %
% % %% Plot an image of our data
% % img_path = 'C:\Users\sjenks\Desktop\research\rochester\AO\AplabData\';
% % tif_img = strcat(img_path,'R_5_recenter_V007_trim119-241.tiff');
% %
% % tif_read_img = imread(tif_img);
% %
% % imshow(tif_img)
% % figure;
% % imshow(tif_img)
% % hold on
% % for eachC_algo = 1:length(CNNPos)
% %     plot(CNNPos(eachC_algo,1),CNNPos(eachC_algo,2), 'r+', 'MarkerSize', 3, 'LineWidth', 2);
% %
% % end

