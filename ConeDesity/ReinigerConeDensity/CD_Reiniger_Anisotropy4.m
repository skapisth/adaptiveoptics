

%%
clear;clc;close all;

%%% if you match the PCD to the value to the supplementary, we are
%%% getting a really close PCD, but the orientation of the figure must
%%% be off because the indexing is not matching
onlyRight = 0;

plotIndiv = 0;

arc20 = 0; %change the two for loops

pixPerdeg = 600;
CDC_Percentile = 20;
% areaPath = 'C:\Users\sjenks\Desktop\research\rochester\AO\adaptiveoptics\ConeDesity\ReinigerConeDensity\AvgConesPerPix\712x712\';
% dataPath = 'C:\Users\sjenks\Desktop\research\rochester\AO\adaptiveoptics\ConeDesity\ReinigerConeDensity\ConeLocations\';
areaPath = 'C:\Users\ruccilab\Documents\adaptiveoptics\ConeDesity\ReinigerConeDensity\AvgConesPerPix\712x712\';
dataPath = 'C:\Users\ruccilab\Documents\adaptiveoptics\ConeDesity\ReinigerConeDensity\ConeLocations\';
if onlyRight
    areaList_OG = dir(areaPath);
    areaList = areaList_OG(4:2:end-2);
    areaList(end+1) = areaList_OG(end-2);
    
    dataList_OG = dir(dataPath);
    dataList = dataList_OG(4:2:end-1);
    dataList(end+1) = dataList_OG(end);
    
else
    areaList = dir(areaPath);
    areaList = areaList(3:end-2);
    
    dataList = dir(dataPath);
    dataList = dataList(3:end);
    
    arc20Eyes = [9 11 21 27 29 33 35 36];
end

%%%%%%%%%% Looping subjects to get Global Variables %%%%%%%%%%%%%%%
% for nf = 1:length(arc20Eyes)
%     nfiles = arc20Eyes(nf);
for nfiles = 1:length(areaList)
    

    
    %%% Loading data
    if nfiles > 1
        clear avgAreaPerCone CDC_PRL_data
    end
    load([areaPath areaList(nfiles).name])
    load([dataPath dataList(nfiles).name])
    refImage_all{nfiles} = refImage;
    avgAreaPerCone_all{nfiles} = avgAreaPerCone;%superior retina is at zero
    
    
    
    if isempty(CDC_PRL_data) == 0
        CDC_x_all(nfiles) = CDC_PRL_data{1};
        CDC_y_all(nfiles) = CDC_PRL_data{2};
        PCD_x_all(nfiles) = CDC_PRL_data{3};
        PCD_y_all(nfiles) = CDC_PRL_data{4};
        PRL_x_all(nfiles) = CDC_PRL_data{5};
        PRL_y_all(nfiles) = CDC_PRL_data{6};
        CD_conePerPixsq2{nfiles} = 1./avgAreaPerCone_all{nfiles};
        CD_conePerDegsq2{nfiles} = CD_conePerPixsq2{nfiles} .* (pixPerdeg).^2;
        CD_nyqAll{nfiles} = ((1/2)*sqrt((2/sqrt(3)).*CD_conePerDegsq2{nfiles}));
        PCD_valGT(nfiles) = CD_conePerDegsq2{nfiles}(PCD_y_all(nfiles), PCD_x_all(nfiles));
        
        
        
        %getting the maximum cones to compare to reinigers peak cone densitys
        [PCD_ours(nfiles), index_PCD_ours(nfiles)] = max(max(CD_conePerDegsq2{nfiles}));
        [PCD_idxRow(nfiles), PCD_idxCol(nfiles)] = find(CD_conePerDegsq2{nfiles} == PCD_ours(nfiles));
        oursPCD_x = PCD_idxCol(nfiles);
        oursPCD_y = PCD_idxRow(nfiles) ;
        
        PCD_X = PCD_x_all(nfiles);
        PCD_Y = PCD_y_all(nfiles);
        CDC_X = CDC_x_all(nfiles);
        CDC_Y = CDC_y_all(nfiles);
        PRL_X = PRL_x_all(nfiles);
        PRL_Y = PRL_y_all(nfiles);
        
        if plotIndiv
            
            figure;
            image(refImage);hold on;
            OverlayImage = imagesc(CD_conePerDegsq2{nfiles});hold on;
            alpha(0.3);colorbar;hold on;
            
            plot(CDC_X,CDC_Y,'.r', 'MarkerSize', 5); hold on;% I flipped this because the point was the max max of the matrix, thus needed to be rotated to be ontop of an image
            plot(PCD_X,PCD_Y,'^k', 'MarkerSize', 5); hold on;
            plot(PRL_X,PRL_Y,'sk', 'MarkerSize', 5); hold on;
            plot(oursPCD_x,oursPCD_y,'c^', 'MarkerSize', 20); hold on;
        end
        
    end
    
    
    %%%% Setting up the axes for the horizontal vs vertical merdians
    %PCD
    hozrPCD_col_R = CD_conePerDegsq2{nfiles}(PCD_Y - 5: PCD_Y + 5,:);
    test(nfiles) = PCD_Y;
    vertPCD_row_R = CD_conePerDegsq2{nfiles}(:,PCD_X-5:PCD_X + 5)';
    avg_hozrPCD_col(nfiles,:) = mean(hozrPCD_col_R);
    avg_vertPCD_row(nfiles,:) = mean(vertPCD_row_R,1);
    
    %CDC
    hozrCDC_col_R = CD_conePerDegsq2{nfiles}(CDC_Y- 5 : CDC_Y + 5,:);
    vertCDC_row_R = CD_conePerDegsq2{nfiles}(:,CDC_X- 5 : CDC_X + 5)';
    avg_hozrCDC_col(nfiles,:) = mean(hozrCDC_col_R);
    avg_vertCDC_row(nfiles,:) = mean(vertCDC_row_R,1)';
    
    %PRL
    hozrPRL_col_R = CD_conePerDegsq2{nfiles}(PRL_Y- 5: PRL_Y + 5,:);
    vertPRL_row_R = CD_conePerDegsq2{nfiles}(:,PRL_X- 5 : PRL_X + 5)';
    avg_hozrPRL_col(nfiles,:) = mean(hozrPRL_col_R);
    avg_vertPRL_row(nfiles,:) = mean(vertPRL_row_R,1)';
    
    
    
    %%%% Calculating CDC from gray scale heatmap
    if plotIndiv
        figure;
        image(refImage);  hold on;
        plot(PCD_X,PCD_Y,'^k', 'MarkerSize', 5); hold on;
        plot(PCD_idxCol(nfiles),PCD_idxRow(nfiles), '^r', 'MarkerSize', 5)
    end
    
    gray_CD = mat2gray(CD_conePerDegsq2{nfiles});
    APC_Prctile = prctile(avgAreaPerCone_all{nfiles},CDC_Percentile,1);
    threshCDC = prctile(APC_Prctile,CDC_Percentile,2);
    
    APC = avgAreaPerCone_all{nfiles} ;
    APC(APC<threshCDC)=1;
    APC(APC>=threshCDC)=0;
    
    measurements = regionprops(APC, gray_CD, 'WeightedCentroid');
    oursCDC = measurements.WeightedCentroid;
    oursCDC_x(nfiles) = oursCDC(1);
    oursCDC_y(nfiles) = oursCDC(2);
    
    CDC_val_all(nfiles) = CD_conePerDegsq2{nfiles}(round(CDC_y_all(nfiles),0),round(CDC_x_all(nfiles),0));
    oursCDC_val_all(nfiles) = CD_conePerDegsq2{nfiles}(round(oursCDC_y(nfiles),0),round(oursCDC_x(nfiles),0));
    
    if plotIndiv
        figure;imshow(APC);hold on;
        plot(oursCDC_x(nfiles), oursCDC_y(nfiles), 'r*', 'LineWidth', 2, 'MarkerSize', 16);
    end
    
end
%%% confidence intervals for the HM and VM distribtions.
CI95_R = tinv([0.025 0.975], (length(areaList))-1);
PCD_CI = CI95_R(2) *(std(avg_vertPCD_row)/sqrt(size(avg_vertPCD_row,1)));
PRL_CI = CI95_R(2) *(std(avg_vertCDC_row)/sqrt(size(avg_vertCDC_row,1)));
CDC_CI = CI95_R(2) *(std(avg_vertPRL_row)/sqrt(size(avg_vertPRL_row,1)));


cmap = hsv(length(areaList));
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Viewing Cone Density Accuracy
%
% %%%%%%%%%%%%%%%%%%%%%%%
% figure;
% subplot(1,3,1)
% scatter(oursCDC_val_all,CDC_val_all)
% refline(1,0)
% xlabel('Our Cone Density Centroid')
% ylabel('Reiniger Cone Density Centroid')
%
% subplot(1,3,2)
% scatter(oursCDC_x,CDC_x_all)
% refline(1,0)
% xlabel('Our Cone Density Centroid')
% ylabel('Reiniger Cone Density Centroid')
%
% subplot(1,3,3)
% scatter(oursCDC_y,CDC_y_all)
% refline(1,0)
% xlabel('Our Cone Density Centroid')
% ylabel('Reiniger Cone Density Centroid')
%
% %%%%%%%%%%%%%%%%%%%%%%%
% figure;
% subplot(1,3,1)
% diff_CD = CDC_val_all - oursCDC_val_all; %Reiniger - our data
% histogram(diff_CD);
% xlabel('Cone Density Centroid difference (Reinger - Our data)')
% ylabel('Frequency ( number of subjects)')
% title('Cone density Centroid difference')
%
% subplot(1,3,2)
% diff_CD = CDC_x_all - oursCDC_x; %Reiniger - our data
% histogram(diff_CD);
% xlabel('CDC index Horizontal difference ')
% ylabel('Frequency (Number of subjects)')
% % title('Peak cone density difference (Reinger - Our data)')
%
% subplot(1,3,3)
% diff_CD = CDC_y_all - oursCDC_y; %Reiniger - our data
% histogram(diff_CD);
% xlabel('CDC index Vertical difference')
% ylabel('Frequency (Number of subjects)')
% sgtitle('Cone density Centroid difference (Reinger - Our data)')
%
% %%%%%%%%%%%%%%%%%%%%%%%
% figure;
% subplot(1,3,1)
% scatter(PCD_ours,PCD_valGT, 25);%,cmap, 'filled');% (our calc, groundtruth from reiniger)
% refline(1,0)
% xlabel('Our Peak Cone Density')
% ylabel('Reiniger Peak Cone Density')
%
% subplot(1,3,2)
% scatter(PCD_idxCol,PCD_x_all, 25);%, cmap,'filled');hold on; % (our calc, groundtruth from reiniger)
% xlabel('Our row index for PCD ')
% ylabel('Reiniger row index for PCD')
% xlim([300 500])
% ylim([300 500])
% refline(1,0)
%
% subplot(1,3,3)
% scatter( PCD_idxRow,PCD_y_all, 25);%, cmap,'filled');hold on; % (our calc, groundtruth from reiniger)
% xlabel('Our row index for PCD ')
% ylabel('Reiniger row index for PCD')
% xlim([300 500])
% ylim([300 500])
% refline(1,0)
%
% %%%%%%%%%%%%%%%%%%%%%%%
% figure;
% subplot(1,3,1)
% diff_CD_val = PCD_valGT - PCD_ours; %Reiniger - our data
% histogram(diff_CD_val);
% xlabel('Peak Cone densitiy difference (Reinger - Our data)')
% ylabel('Frequency ( number of subjects)')
% title('Peak cone density difference')
%
% subplot(1,3,2)
% diff_CDx = PCD_x_all - PCD_idxCol; %Reiniger - our data
% histogram(diff_CDx);
% xlabel('PCD index Horizontal difference ')
% ylabel('Frequency (Number of subjects)')
%
% subplot(1,3,3)
% diff_CDy = PCD_y_all - PCD_idxRow; %Reiniger - our data
% histogram(diff_CDy);
% xlabel('PCD index Vertical difference')
% ylabel('Frequency (Number of subjects)')
% sgtitle('Peak cone density difference (Reinger - Our data)')

%% get Cone density at 20 acrmin with an area of 2 arc min

% variables
targOffset = 0.25; % eccentricity from PRL in deg
area = 0.033; % how big you want to take the area (square)

ecc = round(targOffset * pixPerdeg,0); %eccentrictiy in pixels
box = round(area * pixPerdeg,0); % box area in pixels

warning("Reiniger Data has left and right eye, so left and right cannot be compared here")
%% Main forloop 
for nfiles = 1:length(areaList)
% for nf = 1:length(arc20Eyes)
%     nfiles = arc20Eyes(nf);
    
    PCD_X = PCD_x_all(nfiles);
    PCD_Y = PCD_y_all(nfiles);
    CDC_X = CDC_x_all(nfiles);
    CDC_Y = CDC_y_all(nfiles);
    PRL_X = PRL_x_all(nfiles);
    PRL_Y = PRL_y_all(nfiles);
    
    %location of boxes
    SuperiorCenter = PRL_Y - ecc; %superior retina is zero
    InferiorCenter=  PRL_Y + ecc;
    
    SuperiorCenter_CDC = CDC_Y - ecc; %superior retina is zero
    InferiorCenter_CDC =  CDC_Y + ecc;
    
    
    CD_conePerDegsq = CD_conePerDegsq2{nfiles};
    CD_nyq = CD_nyqAll{nfiles};
    if strcmp( areaList(nfiles).name(20),'R')
        NasalCenter =  PRL_X - ecc; %nasal retina is to the right and covers left visual field
        TempCenter = PRL_X + ecc;
        
        NasalCenter_CDC =  CDC_X - ecc; %nasal retina is to the right and covers left visual field
        TempCenter_CDC = CDC_X + ecc;
        
        text(110,(size(CD_conePerDegsq,1)/2)+10,'Temporal Retina')
        text(size(CD_conePerDegsq,1)-250,(size(CD_conePerDegsq,1)/2)+10,'Nasal Retina')
    else
        NasalCenter =  PRL_X + ecc;
        TempCenter = PRL_X - ecc;
        
        NasalCenter_CDC =  CDC_X + ecc;
        TempCenter_CDC = CDC_X - ecc;
        
        text(10,(size(CD_conePerDegsq,1)/2)-160,'Nasal Retina')
        text(size(CD_conePerDegsq,1)-200,(size(CD_conePerDegsq,1)/2)-60,'Temporal Retina')
    end
    %%%%%%%%%%%%%%%% PRL set up %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%% set up the box locations in ConesPerDeg
    superiorBox = CD_conePerDegsq(SuperiorCenter - box:SuperiorCenter + box,  PRL_X - box: PRL_X + box);
    superiorArea = mean(mean(superiorBox));
    
    inferiorBox = CD_conePerDegsq(InferiorCenter - box: InferiorCenter + box,  PRL_X - box: PRL_X + box);
    inferiorArea = mean(mean(inferiorBox));
    
    nasalBox = CD_conePerDegsq(PRL_Y - box: PRL_Y + box, NasalCenter- box : NasalCenter +box);
    nasalArea = mean(mean(nasalBox));
    
    temporalBox = CD_conePerDegsq(PRL_Y - box: PRL_Y + box,TempCenter- box : TempCenter + box);
    temporalArea = mean(mean(temporalBox));
    
    %black out where the box locations will be
    viewTargLoc = CD_conePerDegsq;
    viewTargLoc(SuperiorCenter - box: SuperiorCenter + box, PRL_X - box: PRL_X + box)=0;
    viewTargLoc(InferiorCenter - box: InferiorCenter + box,  PRL_X - box: PRL_X + box)=0;
    viewTargLoc(PRL_Y - box: PRL_Y + box, NasalCenter- box : NasalCenter +box)=0;
    viewTargLoc(PRL_Y - box: PRL_Y + box,TempCenter- box : TempCenter + box)=0;
    legend(sprintf('%s',areaList(nfiles).name))


    %%%%%% set up the box locations in NYQUIST
    superiorBox_nyq = CD_nyq(SuperiorCenter - box:SuperiorCenter + box,  PRL_X - box: PRL_X + box);
    superiorArea_nyq = mean(mean(superiorBox_nyq));
    
    inferiorBox_nyq = CD_nyq(InferiorCenter - box: InferiorCenter + box,  PRL_X - box: PRL_X + box);
    inferiorArea_nyq = mean(mean(inferiorBox_nyq));
    
    nasalBox_nyq = CD_nyq(PRL_Y - box: PRL_Y + box, NasalCenter- box : NasalCenter +box);
    nasalArea_nyq = mean(mean(nasalBox_nyq));
    
    temporalBox_nyq = CD_nyq(PRL_Y - box: PRL_Y + box,TempCenter- box : TempCenter + box);
    temporalArea_nyq = mean(mean(temporalBox_nyq));
    
    %black out where the box locations will be
    viewTargLoc_nyq = CD_nyq;
    viewTargLoc_nyq(SuperiorCenter - box: SuperiorCenter + box, PRL_X - box: PRL_X + box)=0;
    viewTargLoc_nyq(InferiorCenter - box: InferiorCenter + box,  PRL_X - box: PRL_X + box)=0;
    viewTargLoc_nyq(PRL_Y - box: PRL_Y + box, NasalCenter- box : NasalCenter +box)=0;
    viewTargLoc_nyq(PRL_Y - box: PRL_Y + box,TempCenter- box : TempCenter + box)=0;
    legend(sprintf('%s',areaList(nfiles).name))
    
    
    %%%%%%%%%%%%%%%% CDC set up %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%% set up the box locations in ConesPerDeg
    superiorBox_CDC = CD_conePerDegsq(SuperiorCenter_CDC - box:SuperiorCenter_CDC + box,  CDC_X - box: CDC_X + box);
    superiorArea_CDC = mean(mean(superiorBox_CDC));
    
    inferiorBox_CDC = CD_conePerDegsq(InferiorCenter_CDC - box: InferiorCenter_CDC + box,  CDC_X - box: CDC_X + box);
    inferiorArea_CDC = mean(mean(inferiorBox_CDC));
    
    nasalBox_CDC = CD_conePerDegsq(CDC_Y - box: CDC_Y + box, NasalCenter_CDC- box : NasalCenter_CDC +box);
    nasalArea_CDC = mean(mean(nasalBox_CDC));
    
    temporalBox_CDC = CD_conePerDegsq(CDC_Y - box: CDC_Y + box,TempCenter_CDC- box : TempCenter_CDC + box);
    temporalArea_CDC = mean(mean(temporalBox_CDC));
    
    %black out where the box locations will be
    viewTargLoc_CDC = CD_conePerDegsq;
    viewTargLoc_CDC(SuperiorCenter_CDC - box: SuperiorCenter_CDC + box, CDC_X - box: CDC_X + box)=0;
    viewTargLoc_CDC(InferiorCenter_CDC - box: InferiorCenter_CDC + box,  CDC_X - box: CDC_X + box)=0;
    viewTargLoc_CDC(CDC_Y - box: CDC_Y + box, NasalCenter_CDC- box : NasalCenter_CDC +box)=0;
    viewTargLoc_CDC(CDC_Y - box: CDC_Y + box,TempCenter_CDC- box : TempCenter_CDC + box)=0;
    legend(sprintf('%s',areaList(nfiles).name))

    %%%%%% set up the box locations in NYQUIST
    superiorBox_nyqCDC = CD_nyq(SuperiorCenter_CDC - box:SuperiorCenter_CDC + box,  CDC_X - box: CDC_X + box);
    superiorArea_nyqCDC = mean(mean(superiorBox_nyqCDC));
    
    inferiorBox_nyqCDC = CD_nyq(InferiorCenter_CDC - box: InferiorCenter_CDC + box,  CDC_X - box: CDC_X + box);
    inferiorArea_nyqCDC = mean(mean(inferiorBox_nyqCDC));
    
    nasalBox_nyqCDC = CD_nyq(CDC_Y - box: CDC_Y + box, NasalCenter_CDC- box : NasalCenter_CDC +box);
    nasalArea_nyqCDC = mean(mean(nasalBox_nyqCDC));
    
    temporalBox_nyqCDC = CD_nyq(CDC_Y - box: CDC_Y + box,TempCenter_CDC- box : TempCenter_CDC + box);
    temporalArea_nyqCDC = mean(mean(temporalBox_nyqCDC));
    
    %black out where the box locations will be
    viewTargLoc_NyqCDC = CD_nyq;
    viewTargLoc_NyqCDC(SuperiorCenter_CDC - box: SuperiorCenter_CDC + box, CDC_X - box: CDC_X + box)=0;
    viewTargLoc_NyqCDC(InferiorCenter_CDC - box: InferiorCenter_CDC + box,  CDC_X - box: CDC_X + box)=0;
    viewTargLoc_NyqCDC(CDC_Y - box: CDC_Y + box, NasalCenter_CDC- box : NasalCenter_CDC +box)=0;
    viewTargLoc_NyqCDC(CDC_Y - box: CDC_Y + box,TempCenter_CDC- box : TempCenter_CDC + box)=0;
    legend(sprintf('%s',areaList(nfiles).name))
    
    %%%%%%%%%%%%%%%% figures %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if plotIndiv
        % black out the target locations on the heatmap to verify their location.
        figure;
        %         subplot(1,2,1)
        imagesc(viewTargLoc); hold on;
        a=plot(PRL_X,PRL_Y, 'g.', 'MarkerSize', 25);hold on;
        %     b=plot(PCD_X,PCD_Y, 'r^');hold on;
        c=plot(CDC_X,CDC_Y, 'b*');hold on;
        xlim([100,600])
        ylim([150,600])
        
        text((size(CD_conePerDegsq,1)/2)-60,175,'Superior Retina')
        text((size(CD_conePerDegsq,1)/2)-60,size(CD_conePerDegsq,2)-140,'Inferior Retina Retina')
        text(110,(size(CD_conePerDegsq,1)/2)-30,'Temporal Retina')
        text(size(CD_conePerDegsq,1)-250,(size(CD_conePerDegsq,1)/2)-30,'Nasal Retina')
        legend([a,c],'Preferred Retinal Locus','Cone Density Centroid')
        axis off
        colorbar
        
        figure;
        %     subplot(1,2,2)
        imagesc(viewTargLoc_CDC); hold on;
        a=plot(PRL_X,PRL_Y, 'g.', 'MarkerSize', 25);hold on;
        b=plot(CDC_X,CDC_Y, 'b*');hold on;
        %     plot(PCD_X,PCD_Y, 'r^');hold on;
        xlim([100,600])
        ylim([150,600])
        
        text((size(CD_conePerDegsq,1)/2)-60,175,'Superior Retina')
        text((size(CD_conePerDegsq,1)/2)-60,size(CD_conePerDegsq,2)-140,'Inferior Retina Retina')
        text(110,(size(CD_conePerDegsq,1)/2)-30,'Temporal Retina')
        text(size(CD_conePerDegsq,1)-250,(size(CD_conePerDegsq,1)/2)-30,'Nasal Retina')
        legend([a,c],'Preferred Retinal Locus','Cone Density Centroid')
        axis off
        colorbar
        
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if arc20
        if any(arc20Eyes(:) == nfiles)
            rAreas(nf,:) = [superiorArea  inferiorArea  nasalArea temporalArea];
            rAreas_CDC(nf,:) = [superiorArea_CDC  inferiorArea_CDC  nasalArea_CDC temporalArea_CDC];
            rAreas_nyq(nf,:) = [superiorArea_nyq  inferiorArea_nyq  nasalArea_nyq temporalArea_nyq];
            rAreas_CDC(nf,:) = [superiorArea_nyqCDC  inferiorArea_nyqCDC  nasalArea_nyqCDC temporalArea_nyqCDC];
        end
    else
        rAreas(nfiles,:) = [superiorArea  inferiorArea  nasalArea temporalArea];
        rAreas_CDC(nfiles,:) = [superiorArea_CDC  inferiorArea_CDC  nasalArea_CDC temporalArea_CDC];
        rAreas_nyq(nfiles,:) = [superiorArea_nyq  inferiorArea_nyq  nasalArea_nyq temporalArea_nyq];
        rAreas_nyqCDC(nfiles,:) = [superiorArea_nyqCDC  inferiorArea_nyqCDC  nasalArea_nyqCDC temporalArea_nyqCDC];
    end
    % comparing normalized cone density to performance
    
end


%% Defining Variables for plots 

CI95 = tinv([0.025 0.975], length(rAreas)-1);
ste_N = (std(rAreas)/sqrt(length(rAreas)));
CI_XC = CI95(2) * ste_N;

ste_N_CDC = (std(rAreas_CDC)/sqrt(length(rAreas_CDC)));
CI_XC_CDC = CI95(2) * ste_N_CDC;

ste_N_nyq = (std(rAreas_nyq)/sqrt(length(rAreas_nyq)));
CI_XC_nyq = CI95(2) * ste_N_nyq;

ste_N_nyqCDC = (std(rAreas_nyqCDC)/sqrt(length(rAreas_nyqCDC)));
CI_XC_nyqCDC = CI95(2) * ste_N_nyqCDC;


avgHM_Reiniger = mean([rAreas(:,3) rAreas(:,4)],2);
avgVM_Reiniger = mean([rAreas(:,1) rAreas(:,2)],2);
avgHM_Reiniger_CDC = mean([rAreas_CDC(:,3) rAreas_CDC(:,4)],2);
avgVM_Reiniger_CDC = mean([rAreas_CDC(:,1) rAreas_CDC(:,2)],2);

avgHM_Reiniger_nyq = mean([rAreas_nyq(:,3) rAreas_nyq(:,4)],2);
avgVM_Reiniger_nyq = mean([rAreas_nyq(:,1) rAreas_nyq(:,2)],2);
avgHM_Reiniger_nyqCDC = mean([rAreas_nyqCDC(:,3) rAreas_nyqCDC(:,4)],2);
avgVM_Reiniger_nyqCDC = mean([rAreas_nyqCDC(:,1) rAreas_nyqCDC(:,2)],2);


ste_HM = (std(avgHM_Reiniger)/sqrt(length(avgHM_Reiniger)));
CI_XChm = CI95(2) * ste_HM;
ste_VM = (std(avgVM_Reiniger)/sqrt(length(avgVM_Reiniger)));
CI_XCvm = CI95(2) * ste_VM;

ste_HM_CDC = (std(avgHM_Reiniger_CDC)/sqrt(length(avgHM_Reiniger_CDC)));
CI_XChm_CDC = CI95(2) * ste_HM_CDC;
ste_VM_CDC = (std(avgVM_Reiniger_CDC)/sqrt(length(avgVM_Reiniger_CDC)));
CI_XCvm_CDC = CI95(2) * ste_VM_CDC;

ste_nyqHM = (std(avgHM_Reiniger_nyq)/sqrt(length(avgHM_Reiniger_nyq)));
CI_XChm = CI95(2) * ste_nyqHM;
ste_nyqVM = (std(avgVM_Reiniger_nyq)/sqrt(length(avgVM_Reiniger_nyq)));
CI_XCvm = CI95(2) * ste_nyqVM;

ste_HM_nyqCDC = (std(avgHM_Reiniger_nyqCDC)/sqrt(length(avgHM_Reiniger_nyqCDC)));
CI_XChm_nyqCDC = CI95(2) * ste_HM_nyqCDC;
ste_VM_nyqCDC = (std(avgVM_Reiniger_nyqCDC)/sqrt(length(avgVM_Reiniger_nyqCDC)));
CI_XCvm_nyqCDC = CI95(2) * ste_VM_nyqCDC;

avg_reiniger = mean(rAreas);
avg_reiniger_CDC = mean(rAreas_CDC);

avg_reiniger_nyq = mean(rAreas_nyq);
avg_reiniger_nyqCDC = mean(rAreas_nyqCDC);



%% FIGURES:  PRL with Performance
%%%% Behavioral Vars%%%%%%%%%%%%

load('C:\Users\ruccilab\Documents\adaptiveoptics\ConeDesity\APLab_System\data\FVP.mat');
% load('C:\Users\sjenks\Desktop\research\rochester\AO\adaptiveoptics\ConeDesity\FVP.mat')

CI95_F = tinv([0.025 0.975], length(FvP.data(1).avgPerf)-1);
FovPerf_XC = mean(FvP.data(1).avgPerf(:,1:2:8));
FovPerf_XC = [FovPerf_XC(3),FovPerf_XC(1),FovPerf_XC(2),FovPerf_XC(4)];
FovHVAPerf_XC = mean(FvP.data(1).perf_HVA);
FovHVACI_XC = mean(FvP.data(1).HVA_stePerf)*CI95_F(2);

ste_F = (std(FvP.data(1).avgPerf(:,1:2:8))/sqrt(length(FvP.data(1).avgPerf(:,1:2:8))));
CIXC_F = CI95_F(2) * ste_F;
CIXC_F = [CIXC_F(3), CIXC_F(1), CIXC_F(2), CIXC_F(4)];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


label = {'PRL','CDC' };
figure;
yyaxis left
p1 = errorbar([1,2], avg_reiniger(1:2),CI_XC(1:2),'.-', 'Color', [0 1 0]); hold on;
p3 = errorbar([1,2], avg_reiniger_CDC(1:2),CI_XC(1:2),'.-', 'Color', [0 0 1]); hold on;
xticks(1:2)
xlim([.75 2.25])
ylim([8000 12000])
ylabel('Cone density (cones/deg^2)')
yyaxis right
ylim(((1/2)*sqrt((2/sqrt(3)).*[8000 12000])))
ylabel('Nyquist (cycles/deg)')
xticklabels({'Superior/lowerVF', 'Inferior/UpperVF'})
legend([p1 p3 ],{sprintf('%s Centered', label{1}),sprintf('%s Centered', label{2})})
text(1,1.5,'Vertical Meridian')
ax = gca;
ax.YAxis(1).Color =  [0 .85 0];
ax.YAxis(2).Color = [0 .7 0.5];


figure;
label = {'PRL','CDC' };
yyaxis left
p1 = errorbar([1,2], [mean(avgHM_Reiniger) mean(avgVM_Reiniger)],[CI_XChm CI_XCvm],'.-', 'Color', [0 1 0]); hold on;
p3 = errorbar([1,2],[mean(avgHM_Reiniger_CDC) mean(avgVM_Reiniger_CDC)],[CI_XChm_CDC CI_XCvm_CDC],'.-', 'Color', [0 0 1]); hold on;
xticks(1:2)
xlim([.75 2.25])
ylim([8000 12000])
ylabel('Cone density (cones/deg^2)')
yyaxis right
ylim(((1/2)*sqrt((2/sqrt(3)).*[8000 12000])))
ylabel('Nyquist (cycles/deg)')
xticklabels({'Horizontal', 'Vertical'})
legend([p1 p3],{sprintf('%s Centered', label{1}),sprintf('%s Centered', label{2})})
ax = gca;
ax.YAxis(1).Color =  [0 .85 0];
ax.YAxis(2).Color = [0 .7 0.5];



%% Stats

locXcent = [rAreas; rAreas_CDC];

%Set this up correctly
[p,tableA,stats]=anova2(locXcent,length(rAreas));

sup_PRL = rAreas(:,1);
inf_PRL = rAreas(:,2);
HM_PRL = mean(rAreas(:,3:4),2);
VM_PRL = mean(rAreas(:,1:2),2);

sup_CDC = rAreas_CDC(:,1);
inf_CDC = rAreas_CDC(:,2);
HM_CDC = mean(rAreas_CDC(:,3:4),2);
VM_CDC = mean(rAreas_CDC(:,1:2),2);

HVA_array = [HM_CDC VM_CDC;HM_PRL, VM_PRL];

VMA_array = [sup_CDC inf_CDC;sup_PRL, inf_PRL];


[p_CDhva,tableA_CDhva,stats_CDhva]=anova2(HVA_array,length(HM_CDC));

[p_CDvma,tableA_CDvma,stats_CDvma]=anova2(VMA_array,length(sup_CDC));


sprintf('Data analysis only for Right eye = %i',onlyRight)

[hvaCDC_h, hvaCDC_p, hvaCDC_ci,hvaCDC_STATS] = ttest(HM_CDC, VM_CDC)
[hvaPRL_h, hvaPRL_p, hvaPRL_ci,hvaPRL_STATS] = ttest(HM_PRL, VM_PRL)

[vmaCDC_h, vmaCDC_p, vmaCDC_ci,vmaCDC_STATS] = ttest(sup_CDC, inf_CDC)
[vmaPRL_h, vmaPRL_p, vmaPRL_ci,vmaPRL_STATS] = ttest(sup_PRL, inf_PRL)


%% Plot Distributions along the Verticle meridian 

%%% setting up the plots 
ArraySize = 1001;
nFront = 100;
centerPT = ceil(ArraySize/2);

vert_rowAxis = (1:size(refImage,1))/pixPerdeg;
horz_colAxis =(1:size(refImage,2))/pixPerdeg;

axisR_vertPRL= NaN(length(areaList),ArraySize);
axisR_horzPRL = NaN(length(areaList),ArraySize);
axisR_vertPCD = NaN(length(areaList),ArraySize);
axisR_horzPCD = NaN(length(areaList),ArraySize);
axisR_vertCDC = NaN(length(areaList),ArraySize);
axisR_horzCDC = NaN(length(areaList),ArraySize);

valR_vertPRL= NaN(length(areaList),ArraySize);
valR_horzPRL = NaN(length(areaList),ArraySize);
valR_vertPCD = NaN(length(areaList),ArraySize);
valR_horzPCD = NaN(length(areaList),ArraySize);
valR_vertCDC = NaN(length(areaList),ArraySize);
valR_horzCDC = NaN(length(areaList),ArraySize);



for nfiles = 1:length(areaList)
    
    norm_vertPRL_rowAxis(:,nfiles) = (vert_rowAxis' - (floor(PRL_y_all(nfiles))/pixPerdeg))*60;
    axisR_vertPRL(nfiles,(centerPT - PRL_y_all(nfiles))+1 : centerPT+(712 - PRL_y_all(nfiles))) = norm_vertPRL_rowAxis(:,nfiles);
    valR_vertPRL(nfiles,(centerPT - PRL_y_all(nfiles))+1 : centerPT+(712 - PRL_y_all(nfiles)))  = avg_vertPRL_row(nfiles,:);
    
    norm_horzPRL_colAxis(:,nfiles) = (horz_colAxis' - (floor(PRL_x_all(nfiles))/pixPerdeg))*60;
    axisR_horzPRL(nfiles,(centerPT - PRL_x_all(nfiles))+1 : centerPT+(712 - PRL_x_all(nfiles))) = norm_horzPRL_colAxis(:,nfiles);
    valR_horzPRL(nfiles,(centerPT - PRL_x_all(nfiles))+1 : centerPT+(712 - PRL_x_all(nfiles))) = avg_hozrPRL_col(nfiles,:);

    norm_vertPCD_rowAxis(:,nfiles) = (vert_rowAxis' - (floor(PCD_y_all(nfiles))/pixPerdeg))*60;%x
    axisR_vertPCD(nfiles,(centerPT - PCD_y_all(nfiles))+1 : centerPT+(712 - PCD_y_all(nfiles))) = norm_vertPCD_rowAxis(:,nfiles);
    valR_vertPCD(nfiles,(centerPT - PCD_y_all(nfiles))+1 : centerPT+(712 - PCD_y_all(nfiles))) = avg_vertPCD_row(nfiles,:);
    
    norm_horzPCD_colAxis(:,nfiles) = (horz_colAxis' - (floor(PCD_x_all(nfiles))/pixPerdeg))*60;
    axisR_horzPCD(nfiles,(centerPT - PCD_x_all(nfiles))+1 : centerPT+(712 - PCD_x_all(nfiles))) = norm_horzPCD_colAxis(:,nfiles);
    valR_horzPCD(nfiles,(centerPT - PCD_x_all(nfiles))+1 : centerPT+(712 - PCD_x_all(nfiles))) = avg_hozrPCD_col(nfiles,:);
    
    norm_vertCDC_rowAxis(:,nfiles) = (vert_rowAxis' - (floor(CDC_y_all(nfiles))/pixPerdeg))*60;
    axisR_vertCDC(nfiles,(centerPT - CDC_y_all(nfiles))+1 : centerPT+(712 - CDC_y_all(nfiles))) = norm_vertCDC_rowAxis(:,nfiles);
    valR_vertCDC(nfiles,(centerPT - CDC_y_all(nfiles))+1 : centerPT+(712 - CDC_y_all(nfiles))) = avg_vertCDC_row(nfiles,:);

%     norm_vertCDC_rowAxis(:,nfiles) = (vert_rowAxis' - (floor(oursCDC_y(nfiles))/pixPerdeg))*60;
%     axisR_vertCDC(nfiles,(centerPT - oursCDC_y(nfiles))+1 : centerPT+(712 - oursCDC_y(nfiles))) = norm_vertCDC_rowAxis(:,nfiles);
%     valR_vertCDC(nfiles,(centerPT - oursCDC_y(nfiles))+1 : centerPT+(712 - oursCDC_y(nfiles))) = avg_vertCDC_row(nfiles,:);

    
    
    norm_horzCDC_colAxis(:,nfiles) = (horz_colAxis' - (floor(CDC_x_all(nfiles))/pixPerdeg))*60;
    axisR_horzCDC(nfiles,(centerPT - CDC_x_all(nfiles))+1 : centerPT+(712 - CDC_x_all(nfiles))) = norm_horzCDC_colAxis(:,nfiles);
    valR_horzCDC(nfiles,(centerPT - CDC_x_all(nfiles))+1 : centerPT+(712 - CDC_x_all(nfiles))) = avg_hozrCDC_col(nfiles,:);
    
     
end


% plot(nanmean(axisR_vertPRL),avg_vertCDC_row

CI95_R = tinv([0.025 0.975], (length(areaList))-1);
PCD_CI = CI95_R(2) *(std(valR_vertPCD)/sqrt(size(valR_vertPCD,1)));
PRL_CI = CI95_R(2) *(std(valR_vertPRL)/sqrt(size(valR_vertPRL,1)));
CDC_CI = CI95_R(2) *(std(valR_vertCDC)/sqrt(size(valR_vertCDC,1)));

%%% Plotting the Vertical meridian distribution 

figure;
a = plot(nanmean(axisR_vertPRL),nanmean(valR_vertPRL),'g');hold on;
plot(nanmean(axisR_vertPRL),nanmean(valR_vertPRL)+PRL_CI, 'g-.');hold on;
plot(nanmean(axisR_vertPRL),nanmean(valR_vertPRL)-PRL_CI, 'g-.');hold on;

b = plot(nanmean(axisR_vertPCD),nanmean(valR_vertPCD),'k');hold on;
plot(nanmean(axisR_vertPCD),nanmean(valR_vertPCD) + PCD_CI, 'k-.');hold on;
plot(nanmean(axisR_vertPCD),nanmean(valR_vertPCD) - PCD_CI,'k-.');hold on;


c = plot(nanmean(axisR_vertCDC),nanmean(valR_vertCDC),'b');hold on;
plot(nanmean(axisR_vertCDC),nanmean(valR_vertCDC)+CDC_CI, 'b-.');hold on;
plot(nanmean(axisR_vertCDC),nanmean(valR_vertCDC)-CDC_CI, 'b-.');hold on;

xlim([-20, 20])
ylim([6000, 18000])
legend([a ,b, c], 'PRL', 'PCD', 'CDC')
yyaxis right
ylim(((1/2)*sqrt((2/sqrt(3)).*[6000, 18000])))
ylabel('Nyquist (cycles/deg)')
ax = gca;
ax.YAxis(2).Color = [0 .7 0.5];



figure;
a = plot(nanmean(axisR_vertPRL),nanmean(valR_vertPRL),'g');hold on;
plot(nanmean(axisR_vertPRL),nanmean(valR_vertPRL)+PRL_CI, 'g-.');hold on;
plot(nanmean(axisR_vertPRL),nanmean(valR_vertPRL)-PRL_CI, 'g-.');hold on;

xlim([-20, 20])
ylim([6000, 18000])
xlabel('Eccentricity wrt PRL (arc)')
ylabel('Cone density (cones/deg^2)')
yyaxis right
ylim(((1/2)*sqrt((2/sqrt(3)).*[6000, 18000])))
ylabel('Nyquist (cycles/deg)')
ax = gca;
ax.YAxis(2).Color = [0 .7 0.5];


figure;
b = plot(nanmean(axisR_vertPCD),nanmean(valR_vertPCD),'k');hold on;
plot(nanmean(axisR_vertPCD),nanmean(valR_vertPCD) + PCD_CI, 'k-.');hold on;
plot(nanmean(axisR_vertPCD),nanmean(valR_vertPCD) - PCD_CI,'k-.');hold on;

xlim([-20, 20])
ylim([6000, 18000])
xlabel('Eccentricity wrt PCD (arc)')
ylabel('Cone density (cones/deg^2)')
yyaxis right
ylim(((1/2)*sqrt((2/sqrt(3)).*[6000, 18000])))
ylabel('Nyquist (cycles/deg)')
ax = gca;
ax.YAxis(2).Color = [0 .7 0.5];



figure;
c = plot(nanmean(axisR_vertCDC),nanmean(valR_vertCDC),'b');hold on;
plot(nanmean(axisR_vertCDC),nanmean(valR_vertCDC)+CDC_CI, 'b-.');hold on;
plot(nanmean(axisR_vertCDC),nanmean(valR_vertCDC)-CDC_CI, 'b-.');hold on;

xlim([-20, 20])
ylim([6000, 18000])
xlabel('Eccentricity wrt CDC(arc)')
ylabel('Cone density (cones/deg^2)')
yyaxis right
ylim(((1/2)*sqrt((2/sqrt(3)).*[6000, 18000])))
ylabel('Nyquist (cycles/deg)')
ax = gca;
ax.YAxis(2).Color = [0 .7 0.5];


figure;
for nfiles = 1:length(areaList)
    plot(axisR_vertPRL(nfiles,:),valR_vertPRL(nfiles,:));hold on;
input ''
end
title('PRL centered')
xlim([-20, 20])
ylim([6000, 18000])
yyaxis right
ylim(((1/2)*sqrt((2/sqrt(3)).*[6000, 18000])))
ylabel('Nyquist (cycles/deg)')
ax = gca;
ax.YAxis(2).Color = [0 .7 0.5];

figure;
for nfiles = 1:length(areaList)
    plot(axisR_vertPCD(nfiles,:),valR_vertPCD(nfiles,:));hold on;

end
title('PCD centered')
xlim([-20, 20])
ylim([6000, 18000])
yyaxis right
ylim(((1/2)*sqrt((2/sqrt(3)).*[6000, 18000])))
ylabel('Nyquist (cycles/deg)')
ax = gca;
ax.YAxis(2).Color = [0 .7 0.5];

figure;
for nfiles = 1:length(areaList)
    plot(axisR_vertCDC(nfiles,:),valR_vertCDC(nfiles,:),'g');hold on;
    plot(axisR_horzCDC(nfiles,:),valR_horzCDC(nfiles,:),'r');hold on;
end
legend('vertical', 'horizontal')
title('CDC centered')
xlim([-20, 20])
ylim([6000, 18000])
yyaxis right
ylim(((1/2)*sqrt((2/sqrt(3)).*[6000, 18000])))
ylabel('Nyquist (cycles/deg)')
ax = gca;
ax.YAxis(2).Color = [0 .7 0.5];



%
% % v_shiftCDC(nfiles) = (401-(find(norm_vertCDC_rowAxis(:,nfiles)==0)));
% % axisR_horzCDC(nfiles,h_shiftCDC(nfiles)+1:h_shiftCDC(nfiles)+1+711) = norm_horzCDC_colAxis(:,nfiles)';
%
% axisR(1,59+1:59+1+711) = norm_vertCDC_rowAxis(:,1)';
%
% 801/2
