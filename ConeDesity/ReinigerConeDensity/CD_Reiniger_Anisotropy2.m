

%%
clear;clc;close all;

%%% if you match the PCD to the value to the supplementary, we are
%%% getting a really close PCD, but the orientation of the figure must
%%% be off because the indexing is not matching
onlyRight = 0;

plotIndiv = 0;

pixPerdeg = 600;
CDC_Percentile = 20;
% areaPath = 'C:\Users\sjenks\Desktop\research\rochester\AO\adaptiveoptics\ConeDesity\ReinigerConeDensity\AvgConesPerPix\712x712\';
% dataPath = 'C:\Users\sjenks\Desktop\research\rochester\AO\adaptiveoptics\ConeDesity\ReinigerConeDensity\ConeLocations\';
areaPath = 'C:\Users\ruccilab\Documents\adaptiveoptics\ConeDesity\ReinigerConeDensity\AvgConesPerPix\712x712\';
dataPath = 'C:\Users\ruccilab\Documents\adaptiveoptics\ConeDesity\ReinigerConeDensity\ConeLocations\';
if onlyRight
    areaList_OG = dir(areaPath);
    areaList = areaList_OG(4:2:end-2);
    areaList(end+1) = areaList_OG(end-2);
    
    dataList_OG = dir(dataPath);
    dataList = dataList_OG(4:2:end-1);
    dataList(end+1) = dataList_OG(end);
    
else
    areaList = dir(areaPath);
    areaList = areaList(3:end-2);
    
    dataList = dir(dataPath);
    dataList = dataList(3:end);
end




for nfiles = 1:length(areaList)
    
    %%% Loading data 
    if nfiles > 1
        clear avgAreaPerCone CDC_PRL_data
    end
    load([areaPath areaList(nfiles).name])
    load([dataPath dataList(nfiles).name])
    refImage_all{nfiles} = refImage;
    avgAreaPerCone_all{nfiles} = avgAreaPerCone;%superior retina is at zero
    
    
    if isempty(CDC_PRL_data) == 0
        CDC_x_all(nfiles) = CDC_PRL_data{1};
        CDC_y_all(nfiles) = CDC_PRL_data{2};
        PCD_x_all(nfiles) = CDC_PRL_data{3};
        PCD_y_all(nfiles) = CDC_PRL_data{4};
        PRL_x_all(nfiles) = CDC_PRL_data{5};
        PRL_y_all(nfiles) = CDC_PRL_data{6};
        CD_conePerPixsq2{nfiles} = 1./avgAreaPerCone_all{nfiles};
        CD_conePerDegsq2{nfiles} = CD_conePerPixsq2{nfiles} .* (pixPerdeg).^2;
        PCD_valGT(nfiles) = CD_conePerDegsq2{nfiles}(PCD_y_all(nfiles), PCD_x_all(nfiles));
        
        
        
        %getting the maximum cones to compare to reinigers peak cone densitys
        [PCD_ours(nfiles), index_PCD_ours(nfiles)] = max(max(CD_conePerDegsq2{nfiles}));
        [PCD_idxRow(nfiles), PCD_idxCol(nfiles)] = find(CD_conePerDegsq2{nfiles} == PCD_ours(nfiles));
        oursPCD_x = PCD_idxCol(nfiles);
        oursPCD_y = PCD_idxRow(nfiles) ;
        
        PCD_X = PCD_x_all(nfiles);
        PCD_Y = PCD_y_all(nfiles);
        CDC_X = CDC_x_all(nfiles);
        CDC_Y = CDC_y_all(nfiles);
        PRL_X = PRL_x_all(nfiles);
        PRL_Y = PRL_y_all(nfiles);
        
        if plotIndiv
            
            figure;
            image(refImage);hold on;
            OverlayImage = imagesc(CD_conePerDegsq2{nfiles});hold on;
            alpha(0.3);colorbar;hold on;

            plot(CDC_X,CDC_Y,'.r', 'MarkerSize', 5); hold on;% I flipped this because the point was the max max of the matrix, thus needed to be rotated to be ontop of an image
            plot(PCD_X,PCD_Y,'^k', 'MarkerSize', 5); hold on;
            plot(PRL_X,PRL_Y,'sk', 'MarkerSize', 5); hold on;
            plot(oursPCD_x,oursPCD_y,'c^', 'MarkerSize', 20); hold on;
        end

    end

    
    %%%% Setting up the axes for the horizontal vs vertical merdians
    %PCD
    hozrPCD_col_R = CD_conePerDegsq2{nfiles}(PCD_Y - 5: PCD_Y + 5,:);
    vertPCD_row_R = CD_conePerDegsq2{nfiles}(:,PCD_X-5:PCD_X + 5)';
    avg_hozrPCD_col(nfiles,:) = mean(hozrPCD_col_R);
    avg_vertPCD_row(nfiles,:) = mean(vertPCD_row_R,1);
    
    %CDC
    hozrCDC_col_R = CD_conePerDegsq2{nfiles}(CDC_Y- 5 : CDC_Y + 5,:);
    vertCDC_row_R = CD_conePerDegsq2{nfiles}(:,CDC_X- 5 : CDC_X + 5)';
    avg_hozrCDC_col(nfiles,:) = mean(hozrCDC_col_R);
    avg_vertCDC_row(nfiles,:) = mean(vertCDC_row_R,1)';
    
    %PRL
    hozrPRL_col_R = CD_conePerDegsq2{nfiles}(PRL_Y- 5: PRL_Y + 5,:);
    vertPRL_row_R = CD_conePerDegsq2{nfiles}(:,PRL_X- 5 : PRL_X + 5)';
    avg_hozrPRL_col(nfiles,:) = mean(hozrPRL_col_R);
    avg_vertPRL_row(nfiles,:) = mean(vertPRL_row_R,1)';

    
    
    %%%% Calculating CDC from gray scale heatmap
    if plotIndiv
        figure;
        image(refImage);  hold on;
        plot(PCD_X,PCD_Y,'^k', 'MarkerSize', 5); hold on;
        plot(PCD_idxCol(nfiles),PCD_idxRow(nfiles), '^r', 'MarkerSize', 5)
    end

    gray_CD = mat2gray(CD_conePerDegsq2{nfiles});
    APC_Prctile = prctile(avgAreaPerCone_all{nfiles},CDC_Percentile,1);
    threshCDC = prctile(APC_Prctile,CDC_Percentile,2);
    
    APC = avgAreaPerCone_all{nfiles} ;
    APC(APC<threshCDC)=1;
    APC(APC>=threshCDC)=0;
    
    measurements = regionprops(APC, gray_CD, 'WeightedCentroid');
    oursCDC = measurements.WeightedCentroid;
    oursCDC_x(nfiles) = oursCDC(1);
    oursCDC_y(nfiles) = oursCDC(2);
    
    CDC_val_all(nfiles) = CD_conePerDegsq2{nfiles}(round(CDC_y_all(nfiles),0),round(CDC_x_all(nfiles),0));
    oursCDC_val_all(nfiles) = CD_conePerDegsq2{nfiles}(round(oursCDC_y(nfiles),0),round(oursCDC_x(nfiles),0));
    
    if plotIndiv
        figure;imshow(APC);hold on;
        plot(oursCDC_x(nfiles), oursCDC_y(nfiles), 'r*', 'LineWidth', 2, 'MarkerSize', 16);
    end

end
%%% confidence intervals for the HM and VM distribtions.
CI95_R = tinv([0.025 0.975], (length(areaList))-1);
PCD_CI = CI95_R(2) *(std(avg_vertPCD_row)/sqrt(size(avg_vertPCD_row,1)));
PRL_CI = CI95_R(2) *(std(avg_vertCDC_row)/sqrt(size(avg_vertCDC_row,1)));
CDC_CI = CI95_R(2) *(std(avg_vertPRL_row)/sqrt(size(avg_vertPRL_row,1)));


cmap = hsv(length(areaList));


%% Viewing Cone Density Accuracy 
% 
% %%%%%%%%%%%%%%%%%%%%%%%
% figure;
% subplot(1,3,1)
% scatter(oursCDC_val_all,CDC_val_all)
% refline(1,0)
% xlabel('Our Cone Density Centroid')
% ylabel('Reiniger Cone Density Centroid')
% 
% subplot(1,3,2)
% scatter(oursCDC_x,CDC_x_all)
% refline(1,0)
% xlabel('Our Cone Density Centroid')
% ylabel('Reiniger Cone Density Centroid')
% 
% subplot(1,3,3)
% scatter(oursCDC_y,CDC_y_all)
% refline(1,0)
% xlabel('Our Cone Density Centroid')
% ylabel('Reiniger Cone Density Centroid')
% 
% %%%%%%%%%%%%%%%%%%%%%%%
% figure;
% subplot(1,3,1)
% diff_CD = CDC_val_all - oursCDC_val_all; %Reiniger - our data
% histogram(diff_CD);
% xlabel('Cone Density Centroid difference (Reinger - Our data)')
% ylabel('Frequency ( number of subjects)')
% title('Cone density Centroid difference')
% 
% subplot(1,3,2)
% diff_CD = CDC_x_all - oursCDC_x; %Reiniger - our data
% histogram(diff_CD);
% xlabel('CDC index Horizontal difference ')
% ylabel('Frequency (Number of subjects)')
% % title('Peak cone density difference (Reinger - Our data)')
% 
% subplot(1,3,3)
% diff_CD = CDC_y_all - oursCDC_y; %Reiniger - our data
% histogram(diff_CD);
% xlabel('CDC index Vertical difference')
% ylabel('Frequency (Number of subjects)')
% sgtitle('Cone density Centroid difference (Reinger - Our data)')
% 
% %%%%%%%%%%%%%%%%%%%%%%%
% figure;
% subplot(1,3,1)
% scatter(PCD_ours,PCD_valGT, 25);%,cmap, 'filled');% (our calc, groundtruth from reiniger)
% refline(1,0)
% xlabel('Our Peak Cone Density')
% ylabel('Reiniger Peak Cone Density')
% 
% subplot(1,3,2)
% scatter(PCD_idxCol,PCD_x_all, 25);%, cmap,'filled');hold on; % (our calc, groundtruth from reiniger)
% xlabel('Our row index for PCD ')
% ylabel('Reiniger row index for PCD')
% xlim([300 500])
% ylim([300 500])
% refline(1,0)
% 
% subplot(1,3,3)
% scatter( PCD_idxRow,PCD_y_all, 25);%, cmap,'filled');hold on; % (our calc, groundtruth from reiniger)
% xlabel('Our row index for PCD ')
% ylabel('Reiniger row index for PCD')
% xlim([300 500])
% ylim([300 500])
% refline(1,0)
% 
% %%%%%%%%%%%%%%%%%%%%%%%
% figure;
% subplot(1,3,1)
% diff_CD_val = PCD_valGT - PCD_ours; %Reiniger - our data
% histogram(diff_CD_val);
% xlabel('Peak Cone densitiy difference (Reinger - Our data)')
% ylabel('Frequency ( number of subjects)')
% title('Peak cone density difference')
% 
% subplot(1,3,2)
% diff_CDx = PCD_x_all - PCD_idxCol; %Reiniger - our data
% histogram(diff_CDx);
% xlabel('PCD index Horizontal difference ')
% ylabel('Frequency (Number of subjects)')
% 
% subplot(1,3,3)
% diff_CDy = PCD_y_all - PCD_idxRow; %Reiniger - our data
% histogram(diff_CDy);
% xlabel('PCD index Vertical difference')
% ylabel('Frequency (Number of subjects)')
% sgtitle('Peak cone density difference (Reinger - Our data)')


%% Across subject distributions 
%%% All subjects centered in PCD, CDC, PRL sub pannels
%%%%%%%%%%%%%%
figure;
%%% PCD
pixX_PCDtrimed = [1:length(vertPCD_row_R)];
XaxisPCD_DEG_R = (pixX_PCDtrimed/pixPerdeg)* 60;
pixY_PCDtrimed = [1:length(hozrPCD_col_R)];
YaxisPCD_DEG_R = (pixY_PCDtrimed/pixPerdeg)* 60;
normPCD_Xaxis_deg_R = XaxisPCD_DEG_R - ((PCD_Y/pixPerdeg)* 60);
normPCD_Yaxis_deg_R = YaxisPCD_DEG_R - ((PCD_X/pixPerdeg)* 60);

subplot(1,3,1)
plot(normPCD_Xaxis_deg_R,mean(avg_vertPCD_row), 'r');%vertical meridian
hold on;
plot(normPCD_Xaxis_deg_R, mean(avg_vertPCD_row) + PCD_CI, '--r'); 
hold on;
plot(normPCD_Xaxis_deg_R, mean(avg_vertPCD_row) - PCD_CI, '--r'); %vertical meridian

title('Cone Density centered around PCD')
xlabel('Degrees Eccentricity')
ylabel('Cone Density (cones/degree^2)')
ylim([6000, 18000])
legend('horizontal', 'vertical')

%%%%%%%%%%%%%%
subplot(1,3,2)
%%% CDC (REINIGERS)
pixX_CDCtrimed = [1:length(vertCDC_row_R)];
XaxisCDC_DEG = (pixX_CDCtrimed/pixPerdeg)*60;
pixY_CDCtrimed = [1:length(hozrCDC_col_R)];
YaxisCDC_DEG = (pixY_CDCtrimed/pixPerdeg)*60;

normCDC_Xaxis_deg = XaxisCDC_DEG - ((CDC_Y/pixPerdeg)* 60);
normCDC_Yaxis_deg = YaxisCDC_DEG - ((CDC_X/pixPerdeg)* 60);

%Plot Cone density around peak cone density
plot(normCDC_Xaxis_deg, mean(avg_vertCDC_row), 'r'); %horizontal meridian
hold on;
plot(normCDC_Xaxis_deg,mean(avg_vertCDC_row) + CDC_CI, '--r'); %vertical meridian
hold on;
plot(normCDC_Xaxis_deg,mean(avg_vertCDC_row) - CDC_CI, '--r'); %vertical meridian
title('Cone Density centered around the CDC')
xlabel('Degrees Eccentricity')
ylabel('Cone Density (cones/degree^2)')
ylim([6000, 18000])

%%%%%%%%%%%%%%
subplot(1,3,3)
%%% PRL (REINIGERS)
%convert the pixels to degrees (base off image)
pixX_PRLtrimed = [1:length(vertPRL_row_R)];
XaxisPRL_DEG = (pixX_PRLtrimed/pixPerdeg)*60;
pixY_PRLtrimed = [1:length(hozrPRL_col_R)];
YaxisPRL_DEG = (pixY_PRLtrimed/pixPerdeg)*60;

normPRL_Xaxis_deg = XaxisPRL_DEG - ((PRL_Y/pixPerdeg)*60);
normPRL_Yaxis_deg = YaxisPRL_DEG - ((PRL_X/pixPerdeg)*60);

%Plot Cone density around peak cone density
plot(normPRL_Xaxis_deg, mean(avg_vertPRL_row), 'r'); %vertical meridian
hold on;
plot(normPRL_Xaxis_deg,mean(avg_vertPRL_row) + PRL_CI, '--r'); 
hold on;
plot(normPRL_Xaxis_deg,mean(avg_vertPRL_row) - PRL_CI, '--r');

title('Cone Density centered around the PRL')
xlabel('Degrees Eccentricity')
ylabel('Cone Density (cones/degree^2)')
ylim([6000, 18000])
legend('horizontal', 'vertical')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% ALL subject distribution in 1 plot
% pixX_PCDtrimed = [1:length(vertPCD_row_R)];
% XaxisPCD_DEG_R = (pixX_PCDtrimed/pixPerdeg)* 60;
% pixY_PCDtrimed = [1:length(hozrPCD_col_R)];
% YaxisPCD_DEG_R = (pixY_PCDtrimed/pixPerdeg)* 60;
% normPCD_Xaxis_deg_R = XaxisPCD_DEG_R - ((PCD_X/pixPerdeg)* 60);
% normPCD_Yaxis_deg_R = YaxisPCD_DEG_R - ((PCD_Y/pixPerdeg)* 60);
% 
% pixX_PRLtrimed = [1:length(vertPRL_row_R)];
% XaxisPRL_DEG = (pixX_PRLtrimed/pixPerdeg)*60;
% pixY_PRLtrimed = [1:length(hozrPRL_col_R)];
% YaxisPRL_DEG = (pixY_PRLtrimed/pixPerdeg)*60;
% normPRL_Xaxis_deg = XaxisPRL_DEG - ((PRL_X/pixPerdeg)*60);
% normPRL_Yaxis_deg = YaxisPRL_DEG - ((PRL_Y/pixPerdeg)*60);


figure; 
a = plot(normPCD_Xaxis_deg_R,mean(avg_vertPCD_row), 'k');%vertical meridian
hold on;
plot(normPCD_Xaxis_deg_R, mean(avg_vertPCD_row) + PCD_CI, '--k'); 
hold on;
plot(normPCD_Xaxis_deg_R, mean(avg_vertPCD_row) - PCD_CI, '--k'); %vertical meridian

hold on; 
b = plot(normPRL_Xaxis_deg, mean(avg_vertPRL_row), 'g'); %vertical meridian
hold on;
plot(normPRL_Xaxis_deg,mean(avg_vertPRL_row) + PRL_CI, '--g'); 
hold on;
plot(normPRL_Xaxis_deg,mean(avg_vertPRL_row) - PRL_CI, '--g');

hold on; 
c = plot(normCDC_Xaxis_deg, mean(avg_vertCDC_row), 'b'); %vertical meridian
hold on;
plot(normCDC_Xaxis_deg,mean(avg_vertCDC_row) + CDC_CI, '--b'); 
hold on;
plot(normCDC_Xaxis_deg,mean(avg_vertCDC_row) - CDC_CI, '--b');



legend([a,b,c], 'PCD centered', 'PRL centered','CDC centered')
xlabel('Degrees Eccentricity')
ylabel('Cone Density (cones/degree^2)')
ylim([9000, 18000])
title('Cone Density along the Vertical Merdian')

text(-25,15000,'Superior Retina')
text(15,15000,'Inferior Retina')



%% get Cone density at 20 acrmin with an area of 2 arc min

% variables
targOffset = 0.25; % eccentricity from PRL in deg
area = 0.033; % how big you want to take the area (square)

ecc = round(targOffset * pixPerdeg,0); %eccentrictiy in pixels
box = round(area * pixPerdeg,0); % box area in pixels

warning("Reiniger Data has left and right eye, so left and right cannot be compared here")

for nfiles = 1:length(areaList)
    
    PCD_X = PCD_x_all(nfiles);
    PCD_Y = PCD_y_all(nfiles);
    CDC_X = CDC_x_all(nfiles);
    CDC_Y = CDC_y_all(nfiles);
    PRL_X = PRL_x_all(nfiles);
    PRL_Y = PRL_y_all(nfiles);
    
    %location of boxes
    SuperiorCenter = PRL_Y - ecc; %superior retina is zero
    InferiorCenter=  PRL_Y + ecc;
    
    SuperiorCenter_CDC = CDC_Y - ecc; %superior retina is zero
    InferiorCenter_CDC =  CDC_Y + ecc;
    
    
    CD_conePerDegsq = CD_conePerDegsq2{nfiles};
    if strcmp( areaList(nfiles).name(20),'R')
        NasalCenter =  PRL_X - ecc; %nasal retina is to the right and covers left visual field
        TempCenter = PRL_X + ecc;
        
        NasalCenter_CDC =  CDC_X - ecc; %nasal retina is to the right and covers left visual field
        TempCenter_CDC = CDC_X + ecc;
        
        text(110,(size(CD_conePerDegsq,1)/2)+10,'Temporal Retina')
        text(size(CD_conePerDegsq,1)-250,(size(CD_conePerDegsq,1)/2)+10,'Nasal Retina')
    else
        NasalCenter =  PRL_X + ecc;
        TempCenter = PRL_X - ecc;
        
        NasalCenter_CDC =  CDC_X + ecc;
        TempCenter_CDC = CDC_X - ecc;
        
        text(10,(size(CD_conePerDegsq,1)/2)-160,'Nasal Retina')
        text(size(CD_conePerDegsq,1)-200,(size(CD_conePerDegsq,1)/2)-60,'Temporal Retina')
    end
    %%%%%%%%%%%%%%%% PRL set up %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %set up the box locations
    superiorBox = CD_conePerDegsq(SuperiorCenter - box:SuperiorCenter + box,  PRL_X - box: PRL_X + box);
    superiorArea = mean(mean(superiorBox));
    
    inferiorBox = CD_conePerDegsq(InferiorCenter - box: InferiorCenter + box,  PRL_X - box: PRL_X + box);
    inferiorArea = mean(mean(inferiorBox));
    
    nasalBox = CD_conePerDegsq(PRL_Y - box: PRL_Y + box, NasalCenter- box : NasalCenter +box);
    nasalArea = mean(mean(nasalBox));
    
    temporalBox = CD_conePerDegsq(PRL_Y - box: PRL_Y + box,TempCenter- box : TempCenter + box);
    temporalArea = mean(mean(temporalBox));
    
    %black out where the box locations will be
    viewTargLoc = CD_conePerDegsq;
    viewTargLoc(SuperiorCenter - box: SuperiorCenter + box, PRL_X - box: PRL_X + box)=0;
    viewTargLoc(InferiorCenter - box: InferiorCenter + box,  PRL_X - box: PRL_X + box)=0;
    viewTargLoc(PRL_Y - box: PRL_Y + box, NasalCenter- box : NasalCenter +box)=0;
    viewTargLoc(PRL_Y - box: PRL_Y + box,TempCenter- box : TempCenter + box)=0;
    legend(sprintf('%s',areaList(nfiles).name))
    
    
    %%%%%%%%%%%%%%%% CDC set up %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    superiorBox_CDC = CD_conePerDegsq(SuperiorCenter_CDC - box:SuperiorCenter_CDC + box,  CDC_X - box: CDC_X + box);
    superiorArea_CDC = mean(mean(superiorBox_CDC));
    
    inferiorBox_CDC = CD_conePerDegsq(InferiorCenter_CDC - box: InferiorCenter_CDC + box,  CDC_X - box: CDC_X + box);
    inferiorArea_CDC = mean(mean(inferiorBox_CDC));
    
    nasalBox_CDC = CD_conePerDegsq(CDC_Y - box: CDC_Y + box, NasalCenter_CDC- box : NasalCenter_CDC +box);
    nasalArea_CDC = mean(mean(nasalBox_CDC));
    
    temporalBox_CDC = CD_conePerDegsq(CDC_Y - box: CDC_Y + box,TempCenter_CDC- box : TempCenter_CDC + box);
    temporalArea_CDC = mean(mean(temporalBox_CDC));
    
    %black out where the box locations will be
    viewTargLoc_CDC = CD_conePerDegsq;
    viewTargLoc_CDC(SuperiorCenter_CDC - box: SuperiorCenter_CDC + box, CDC_X - box: CDC_X + box)=0;
    viewTargLoc_CDC(InferiorCenter_CDC - box: InferiorCenter_CDC + box,  CDC_X - box: CDC_X + box)=0;
    viewTargLoc_CDC(CDC_Y - box: CDC_Y + box, NasalCenter_CDC- box : NasalCenter_CDC +box)=0;
    viewTargLoc_CDC(CDC_Y - box: CDC_Y + box,TempCenter_CDC- box : TempCenter_CDC + box)=0;
    legend(sprintf('%s',areaList(nfiles).name))
    
    %%%%%%%%%%%%%%%% figures %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if plotIndiv
        % black out the target locations on the heatmap to verify their location.
        figure;
        %         subplot(1,2,1)
        imagesc(viewTargLoc); hold on;
        a=plot(PRL_X,PRL_Y, 'g.', 'MarkerSize', 25);hold on;
        %     b=plot(PCD_X,PCD_Y, 'r^');hold on;
        c=plot(CDC_X,CDC_Y, 'b*');hold on;
        xlim([100,600])
        ylim([150,600])
        
        text((size(CD_conePerDegsq,1)/2)-60,175,'Superior Retina')
        text((size(CD_conePerDegsq,1)/2)-60,size(CD_conePerDegsq,2)-140,'Inferior Retina Retina')
        text(110,(size(CD_conePerDegsq,1)/2)-30,'Temporal Retina')
        text(size(CD_conePerDegsq,1)-250,(size(CD_conePerDegsq,1)/2)-30,'Nasal Retina')
        legend([a,c],'Preferred Retinal Locus','Cone Density Centroid')
        axis off
        colorbar
        
        figure;
        %     subplot(1,2,2)
        imagesc(viewTargLoc_CDC); hold on;
        a=plot(PRL_X,PRL_Y, 'g.', 'MarkerSize', 25);hold on;
        b=plot(CDC_X,CDC_Y, 'b*');hold on;
        %     plot(PCD_X,PCD_Y, 'r^');hold on;
        xlim([100,600])
        ylim([150,600])
        
        text((size(CD_conePerDegsq,1)/2)-60,175,'Superior Retina')
        text((size(CD_conePerDegsq,1)/2)-60,size(CD_conePerDegsq,2)-140,'Inferior Retina Retina')
        text(110,(size(CD_conePerDegsq,1)/2)-30,'Temporal Retina')
        text(size(CD_conePerDegsq,1)-250,(size(CD_conePerDegsq,1)/2)-30,'Nasal Retina')
        legend([a,c],'Preferred Retinal Locus','Cone Density Centroid')
        axis off
        colorbar
        
    end
    rAreas(nfiles,:) = [superiorArea  inferiorArea  nasalArea temporalArea];
    rAreas_CDC(nfiles,:) = [superiorArea_CDC  inferiorArea_CDC  nasalArea_CDC temporalArea_CDC];
    
    % comparing normalized cone density to performance
    
end

CI95 = tinv([0.025 0.975], length(rAreas)-1);
ste_N = (std(rAreas)/sqrt(length(rAreas)));
CI_XC = CI95(2) * ste_N;

ste_N_CDC = (std(rAreas_CDC)/sqrt(length(rAreas_CDC)));
CI_XC_CDC = CI95(2) * ste_N_CDC;


avgHM_Reiniger = mean([rAreas(:,3) rAreas(:,4)],2);
avgVM_Reiniger = mean([rAreas(:,1) rAreas(:,2)],2);
avgHM_Reiniger_CDC = mean([rAreas_CDC(:,3) rAreas_CDC(:,4)],2);
avgVM_Reiniger_CDC = mean([rAreas_CDC(:,1) rAreas_CDC(:,2)],2);

ste_HM = (std(avgHM_Reiniger)/sqrt(length(avgHM_Reiniger)));
CI_XChm = CI95(2) * ste_HM;
ste_VM = (std(avgVM_Reiniger)/sqrt(length(avgVM_Reiniger)));
CI_XCvm = CI95(2) * ste_VM;

ste_HM_CDC = (std(avgHM_Reiniger_CDC)/sqrt(length(avgHM_Reiniger_CDC)));
CI_XChm_CDC = CI95(2) * ste_HM_CDC;
ste_VM_CDC = (std(avgVM_Reiniger_CDC)/sqrt(length(avgVM_Reiniger_CDC)));
CI_XCvm_CDC = CI95(2) * ste_VM_CDC;


avg_reiniger = mean(rAreas);
avg_reiniger_CDC = mean(rAreas_CDC);

figure;
errorbar([1:4], avg_reiniger,CI_XC, 'Color', [0 1 0])
hold on;
errorbar([1:4], avg_reiniger_CDC,CI_XC_CDC, 'Color', [0 0 1])
xticks([1:4])
xlim([.75 4.25])
ylim([8000 12000])
ylabel('Cone density (cones/deg^2)')
xticklabels({'Superior/lowerVF', 'Inferior/UpperVF', 'Nasal','Temporal'})
legend('PRL Centered', 'CDC Centered')

%% PRL with Performance
% load('C:\Users\ruccilab\Documents\adaptiveoptics\ConeDesity\APLab_System\data\FVP.mat');
load('C:\Users\sjenks\Desktop\research\rochester\AO\adaptiveoptics\ConeDesity\FVP.mat')
CI95_F = tinv([0.025 0.975], length(FvP.data(1).avgPerf)-1);
FovPerf_XC = mean(FvP.data(1).avgPerf(:,1:2:8));
FovPerf_XC = [FovPerf_XC(3),FovPerf_XC(1),FovPerf_XC(2),FovPerf_XC(4)];
FovHVAPerf_XC = mean(FvP.data(1).perf_HVA);
FovHVACI_XC = mean(FvP.data(1).HVA_stePerf)*CI95_F(2);



ste_F = (std(FvP.data(1).avgPerf(:,1:2:8))/sqrt(length(FvP.data(1).avgPerf(:,1:2:8))));
CIXC_F = CI95_F(2) * ste_F;
CIXC_F = [CIXC_F(3), CIXC_F(1), CIXC_F(2), CIXC_F(4)];

label = {'PRL','CDC' };
for cc = 1:2
    
    figure;
    if cc == 1
        yyaxis left
        p1 = errorbar([1,2], avg_reiniger(1:2),CI_XC(1:2),'.-', 'Color', [0 1 0]); hold on;
        % errorbar([3,4], avg_reiniger(3:4),CI_XC(3:4),'.-', 'Color', [0 1 0]); hold on;
        xticks(1:2)
        xlim([.75 2.25])
        ylim([8000 12000])
        ylabel('Cone density (cones/deg^2)')
    elseif cc ==2
        yyaxis left
        p1 = errorbar([1,2], avg_reiniger_CDC(1:2),CI_XC(1:2),'.-', 'Color', [0 0 1]); hold on;
        % errorbar([3,4], avg_reiniger(3:4),CI_XC(3:4),'.-', 'Color', [0 1 0]); hold on;
        xticks(1:2)
        xlim([.75 2.25])
        ylim([8000 12000])
        ylabel('Cone density (cones/deg^2)')
    end
    
    yyaxis right
    p2 = errorbar([1,2], FovPerf_XC(1:2),CIXC_F(1:2),'.-','Color',[1 0 1]); hold on;
    % errorbar([3, 4], FovPerf_XC(3:4),CIXC_F(3:4),'.-','Color',[1 0 1]); hold on;
    xticks(1:2)
    xlim([.75 2.25])
    ylim([0.5 1])
    ylabel('Proportion Correct)')
    
    xticklabels({'Superior/lowerVF', 'Inferior/UpperVF'})
    legend([p1 p2],{sprintf('%s Centered', label{cc}),'Performance'})
    text(1,1.5,'Vertical Meridian')
    % text(3,0.525,'Horizontal Meridian')
    ax = gca;
    if cc == 1
        ax.YAxis(1).Color =  [0 1 0];
    else
        ax.YAxis(1).Color =  [0 0 1];
    end
    ax.YAxis(2).Color = [1 0 1];
    
end


label = {'PRL','CDC' };
for cc = 1:2
    
    figure;
    if cc == 1
        yyaxis left
        p1 = errorbar([1,2], [mean(avgHM_Reiniger) mean(avgVM_Reiniger)],[CI_XChm CI_XCvm],'.-', 'Color', [0 1 0]); hold on;
        % errorbar([3,4], avg_reiniger(3:4),CI_XC(3:4),'.-', 'Color', [0 1 0]); hold on;
        xticks(1:2)
        xlim([.75 2.25])
        ylim([8000 12000])
        ylabel('Cone density (cones/deg^2)')
    elseif cc ==2
        yyaxis left
        p1 = errorbar([1,2],[mean(avgHM_Reiniger_CDC) mean(avgVM_Reiniger_CDC)],[CI_XChm_CDC CI_XCvm_CDC],'.-', 'Color', [0 0 1]); hold on;
        % errorbar([3,4], avg_reiniger(3:4),CI_XC(3:4),'.-', 'Color', [0 1 0]); hold on;
        xticks(1:2)
        xlim([.75 2.25])
        ylim([8000 12000])
        ylabel('Cone density (cones/deg^2)')
    end
    
    yyaxis right
    p2 = errorbar([1,2], FovHVAPerf_XC,FovHVACI_XC(1:2),'.-','Color',[1 0 1]); hold on;
    % errorbar([3, 4], FovPerf_XC(3:4),CIXC_F(3:4),'.-','Color',[1 0 1]); hold on;
    xticks(1:2)
    xlim([.75 2.25])
    ylim([0.5 1])
    ylabel('Proportion Correct)')
    
    xticklabels({'Horizontal', 'Vertical'})
    legend([p1 p2],{sprintf('%s Centered', label{cc}),'Performance'})
    %     text(1,1.5,'Horizontal Meridian')
    % text(3,0.525,'Horizontal Meridian')
    ax = gca;
    if cc == 1
        ax.YAxis(1).Color =  [0 1 0];
    else
        ax.YAxis(1).Color =  [0 0 1];
    end
    ax.YAxis(2).Color = [1 0 1];
    
end


%% Stats

locXcent = [rAreas; rAreas_CDC];

%Set this up correctly
[p,tableA,stats]=anova2(locXcent,length(rAreas));

sup_PRL = rAreas(:,1);
inf_PRL = rAreas(:,2);
HM_PRL = mean(rAreas(:,3:4),2);
VM_PRL = mean(rAreas(:,1:2),2);

sup_CDC = rAreas_CDC(:,1);
inf_CDC = rAreas_CDC(:,2);
HM_CDC = mean(rAreas_CDC(:,3:4),2);
VM_CDC = mean(rAreas_CDC(:,1:2),2);

HVA_array = [HM_CDC VM_CDC;HM_PRL, VM_PRL];

VMA_array = [sup_CDC inf_CDC;sup_PRL, inf_PRL];


[p_CDhva,tableA_CDhva,stats_CDhva]=anova2(HVA_array,length(HM_CDC));

[p_CDvma,tableA_CDvma,stats_CDvma]=anova2(VMA_array,length(sup_CDC));


sprintf('Data analysis only for Right eye = %i',onlyRight)

[hvaCDC_h, hvaCDC_p, hvaCDC_ci,hvaCDC_STATS] = ttest(HM_CDC, VM_CDC)
[hvaPRL_h, hvaPRL_p, hvaPRL_ci,hvaPRL_STATS] = ttest(HM_PRL, VM_PRL)

[vmaCDC_h, vmaCDC_p, vmaCDC_ci,vmaCDC_STATS] = ttest(sup_CDC, inf_CDC)
[vmaPRL_h, vmaPRL_p, vmaPRL_ci,vmaPRL_STATS] = ttest(sup_PRL, inf_PRL)

%% old individual subject distributions.

for nfiles = 1:length(areaList)
    if plotIndiv
        figure;
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%% PCD
        %trim the edges of the matrix (OURS)
        hozrPCD_col = CD_conePerDegsq2{nfiles}(oursPCD_y - 5: oursPCD_y + 5,:);
        vertPCD_row = CD_conePerDegsq2{nfiles}(:,oursPCD_x -5:oursPCD_x + 5)';
        %convert the pixels to degrees (base off image)
        pixX_PCDtrimed = [1:length(vertPCD_row)];
        XaxisPCD_DEG = pixX_PCDtrimed/pixPerdeg;
        pixY_PCDtrimed = [1:length(hozrPCD_col)];
        YaxisPCD_DEG = pixY_PCDtrimed/pixPerdeg;
        normPCD_Xaxis_deg = XaxisPCD_DEG - (oursPCD_y/pixPerdeg);
        normPCD_Yaxis_deg = YaxisPCD_DEG - (oursPCD_x /pixPerdeg);
        
        
        %trim the edges of the matrix (REINIGERS)
        hozrPCD_col_R = CD_conePerDegsq2{nfiles}(PCD_Y - 5: PCD_Y + 5,:);
        vertPCD_row_R = CD_conePerDegsq2{nfiles}(:,PCD_X-5:PCD_X + 5)';
        %convert the pixels to degrees (base off image)
        pixX_PCDtrimed = [1:length(vertPCD_row_R)];
        XaxisPCD_DEG_R = pixX_PCDtrimed/pixPerdeg;
        pixY_PCDtrimed = [1:length(hozrPCD_col_R)];
        YaxisPCD_DEG_R = pixY_PCDtrimed/pixPerdeg;
        normPCD_Xaxis_deg_R = XaxisPCD_DEG_R - (PCD_Y/pixPerdeg);
        normPCD_Yaxis_deg_R = YaxisPCD_DEG_R - (PCD_X /pixPerdeg);
        
        subplot(1,3,1)
        %Plot Cone density around peak cone density
        
        plot(normPCD_Yaxis_deg,mean(hozrPCD_col), 'r');%horizontal meridian
        hold on;
        plot(normPCD_Xaxis_deg, mean(vertPCD_row), 'g'); %vertical meridian
        
        hold on;
        
        plot(normPCD_Yaxis_deg_R,mean(hozrPCD_col_R), '--r');%horizontal meridian
        hold on;
        plot(normPCD_Xaxis_deg_R, mean(vertPCD_row_R), '.-g'); %vertical meridian
        
        title('Cone Density centered around PCD')
        xlabel('Degrees Eccentricity')
        ylabel('Cone Density (cones/degree^2)')
        ylim([6000, 18000])
        legend('horizontal', 'vertical')
        
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        subplot(1,3,2)
        % %%% CDC (REINIGERS)
        hozrCDC_col = CD_conePerDegsq2{nfiles}(CDC_Y- 5 : CDC_Y + 5,:);
        vertCDC_row = CD_conePerDegsq2{nfiles}(:,CDC_X- 5 : CDC_X + 5);
        %convert the pixels to degrees (base off image)
        pixX_CDCtrimed = [1:length(vertCDC_row)];
        XaxisCDC_DEG = pixX_CDCtrimed/pixPerdeg;
        pixY_CDCtrimed = [1:length(hozrCDC_col)];
        YaxisCDC_DEG = pixY_CDCtrimed/pixPerdeg;
        
        normPRL_Xaxis_deg = XaxisCDC_DEG - (CDC_Y/pixPerdeg);
        normPRL_Yaxis_deg = YaxisCDC_DEG - (CDC_X/pixPerdeg);
        
        
        %Plot Cone density around peak cone density
        plot(normPRL_Yaxis_deg, mean(hozrCDC_col), 'r'); %horizontal meridian
        hold on;
        plot(normPRL_Xaxis_deg,mean(vertCDC_row'), 'g'); %vertical meridian
        title('Cone Density centered around the CDC')
        xlabel('Degrees Eccentricity')
        ylabel('Cone Density (cones/degree^2)')
        ylim([6000, 18000])
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        subplot(1,3,3)
        %%% PRL (REINIGERS)
        %trim the edges of the matrix
        hozrPRL_col = CD_conePerDegsq2{nfiles}(PRL_Y- 5: PRL_Y + 5,:);
        vertPRL_row = CD_conePerDegsq2{nfiles}(:,PRL_X- 5 : PRL_X + 5)';
        %convert the pixels to degrees (base off image)
        pixX_PRLtrimed = [1:length(vertPRL_row)];
        XaxisPRL_DEG = pixX_PRLtrimed/pixPerdeg;
        pixY_PRLtrimed = [1:length(hozrPRL_col)];
        YaxisPRL_DEG = pixY_PRLtrimed/pixPerdeg;
        
        normPRL_Xaxis_deg = XaxisPRL_DEG - (PRL_Y/pixPerdeg);
        normPRL_Yaxis_deg = YaxisPRL_DEG - (PRL_X/pixPerdeg);
        
        
        %Plot Cone density around peak cone density
        plot(normPRL_Yaxis_deg, mean(hozrPRL_col), 'r'); %horizontal meridian
        hold on;
        plot(normPRL_Xaxis_deg,mean(vertPRL_row), 'g'); %vertical meridian
        title('Cone Density centered around the PRL')
        xlabel('Degrees Eccentricity')
        ylabel('Cone Density (cones/degree^2)')
        ylim([6000, 18000])
        legend('horizontal', 'vertical')
        
    end
end
