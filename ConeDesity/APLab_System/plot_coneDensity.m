
%%imageFlipDate
%% Figures and analysis of Cone Density

function plot_coneDensity(RootPath,DataFileName,PRLFileName,SavePath,pixPerdeg,AplabData,arrayIndex,autoUdateTable)
%EyeCandyImageInverted = datetime(2022,07,01, 'Format','ddMMMyyyy');

CDC_percentile = 20;

imageFlipDate = datetime(2022,08,03); %changed hardware in AO system so Superior retina up.
%     raiseTableDate = datetime(2022,08,20); %raised table and changed 940nm collumated to 680nm.
sessDate = datetime(str2double(DataFileName(end -9:end-6)),str2double(DataFileName(end -4:end-3)),str2double(DataFileName(end -1:end)));


if AplabData
    % Load avgeAreaPerPix
    load([RootPath DataFileName '/avgAreaPerCone_' DataFileName '.mat'])
    % Load PRL file
    load([RootPath DataFileName '/' PRLFileName])
   
    if exist('trialData','var')
        nPRLvids = length(trialData);

        masterRefImage = trialData([trialData.globalRef]).retinalImage;
    else
        vars = who();
        TF = contains(vars, 'shift');
        nPRLvids = length(vars(TF));
    end


    if exist('PRLx','var') == 1
        PRL_X = round(PRLx,0);
        if sessDate<imageFlipDate
            PRL_Y = round(size(CD_conePerDegsq,1) - PRLy,0)-1;
        else
            PRL_Y = round(PRLy,0);
        end

    elseif exist('PRL_all','var') == 1
        PRL_X = round(PRL_all(1),0);
        if sessDate<imageFlipDate
            PRL_Y = round(size(CD_conePerDegsq,1) - PRL_all(2),0)-1;
        else
            PRL_Y = round(PRL_all(2),0);
        end
    else
        %         PRL_X = round(PRL_x,0);
        %         PRL_Y = round(size(avgAreaPerCone,1) - PRL_y,0);
    end

else
    %for Reiniger et al 2021 data
    load([RootPath DataFileName]);

    %load refimage and PRL CDC PCD
    load(PRLFileName)

    PRL_X = round(PRL_x,0);
    PRL_Y = round(PRL_y,0);


end


%%% Get Peak Cone Density
%getting the maximum cones to compare to reinigers peak cone densitys
[PCD  PCD_index]= max(max(CD_conePerDegsq));
big_idx = find(CD_conePerDegsq == PCD );
[oursPCD_y,oursPCD_x] = ind2sub(size(CD_conePerDegsq),big_idx);

APC_Prctile = prctile(avgAreaPerCone,CDC_percentile,1);
threshCDC = prctile(APC_Prctile,CDC_percentile,2);

APC = avgAreaPerCone ;

APC(APC<threshCDC)=1;
APC(APC>=threshCDC)=0;

measurements = regionprops(APC, refImage, 'WeightedCentroid');
centerOfMass = measurements.WeightedCentroid;
oursCDC_x = centerOfMass(1);
oursCDC_y = centerOfMass(2);
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
CD_nyq = ((1/2)*sqrt((2/sqrt(3)).*CD_conePerDegsq));



%%

% if AplabData
figure;imshow(APC);
hold on;
plot(oursCDC_x, oursCDC_y, 'r*', 'LineWidth', 2, 'MarkerSize', 16);
% end


%https://www.mathworks.com/help/thingspeak/create-heatmap-overlay-image.html
figure;
image(refImage);
hold on
OverlayImage = imagesc(CD_conePerDegsq,[min(CD_conePerDegsq(:)) max(CD_conePerDegsq(:))]);
%set(gca,'xticklabel',xt,'yticklabel',yt)

% xticklabels([1:size(CD_conePerDegsq,2)/6:size(CD_conePerDegsq,2)]/pixPerdeg)
hold on
alpha(0.3)
colorbar;
hold on

if AplabData
    a=plot(oursPCD_x,oursPCD_y,'^k'); hold on;
    b=plot(PRL_X,PRL_Y,'or'); hold on;
    c=plot(oursCDC_x,oursCDC_y,'*b'); hold on;
    halfFrame =   (.85*pixPerdeg)/2;
    xlim([PRL_X-halfFrame PRL_X + halfFrame])
    ylim([PRL_Y-halfFrame PRL_Y + halfFrame])

    caxis([0 15000])
    legend(sprintf('%s\n PCD',DataFileName),'PRL','CDC')

    text(PRL_X-50, PRL_Y + 250,'Inferior Retina')
    text(PRL_X-50, PRL_Y - 250,'Superior Retina')
    text(PRL_X + 200, PRL_Y, 'Nasal Retina')
    text(PRL_X - 300, PRL_Y, 'Temporal Retina')

    ws = (10/60)*pixPerdeg;    % 10 arcmin
    szI = size(refImage);
    hs = 20;
    px = (PRL_X - 300)+20;                % position in x
    py =(PRL_Y + 300) - 20 - hs;   % position in y
    r = rectangle('Position',[px,py,ws,hs]);
    r.FaceColor = [1 1 1];
    t = text(px,py-1.5*hs,'10 arcmin');
    t.Color = [1 1 1];

    %     saveas(gcf,sprintf('%sOverlayed_HeatMap_%s',SavePath, DataFileName), 'fig')
    %     saveas(gcf,sprintf('%sOverlayed_HeatMap_%s',SavePath,DataFileName), 'png')
    %     saveas(gcf,sprintf('%s%s/Overlayed_HeatMap_%s',RootPath,DataFileName, DataFileName), 'fig')
    %     saveas(gcf,sprintf('%s%s/Overlayed_HeatMap_%s',RootPath,DataFileName,DataFileName), 'png')
else

    hold on;
    plot(CDC_x,CDC_y,'.r', 'MarkerSize', 5); hold on;% I flipped this because the point was the max max of the matrix, thus needed to be rotated to be ontop of an image
    plot(PCD_x,PCD_y,'^k', 'MarkerSize', 5); hold on;
    plot(PRL_x,PRL_y,'sk', 'MarkerSize', 5); hold on;
    plot(oursPCD_x,oursPCD_y,'g.', 'MarkerSize', 20); hold on;

end



figure;
image(refImage);
hold on
OverlayImage = imagesc(CD_conePerDegsq,[min(CD_conePerDegsq(:)) max(CD_conePerDegsq(:))]);
%set(gca,'xticklabel',xt,'yticklabel',yt)

% xticklabels([1:size(CD_conePerDegsq,2)/6:size(CD_conePerDegsq,2)]/pixPerdeg)
hold on
alpha(0.3)
colorbar;
hold on

for ii = 1:nPRLvids

    if exist('trialData','var')
        PRLpt = trialData(ii).PRL;
        PRL_ptind = trialData(ii).locs;
    else
        if sessDate<imageFlipDate
            prl = eval(sprintf('PRL%i', ii))+ eval(sprintf('shift%i', ii)) ;    
            locs = eval(sprintf('locs%i', ii))+ eval(sprintf('shift%i', ii)) ;
            prly = (size(CD_conePerDegsq,1) - prl(2))+1;
            locsy = (size(CD_conePerDegsq,1) - locs(:,2))+1; % this doesn't count for the size of the image

            PRLpt = [prl(1) prly];
            PRL_ptind= [locs(:,1) locsy];
        else
            PRLpt = eval(sprintf('PRL%i', ii)) + eval(sprintf('shift%i', ii));
            PRL_ptind= eval(sprintf('locs%i', ii)) + eval(sprintf('shift%i', ii));
        end

    end
    PRL_ptx = ((PRL_ptind(:,1)- oursCDC_x)/pixPerdeg)*60;
    PRL_pty = ((oursCDC_y -PRL_ptind(:,2))/pixPerdeg)*60;
    %     ptshift = eval(sprintf('shift%i', ii));
    %     PRLloc = PRLpt + ptshift;
    PRLloc_all(ii,:) = PRLpt;
    PRLind_0CDC{ii} = [PRL_ptx,PRL_pty] ;
    PRLind_arc{ii} = (PRL_ptind/pixPerdeg)*60;
    plot(PRLpt(1), PRLpt(2), '.c', 'Markersize', 10);hold on;

end
plot(PRL_X, PRL_Y, 'go', 'MarkerSize',10 );hold on;
plot(oursCDC_x, oursCDC_y, 'b*', 'MarkerSize',10 );hold on;


%%% Figure for CDC- PRL distance
distPRLCDC_pix = sqrt((oursCDC_x - PRLloc_all(:,1)).^2+ (oursCDC_y-PRLloc_all(:,2)).^2);
PRLCDCdist_all = (distPRLCDC_pix/pixPerdeg)*60;

xPRLall_0CDC = ((PRLloc_all(:,1) - oursCDC_x)/pixPerdeg)*60;%switch order of opperationsbecause image origin is top left and the plotting is
% bottom right.
yPRLall_0CDC = ((oursCDC_y - PRLloc_all(:,2))/pixPerdeg)*60;

CI95 = tinv([0.025 0.975], length(yPRLall_0CDC));
xPRLavg_0CDC = mean(xPRLall_0CDC);
yPRLavg_0CDC = mean(yPRLall_0CDC);
xPRL_CI_0CDC = (std(xPRLall_0CDC)/sqrt(length(xPRLall_0CDC)))*CI95;
yPRL_CI_0CDC = (std(yPRLall_0CDC)/sqrt(length(yPRLall_0CDC)))*CI95;

figure;
a=plot(xPRLall_0CDC,yPRLall_0CDC, 'r.', 'MarkerSize', 10);hold on;
b=plot(0,0, '*b', 'MarkerSize', 10);hold on;
c = errorbar(xPRLavg_0CDC,yPRLavg_0CDC,xPRL_CI_0CDC(1),xPRL_CI_0CDC(2), yPRL_CI_0CDC(1),yPRL_CI_0CDC(2), 'k.','MarkerSize', 12 );hold on;
xlim([-15 15])
ylim([-15 15])
xlabel('arcmin')
ylabel('arcmin')
text(-3, -14,'Inferior Retina')
text(-4, 14,'Superior Retina')
text(9, 0, 'Nasal Retina')
text(-14, 0, 'Temporal Retina')
legend([a, b,c], 'PRL trials','CDC', sprintf(' Avg PRL \n nTrials = %i', length(xPRLall_0CDC)))

figure;
a=plot(0,0, '*b', 'MarkerSize', 10);hold on;
cmapRange = colormap(jet(length(PRLind_0CDC)));
for ss = 1:length(PRLind_0CDC)
    plot(PRLind_0CDC{ss}(:,1), PRLind_0CDC{ss}(:,2) ,'.','MarkerFaceColor',cmapRange(ss,:),'MarkerEdgeColor',cmapRange(ss,:)); hold on;
    plot(xPRLall_0CDC(ss),yPRLall_0CDC(ss),  'square','MarkerFaceColor',cmapRange(ss,:), 'MarkerEdgeColor',cmapRange(ss,:),'MarkerSize', 10);hold on;
    ellipseXY(PRLind_0CDC{ss}(:,1), PRLind_0CDC{ss}(:,2), 80, cmapRange(ss,:), 0);hold on;
%     input ''
end

% test = [];
% for ss = 1:length(PRLind_0CDC)
% 
% test = [test PRLind_0CDC{ss}'];
% end
% test'
% 
% 
% 
%     xedges = linspace(-20,20,100); yedges =  linspace(-20,20,100);
%     histmat  = hist2(test(1,:), test(2,:),...
%         xedges, yedges);
%     figure; pcolor(xedges,yedges,histmat'); colorbar ; axis square tight ;
%     figure; contour(histmat)





%%% PCD %%%
hozrPCD_col = CD_conePerDegsq(oursPCD_y - 5: oursPCD_y + 5,:);
vertPCD_row = CD_conePerDegsq(:,oursPCD_x-5:oursPCD_x + 5)';
%convert the pixels to degrees (base off image)
pixX_PCDtrimed = [1:length(vertPCD_row)];
XaxisPCD_DEG = pixX_PCDtrimed/pixPerdeg;
pixY_PCDtrimed = [1:length(hozrPCD_col)];
YaxisPCD_DEG = pixY_PCDtrimed/pixPerdeg;
normPCD_Xaxis_deg = XaxisPCD_DEG - (oursPCD_y/pixPerdeg);
normPCD_Yaxis_deg = YaxisPCD_DEG - (oursPCD_x /pixPerdeg);


%%% CDC %%%
hozrCDC_col = CD_conePerDegsq(oursCDC_y - 5: oursCDC_y + 5,:);
vertCDC_row = CD_conePerDegsq(:,oursCDC_x-5:oursCDC_x + 5)';
%convert the pixels to degrees (base off image)
pixX_CDCtrimed = [1:length(vertCDC_row)];
XaxisCDC_DEG = pixX_CDCtrimed/pixPerdeg;
pixY_CDCtrimed = [1:length(hozrCDC_col)];
YaxisCDC_DEG = pixY_CDCtrimed/pixPerdeg;
normCDC_Xaxis_deg = XaxisCDC_DEG - (oursCDC_y/pixPerdeg);
normCDC_Yaxis_deg = YaxisCDC_DEG - (oursCDC_x /pixPerdeg);


%%% PRL %%%
hozrPRL_col = CD_conePerDegsq(PRL_Y- 5: PRL_Y + 5,:);
vertPRL_row = CD_conePerDegsq(:,PRL_X- 5 : PRL_X + 5)';
%convert the pixels to degrees (base off image)
pixX_PRLtrimed = [1:length(vertPRL_row)];
XaxisPRL_DEG = pixX_PRLtrimed/pixPerdeg;
pixY_PRLtrimed = [1:length(hozrPRL_col)];
YaxisPRL_DEG = pixY_PRLtrimed/pixPerdeg;

normPRL_Xaxis_deg = XaxisPRL_DEG - (PRL_Y/pixPerdeg);
normPRL_Yaxis_deg = YaxisPRL_DEG - (PRL_X/pixPerdeg);

figure;
% plot(normPCD_Yaxis_deg,mean(hozrPCD_col), 'r');%horizontal meridian
hold on;
plot(normPCD_Xaxis_deg*60, mean(vertPCD_row), 'k'); %vertical meridian
hold on;
plot(normPRL_Xaxis_deg*60,mean(vertPRL_row), 'c');
legend('PCD', 'PRL')

text(-25,15000,'Superior Retina')
text(15,15000,'Inferior Retina')

title('Cone Density centered around PCD')
xlabel('Eccentricity (arcmin)')
ylabel('Cone Density (cones/degree^2)')
ylim([6000, 18000])



figure;
% plot(normPCD_Yaxis_deg,mean(hozrPCD_col), 'r');%horizontal meridian
hold on;
plot(normCDC_Yaxis_deg*60, mean(hozrCDC_col), 'r'); %horizontal meridian
hold on;
plot(normPRL_Yaxis_deg*60,mean(hozrPRL_col), 'c');
legend('CDC', 'PRL')

text(-25,15000,'Temporal Retina')
text(15,15000,'Nasal Retina')

title('Cone Density centered around PCD')
xlabel('Eccentricity (arcmin)')
ylabel('Cone Density (cones/degree^2)')
ylim([6000, 18000])


figure;
% plot(normPCD_Yaxis_deg,mean(hozrPCD_col), 'r');%vertical meridian
hold on;
plot(normCDC_Xaxis_deg*60, mean(vertCDC_row), 'r'); %vertical meridian
hold on;
plot(normPRL_Xaxis_deg*60,mean(vertPRL_row), 'c');
legend('CDC', 'PRL')

text(-25,15000,'Superior Retina')
text(15,15000,'Inferior Retina')

title('Cone Density centered around PCD')
xlabel('Eccentricity (arcmin)')
ylabel('Cone Density (cones/degree^2)')
ylim([6000, 18000])


%% general information:

PRL_CD = CD_conePerDegsq(PRL_Y,PRL_X);
CDC_CD = CD_conePerDegsq(round(oursCDC_y,0),round(oursCDC_x,0));

%euclidian distance with pix to arcminute converstion.
PRL_PCDdist = (sqrt((PRL_X - oursPCD_x)^2 + (PRL_Y - oursPCD_y)^2))/(pixPerdeg/60);
PRL_CDCdist = (sqrt((PRL_X - oursCDC_x)^2 + (PRL_Y - oursCDC_y)^2))/(pixPerdeg/60);

PCD_CDCdist = (sqrt((oursPCD_x - oursCDC_x)^2 + (oursPCD_y - oursCDC_y)^2))/(pixPerdeg/60);
SubName = string(DataFileName);
T = table(SubName,PCD, oursPCD_x, oursPCD_y, CDC_CD,round(oursCDC_x,0),round(oursCDC_y,0),PRL_CD,PRL_X,PRL_Y, ...
    PRL_PCDdist,PRL_CDCdist,PCD_CDCdist,pixPerdeg,...
    'VariableNames', {'Subject','PCD ConeDensity','PCD X', 'PCD Y', 'CDC ConeDensity','CDC X','CDC Y', 'PRL ConeDensity','PRL X','PRL Y'...
    'PRL-PCD (arc)', 'PRL - CDC(arc)', 'PCD - CDC(arc)','pixPerdeg'})

if AplabData
    save(sprintf('%sXC/%s_CD_data_PJ.mat',RootPath,SubName), 'SubName','refImage','avgAreaPerCone' ,'CD_conePerDegsq','CD_nyq','ConeCoord','PCD', 'oursPCD_x', 'oursPCD_y', 'CDC_CD',...
        'oursCDC_x','oursCDC_y','PRL_CD','PRL_X','PRL_Y','PRLloc_all','PRLind_0CDC','PRLind_arc', ...
        'PRL_PCDdist','PRL_CDCdist','PCD_CDCdist','pixPerdeg')
    
%     mkdir(SavePath)

    save(sprintf('%s%s_CD_data_PJ.mat',SavePath,SubName), 'SubName','refImage','avgAreaPerCone' ,'CD_conePerDegsq','CD_nyq','ConeCoord','PCD', 'oursPCD_x', 'oursPCD_y', 'CDC_CD',...
        'oursCDC_x','oursCDC_y','PRL_CD','PRL_X','PRL_Y','PRLloc_all','PRLind_0CDC','PRLind_arc', ...
        'PRL_PCDdist','PRL_CDCdist','PCD_CDCdist','pixPerdeg')
else

    save(sprintf('%sXC/%s_CD_data_PJ.mat',RootPath(1:end-23),DataFileName(16:end-4)), 'SubName','refImage','avgAreaPerCone' ,'CD_conePerDegsq', 'CD_nyq','PCD', 'oursPCD_x', 'oursPCD_y', 'CDC_CD',...
        'oursCDC_x','oursCDC_y','PRL_CD','PRL_X','PRL_Y', ...
        'PRL_PCDdist','PRL_CDCdist','PCD_CDCdist','pixPerdeg')
end
if AplabData
    fprintf('\n\nPeak Cone density: X = %i; Y = %i;\n', oursPCD_x, oursPCD_y)
    fprintf('Cone Density Centroid: X = %i; Y = %i;\n', round(oursCDC_x,0), round(oursCDC_y,0))
    fprintf('Preferred Locus of Fixation: X = %i; Y = %i;\n', PRL_X, PRL_Y)


    T = table(string(DataFileName),PCD, oursPCD_x, oursPCD_y, CDC_CD,round(oursCDC_x,0),round(oursCDC_y,0),PRL_CD,PRL_X,PRL_Y, ...
        PRL_PCDdist,PRL_CDCdist,PCD_CDCdist, pixPerdeg,...
        'VariableNames', {'Subject','PCD ConeDensity','PCD X', 'PCD Y', 'CDC ConeDensity','CDC X','CDC Y', 'PRL ConeDensity','PRL X','PRL Y',...
        'PRL-PCD (arc)', 'PRL - CDC(arc)', 'PCD - CDC(arc)','pixPerdeg'});

    if autoUdateTable
        % Write data to text file
        writetable(T,[SavePath DataFileName '_PCD_CDC_PRL_locs_PJ.txt'],'Delimiter','\t','WriteRowNames',true);
        writetable(T,[RootPath  DataFileName '/' DataFileName '_PCD_CDC_PRL_locs.txt'],'Delimiter','\t','WriteRowNames',true);

        filename = 'C:\Users\sjenks\Box\APLab-Projects\AO\Data_SummarizedSubject\SummarizedData.xlsx';
        % filename ='C:\Users\sjenks\Box\APLab-Projects\AO\ConeDensity\Test_matlabUpdate.xlsx';
        writetable(T,filename,'Sheet',3,'Range',sprintf('B%i',2+arrayIndex),'WriteVariableNames',0)
    end


    arcEccen = [0, 10, 15 20 25];% in arc min
    arcradii = 2.5;%half the length of box in arc minutes
    [ConeDensity_ecc,tableEccen_PRL,tableEccen_CDC,tableEccen_Nyqist_PRL,tableEccen_Nyqist_CDC] = EccenConeDensity(CD_conePerDegsq,CD_nyq,arcEccen,arcradii,pixPerdeg,PRL_X,PRL_Y, oursCDC_x, oursCDC_y)

if AplabData
    save(sprintf('%s%s_acrossEccen_PJ.mat',SavePath,SubName), 'ConeDensity_ecc','tableEccen_PRL', 'tableEccen_CDC', 'tableEccen_Nyqist_PRL','tableEccen_Nyqist_CDC' );
    save(sprintf('%s%s_acrossEccen_PJ.mat',RootPath,SubName), 'ConeDensity_ecc','tableEccen_PRL', 'tableEccen_CDC', 'tableEccen_Nyqist_PRL','tableEccen_Nyqist_CDC' );
    save(sprintf('%sXC/%s_acrossEccen_PJ.mat',RootPath,SubName), 'ConeDensity_ecc','tableEccen_PRL', 'tableEccen_CDC', 'tableEccen_Nyqist_PRL','tableEccen_Nyqist_CDC' );
end

    tableEccen_PRL.Properties.DimensionNames{1} = DataFileName;
    tableEccen_CDC.Properties.DimensionNames{1} = DataFileName;
    if arrayIndex == 1
        index = 0;
    else
        index = (arrayIndex-1);
    end

    if autoUdateTable
        writetable(tableEccen_PRL,filename,'Sheet',4,'Range',sprintf('B%i',(index * 6) + 3 ),'WriteVariableNames',1,'WriteRowNames',true)
        writetable(tableEccen_CDC,filename,'Sheet',4,'Range',sprintf('I%i',(index *6)+3),'WriteVariableNames',1,'WriteRowNames',true)
    end
end





%%




