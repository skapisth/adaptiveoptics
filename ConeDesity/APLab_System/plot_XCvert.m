
clear;clc;close all


%% Plot Distributions along the Verticle meridian
dirPath ='C:\Users\ruccilab\Documents\adaptiveoptics\ConeDesity\APLab_System\data\XC\';


areaList = dir(sprintf('%s*_CD_Data.mat',dirPath));
pixPerdeg = 512;

areaList(~[1:3,7:10, 13:14]) = [];
% areaList([5,6,8,12,13]) = [];% removes Z160 and Z055
% areaList([4,5,6,8,11,12,13,15]) = [];% removes all duplicates from good one. 




for nfiles = 1:length(areaList)
    Data{nfiles}= load([dirPath areaList(nfiles).name]);
    sRef(nfiles, :) = size(Data{nfiles}.refImage);

    vert_rowAxis{nfiles} = (1:size(Data{nfiles}.refImage,1))/pixPerdeg;
    horz_colAxis{nfiles} =(1:size(Data{nfiles}.refImage,2))/pixPerdeg;

end


%%% setting up the plots
if rem(max(max(sRef)),2) ==0
    ArraySize = max(max(sRef))+501;%must be an odd number
else
    ArraySize = max(max(sRef))+500;
end
nFront = 100;
pixPerdeg = 512;
centerPT = ceil(ArraySize/2);




axisR_vertPRL= NaN(length(areaList),ArraySize);
axisR_horzPRL = NaN(length(areaList),ArraySize);
axisR_vertPCD = NaN(length(areaList),ArraySize);
axisR_horzPCD = NaN(length(areaList),ArraySize);
axisR_vertCDC = NaN(length(areaList),ArraySize);
axisR_horzCDC = NaN(length(areaList),ArraySize);

valR_vertPRL= NaN(length(areaList),ArraySize);
valR_horzPRL = NaN(length(areaList),ArraySize);
valR_vertPCD = NaN(length(areaList),ArraySize);
valR_horzPCD = NaN(length(areaList),ArraySize);
valR_vertCDC = NaN(length(areaList),ArraySize);
valR_horzCDC = NaN(length(areaList),ArraySize);



for nfiles = 1:length(areaList)
    PCD_x_all = Data{nfiles}.oursPCD_x;
    PCD_y_all = Data{nfiles}.oursPCD_y;
    PRL_x_all = Data{nfiles}.PRL_X;
    PRL_y_all = Data{nfiles}.PRL_Y;
    CDC_x_all = Data{nfiles}.oursCDC_x;
    CDC_y_all = Data{nfiles}.oursCDC_y;

    % make avgVertPRCD and normaVertPCD
    avg_hozrPCD_col = mean(Data{nfiles}.CD_conePerDegsq(PCD_y_all - 5: PCD_y_all + 5,:));
    avg_vertPCD_row = mean(Data{nfiles}.CD_conePerDegsq(:,PCD_x_all-5:PCD_x_all + 5)');
    avg_hozrCDC_col = mean(Data{nfiles}.CD_conePerDegsq(CDC_y_all - 5: CDC_y_all + 5,:));
    avg_vertCDC_row = mean(Data{nfiles}.CD_conePerDegsq(:,CDC_x_all-5:CDC_x_all + 5)');
    avg_hozrPRL_col = mean(Data{nfiles}.CD_conePerDegsq(PRL_y_all - 5: PRL_y_all + 5,:));
    avg_vertPRL_row = mean(Data{nfiles}.CD_conePerDegsq(:,PRL_x_all-5:PRL_x_all + 5)');

    norm_vertPRL_rowAxis{nfiles} = (vert_rowAxis{nfiles}' - (floor(PRL_y_all)/pixPerdeg))*60;
    axisR_vertPRL(nfiles,(centerPT - PRL_y_all)+1 : centerPT+(sRef(nfiles,1) - PRL_y_all)) = norm_vertPRL_rowAxis{nfiles};
    valR_vertPRL(nfiles,(centerPT - PRL_y_all)+1 : centerPT+(sRef(nfiles,1) - PRL_y_all))  = avg_vertPRL_row;

    norm_horzPRL_colAxis{nfiles} = (horz_colAxis{nfiles}' - (floor(PRL_x_all)/pixPerdeg))*60;
    axisR_horzPRL(nfiles,(centerPT - PRL_x_all)+1 : centerPT+(sRef(nfiles,2) - PRL_x_all)) = norm_horzPRL_colAxis{nfiles};
    valR_horzPRL(nfiles,(centerPT - PRL_x_all)+1 : centerPT+(sRef(nfiles,2) - PRL_x_all)) = avg_hozrPRL_col;

    norm_vertPCD_rowAxis{nfiles} = (vert_rowAxis{nfiles}' - (floor(PCD_y_all)/pixPerdeg))*60;%x
    axisR_vertPCD(nfiles,(centerPT - PCD_y_all)+1 : centerPT+(sRef(nfiles,1) - PCD_y_all)) = norm_vertPCD_rowAxis{nfiles};
    valR_vertPCD(nfiles,(centerPT - PCD_y_all)+1 : centerPT+(sRef(nfiles,1) - PCD_y_all)) = avg_vertPCD_row;

    norm_horzPCD_colAxis{nfiles} = (horz_colAxis{nfiles}' - (floor(PCD_x_all)/pixPerdeg))*60;
    axisR_horzPCD(nfiles,(centerPT - PCD_x_all)+1 : centerPT+(sRef(nfiles,2) - PCD_x_all)) = norm_horzPCD_colAxis{nfiles};
    valR_horzPCD(nfiles,(centerPT - PCD_x_all)+1 : centerPT+(sRef(nfiles,2) - PCD_x_all)) = avg_hozrPCD_col;

    norm_vertCDC_rowAxis{nfiles} = (vert_rowAxis{nfiles}' - (floor(CDC_y_all)/pixPerdeg))*60;
    axisR_vertCDC(nfiles,(centerPT - CDC_y_all)+1 : centerPT+(sRef(nfiles,1) - CDC_y_all)) = norm_vertCDC_rowAxis{nfiles};
    valR_vertCDC(nfiles,(centerPT - CDC_y_all)+1 : centerPT+(sRef(nfiles,1) - CDC_y_all)) = avg_vertCDC_row;

    norm_horzCDC_colAxis{nfiles} = (horz_colAxis{nfiles}' - (floor(CDC_x_all)/pixPerdeg))*60;
    axisR_horzCDC(nfiles,(centerPT - CDC_x_all)+1 : centerPT+(sRef(nfiles,2) - CDC_x_all)) = norm_horzCDC_colAxis{nfiles};
    valR_horzCDC(nfiles,(centerPT - CDC_x_all)+1 : centerPT+(sRef(nfiles,2) - CDC_x_all)) = avg_hozrCDC_col;


end

CI95_R = tinv([0.025 0.975], (length(areaList))-1);
PCDv_CI = CI95_R(2) *(std(avg_vertPCD_row)/sqrt(size(areaList,1)));
PRLv_CI = CI95_R(2) *(std(avg_vertCDC_row)/sqrt(size(areaList,1)));
CDCv_CI = CI95_R(2) *(std(avg_vertPRL_row)/sqrt(size(areaList,1)));





PRL_hva = nanmean(valR_horzPRL - valR_vertPRL);
PRL_hvaCI = CI95_R(2) *(std(valR_horzPRL - valR_vertPRL)/sqrt(size(valR_vertPRL,1)));

CDC_hva = nanmean(valR_horzCDC - valR_vertCDC);
CDC_hvaCI = CI95_R(2) *(std(valR_horzCDC - valR_vertCDC)/sqrt(size(valR_vertCDC,1)));


PRL_vma = nanmean(valR_vertPRL(:,centerPT: end)) - flip(nanmean(valR_vertPRL(:, 1:centerPT),1));
PRL_vmaCI = CI95_R(2)*((std(valR_vertPRL(:,centerPT: end)) - flip(std(valR_vertPRL(:, 1:centerPT),1)))/sqrt(size(valR_vertPRL,1)/2));


CDC_vma = nanmean(valR_vertCDC(:,centerPT: end)) - flip(nanmean(valR_vertCDC(:, 1:centerPT),1));
CDC_vmaCI = CI95_R(2)*((std(valR_vertCDC(:,centerPT: end)) - flip(std(valR_vertCDC(:, 1:centerPT),1)))/sqrt(size(valR_vertCDC,1)/2));



%%


figure;
a = plot(nanmean(axisR_vertPRL),nanmean(valR_vertPRL),'g');hold on;
plot(nanmean(axisR_vertPRL),nanmean(valR_vertPRL)+PRLv_CI, 'g-.');hold on;
plot(nanmean(axisR_vertPRL),nanmean(valR_vertPRL)-PRLv_CI, 'g-.');hold on;

% b = plot(nanmean(axisR_vertPCD),nanmean(valR_vertPCD),'k');hold on;
% plot(nanmean(axisR_vertPCD),nanmean(valR_vertPCD) + PCDv_CI, 'k-.');hold on;
% plot(nanmean(axisR_vertPCD),nanmean(valR_vertPCD) - PCDv_CI,'k-.');hold on;


c = plot(nanmean(axisR_vertCDC),nanmean(valR_vertCDC),'b');hold on;
plot(nanmean(axisR_vertCDC),nanmean(valR_vertCDC)+CDCv_CI, 'b-.');hold on;
plot(nanmean(axisR_vertCDC),nanmean(valR_vertCDC)-CDCv_CI, 'b-.');hold on;

xlim([-30, 30])
ylim([9000, 16000])
xlabel('Eccentricity wrt PRL (arc)')
ylabel('Cone density (cones/deg^2)')
legend([a ,b, c], 'PRL', 'PCD', 'CDC')
yyaxis right
ylim(((1/2)*sqrt((2/sqrt(3)).*[9000, 16000])))
ylabel('Nyquist (cycles/deg)')
title('Upper vs Lower Vertical Meridian')
ax = gca;
ax.YAxis(2).Color = [0 .7 0.5];



figure;
a = plot(nanmean(axisR_vertPRL),nanmean(valR_vertPRL),'g');hold on;
plot(nanmean(axisR_vertPRL),nanmean(valR_vertPRL)+PRLv_CI, 'g-');hold on;
plot(nanmean(axisR_vertPRL),nanmean(valR_vertPRL)-PRLv_CI, 'g-');hold on;

xlim([-30, 30])
ylim([9000, 16000])
xlabel('Eccentricity wrt PRL (arc)')
ylabel('Cone density (cones/deg^2)')
title('Upper vs Lower Vertical Meridian')
yyaxis right
ylim(((1/2)*sqrt((2/sqrt(3)).*[9000, 16000])))
ylabel('Nyquist (cycles/deg)')
ax = gca;
ax.YAxis(2).Color = [0 .7 0.5];


figure;
b = plot(nanmean(axisR_vertPCD),nanmean(valR_vertPCD),'k');hold on;
plot(nanmean(axisR_vertPCD),nanmean(valR_vertPCD) + PCDv_CI, 'k-');hold on;
plot(nanmean(axisR_vertPCD),nanmean(valR_vertPCD) - PCDv_CI,'k-');hold on;

xlim([-30, 30])
ylim([9000, 16000])
xlabel('Eccentricity wrt PCD (arc)')
ylabel('Cone density (cones/deg^2)')
title('Upper vs Lower Vertical Meridian')
yyaxis right
ylim(((1/2)*sqrt((2/sqrt(3)).*[9000, 16000])))
ylabel('Nyquist (cycles/deg)')
ax = gca;
ax.YAxis(2).Color = [0 .7 0.5];



figure;
c = plot(nanmean(axisR_vertCDC),nanmean(valR_vertCDC),'b');hold on;
plot(nanmean(axisR_vertCDC),nanmean(valR_vertCDC)+CDCv_CI, 'b-');hold on;
plot(nanmean(axisR_vertCDC),nanmean(valR_vertCDC)-CDCv_CI, 'b-');hold on;

xlim([-30, 30])
ylim([9000, 16000])
xlabel('Eccentricity wrt CDC(arc)')
ylabel('Cone density (cones/deg^2)')
yyaxis right
ylim(((1/2)*sqrt((2/sqrt(3)).*[9000, 16000])))
ylabel('Nyquist (cycles/deg)')
ax = gca;
ax.YAxis(2).Color = [0 .7 0.5];


figure;
a = plot(nanmean(axisR_vertPRL),nanmean(valR_horzPRL),'g');hold on;
plot(nanmean(axisR_vertPRL),nanmean(valR_horzPRL)+PRLh_CI, 'g-.');hold on;
plot(nanmean(axisR_vertPRL),nanmean(valR_horzPRL)-PRLv_CI, 'g-.');hold on;

% b = plot(nanmean(axisR_vertPCD),nanmean(valR_vertPCD),'k');hold on;
% plot(nanmean(axisR_vertPCD),nanmean(valR_vertPCD) + PCDv_CI, 'k-.');hold on;
% plot(nanmean(axisR_vertPCD),nanmean(valR_vertPCD) - PCDv_CI,'k-.');hold on;


c = plot(nanmean(axisR_vertCDC),nanmean(valR_vertCDC),'b');hold on;
plot(nanmean(axisR_vertCDC),nanmean(valR_vertCDC)+CDCv_CI, 'b-.');hold on;
plot(nanmean(axisR_vertCDC),nanmean(valR_vertCDC)-CDCv_CI, 'b-.');hold on;

xlim([-30, 30])
ylim([9000, 16000])
xlabel('Eccentricity wrt PRL (arc)')
ylabel('Cone density (cones/deg^2)')
legend([a ,b, c], 'PRL', 'PCD', 'CDC')
yyaxis right
ylim(((1/2)*sqrt((2/sqrt(3)).*[9000, 16000])))
ylabel('Nyquist (cycles/deg)')
title('Upper vs Lower Vertical Meridian')
ax = gca;
ax.YAxis(2).Color = [0 .7 0.5];




%%%%% individual plots VM %%%%%%%

figure;
for nfiles = 1:length(areaList)
    plot(axisR_vertPRL(nfiles,:),valR_vertPRL(nfiles,:));hold on;

end
title('PRL centered')
xlim([-30, 30])
ylim([9000, 18000])
xlabel('Eccentricity wrt PRL(arc)')
ylabel('Cone density (cones/deg^2)')
yyaxis right
ylim(((1/2)*sqrt((2/sqrt(3)).*[9000, 16000])))
ylabel('Nyquist (cycles/deg)')
ax = gca;
ax.YAxis(2).Color = [0 .7 0.5];

figure;
for nfiles = 1:length(areaList)
    plot(axisR_vertPCD(nfiles,:),valR_vertPCD(nfiles,:));hold on;

end
title('PCD centered')
% xlim([-20, 20])
ylim([9000, 18000])
xlabel('Eccentricity wrt PCD(arc)')
ylabel('Cone density (cones/deg^2)')
yyaxis right
ylim(((1/2)*sqrt((2/sqrt(3)).*[9000, 16000])))
ylabel('Nyquist (cycles/deg)')
ax = gca;
ax.YAxis(2).Color = [0 .7 0.5];

figure;
for nfiles = 1:length(areaList)
    plot(axisR_vertCDC(nfiles,:),valR_vertCDC(nfiles,:));hold on;
%     plot(axisR_horzCDC(nfiles,:),valR_horzCDC(nfiles,:),'r');hold on;
   
end
legend('vertical', 'horizontal')
title('CDC centered')
% xlim([-20, 20])
ylim([9000, 16000])
xlabel('Eccentricity wrt CDC(arc)')
ylabel('Cone density (cones/deg^2)')
yyaxis right
ylim(((1/2)*sqrt((2/sqrt(3)).*[9000, 16000])))
ylabel('Nyquist (cycles/deg)')
ax = gca;
ax.YAxis(2).Color = [0 .7 0.5];

