%function plotAOImages(pathName,dataAll)

% added these definitions to run this as a script rather than a fuction
% during testing (remove before converting back to function)
temp = load('Z091_EyeData_MiniScot.mat');
pathName = ('Z190_R_2022_06_03_CD_data.mat');
dataAll = temp.dataAll{1}; 

% coneMap = load('C:\Users\Ruccilab\Downloads\Z190_R_2022_06_03_CD_data.mat');
coneMap = load(pathName);

% make axes that are centered on the PRL determined from the cone image
[h,w] = size(coneMap.refImage); % height and width in pixels
xPix = (1:1:w);                 % pixel coordinates in x
yPix = (1:1:h);                 % pixel coordinates in y
% center on PRL and convert to arcmin
x = (xPix - coneMap.PRL_X)*(60/coneMap.pixPerdeg);
y = (yPix - coneMap.PRL_Y)*(60/coneMap.pixPerdeg);

% find other retinal loci and transform to PRL-centered axes
PRL_Xarcmin = 0;    % zero by definition of our axes (PRL at origin)
PRL_Yarcmin = 0;    % zero by definition of our axes (PRL at origin)
CDC_Xarcmin = (coneMap.oursCDC_x - coneMap.PRL_X)*(60/coneMap.pixPerdeg);
CDC_Yarcmin = (coneMap.oursCDC_y - coneMap.PRL_Y)*(60/coneMap.pixPerdeg);
PCD_Xarcmin = (coneMap.oursPCD_x - coneMap.PRL_X)*(60/coneMap.pixPerdeg);
PCD_Yarcmin = (coneMap.oursPCD_y - coneMap.PRL_Y)*(60/coneMap.pixPerdeg);

% define limits and axes for histogram and plotting
xMax = 40;      % maximum value in x (arcmin)
yMax = 40;      % maximum value in y (arcmin)
binWidth = 1;   % bin width for histogram (arcmin)
xHist = (-xMax:binWidth:xMax);  % x values for histogram bin centers
yHist = (-yMax:binWidth:yMax);  % y values for histogram bin centers
[XH,YH] = meshgrid(xHist,yHist);    % generate grid of x and y points
xEdges = xHist-binWidth/2;      % x values for histogram bin edges
xEdges(end+1) = xEdges(end)+binWidth;   % extra value for last edge
yEdges = yHist-binWidth/2;      % y values for histogram bin edges
yEdges(end+1) = yEdges(end)+binWidth;   % extra value for last edge


% %generate cone mosaic figure with important points labeled
% f1 = figure;
% imagesc([min(x),max(x)],[min(y),max(y)],coneMap.refImage)
% daspect([1 1 1])    % set aspect ratio to 1:1
% colormap('gray')
% axis([-xMax xMax -yMax yMax])
% hold on
% legS(1) = plot(PRL_Xarcmin,PRL_Yarcmin,'*','Color','b','LineWidth',1,'MarkerSize',8);
% legS(2) =plot(PCD_Xarcmin,PCD_Yarcmin,'^','Color','k','LineWidth',1,'MarkerSize',6);
% legS(3) =plot(CDC_Xarcmin,CDC_Yarcmin,'d','Color','m','LineWidth',1,'MarkerSize',6);
% l1 = legend(legS,{'PRL','PCD','CDC'});
% hold off
% 
clear('legS');      % clear so it can be reused later

f2 = figure; %sanity check to see normal offset and to make sure heatmap has correct orientation
ax(1) = subplot(1,3,1);      % just the data
plot(dataAll.Scotoma.allXCleanFIX,dataAll.Scotoma.allYCleanFIX,'.r')
axis([-xMax xMax -yMax yMax])
axis square
title('Fixation points during task')

ax(2) = subplot(1,3,2);      % plot the heatmap next to the data to check orientation and shape
%%% old method using generateHeatMapSimple() function
% resultTest = generateHeatMapSimple( ...
%     (dataAll.Scotoma.allXCleanFIX), ... 
%     (dataAll.Scotoma.allYCleanFIX), ... % don't filp y yet (monitor coordinate frame)
%     'Bins', bins,...
%     'StimulusSize', 5,...
%     'AxisValue', axisVal,...
%     'Uncrowded', 0,...
%     'Borders', 1);
h1 = histogram2(dataAll.Scotoma.allXCleanFIX,dataAll.Scotoma.allYCleanFIX,...
    xEdges,yEdges,'DisplayStyle','tile','ShowEmptyBins','off',...
    'Normalization','probability','EdgeColor','none');
daspect([1 1 1])
colormap jet
r1 = rectangle('Position',[-7/2,-7/2,7,7]);
r1.EdgeColor = 'm'; 
hold on
p1 = plot(mean(dataAll.Control.allXCleanFIX), mean(dataAll.Control.allYCleanFIX),'*b');
axis square
grid off
legend(p1,'PRL for control experiment')
title('Histogram of fixation points during task')
hold off

% find peak, centroid, and weighted centroid to compare results
h1Vals = h1.Values.';       % histogram values, transposed
h1Norm = h1Vals./max(max(h1Vals));  % normalize to peak value of 1  
pkInd = find(h1Norm==1);    % find linear index of peak of histogram
xPk = XH(pkInd);            % x-coordinate for peak
yPk = YH(pkInd);            % y-coordinate for peak
xMean = mean(dataAll.Scotoma.allXCleanFIX); %x-coordinate for mean position
yMean = mean(dataAll.Scotoma.allYCleanFIX); %y-coordinate for mean position

h1ValsVect = reshape(h1Vals,[],1);  % make a vector for sorting
h1ValsSorted = sort(h1ValsVect,'descend');  % sort probabilities descending
h1ValsSum = cumsum(h1ValsSorted);   % sum probabilities

dataTh = 0.68;  % fraction of PDF to include (percent of hist data)
thInd = find(h1ValsSum>dataTh,1);   % find index of first element to cross threshold
thVal = h1ValsSorted(thInd)/h1ValsSorted(1);    % find threshold value and normalize

h1Th = zeros(size(h1Norm));     % initialize array for BW mask
h1Th(h1Norm>thVal) = 1;         % set values above threshold to 1

h1Centroid = regionprops(h1Th,'Centroid');  % normal centroid
h1WC = regionprops(h1Th,h1Norm,'WeightedCentroid'); % weighted centroid
% get centroid coordinates by interpolating b/t pixels
xC = interp1((1:1:length(xHist)),xHist,h1Centroid.Centroid(1));
yC = interp1((1:1:length(yHist)),yHist,h1Centroid.Centroid(2));
% get weighted centroid coordinates by interpolating b/t pixels
xWC = interp1((1:1:length(xHist)),xHist,h1WC.WeightedCentroid(1));
yWC = interp1((1:1:length(yHist)),yHist,h1WC.WeightedCentroid(2));

h1NormClipped = h1Norm;     % copy for plotting, where low values will be replaced with nan
h1NormClipped(h1NormClipped<0.01) = nan; % clip at 1% relative to peak

% shift axes to place origin at weighted centroid of heatmap, which will be
% aligned with the PRL in the retinal image
XH_aligned = XH-xWC;        % subtract centroid x-value to create new origin
YH_aligned = YH-yWC;        % subtract centroid y-value to create new origin

ax(3) = subplot(1,3,3);
imagesc([-xMax,xMax],[-yMax,yMax],h1Th)
daspect([1 1 1])
set(ax(3),'YDir','normal')    % put y-axis with positive values up
colormap(ax(3),'gray')
hold on
r1 = rectangle('Position',[-7/2,-7/2,7,7]);
r1.EdgeColor = 'm'; 
legS(1) = plot(xWC,yWC,'*c','LineWidth',0.7);   % weighted centroid
legS(2) = plot(xPk,yPk,'sb','LineWidth',0.7);   % peak of histogram
legS(3) = plot(xMean,yMean,'og','LineWidth',0.7);   % mean of fixation points
% legS(4) = plot(xC,yC,'+c','LineWidth',0.7);     % centroid (disabled)
l1 = legend(legS,{'weighted centroid','histogram peak','mean of fixation'});
l1.Color = [0.7 0.7 0.7];   % make the legend color gray
textStr = sprintf('Centroid at (%0.1f,%0.1f)',xWC,yWC);
text(-20,-35,textStr,'Color',[1 1 1]);
titleStr = sprintf('Threshold with top %d percent',dataTh*100);
title(titleStr)
hold off

clear('legS')

% generate figure to compare alignment before and after applying shift
f3 = figure;
subplot(1,2,1)
% figure with histogram data centered on fixation marker
surf(XH,YH,h1NormClipped);    % histogram data
colormap('jet');       % colormap for histogram
view(2)
shading interp
grid off
daspect([1 1 1]);       % make data aspect ratio 1:1
axis([-xMax xMax -yMax yMax])
% plot other features to make sure alignment is preserved
hold on
% draw fixation marker
plot3([-7/2,7/2,7/2,-7/2,-7/2],...
    [-7/2,-7/2,7/2,7/2,-7/2],...
    [1,1,1,1,1],'-m');
% draw x and y axes to check origin placement
line([0 0],[-yMax,yMax],[1 1],'Color','k')  
line([-xMax,xMax],[0 0],[1 1],'Color','k')
% draw other relevant points
plot3(xWC,yWC,1,'*c','LineWidth',0.7);   % weighted centroid
plot3(xPk,yPk,1,'sb','LineWidth',0.7);   % peak of histogram
plot3(xMean,yMean,1,'og','LineWidth',0.7);   % mean of fixation points
hold off
title('Centered on stimulus')

subplot(1,2,2)
% figure with histogram data centered on the weighted centroid
surf(XH_aligned,YH_aligned,h1NormClipped);    % histogram data
colormap('jet');       % colormap for histogram
view(2)
shading interp
grid off
daspect([1 1 1]);       % make data aspect ratio 1:1
axis([-xMax xMax -yMax yMax])
% plot other features to make sure alignment is preserved
hold on
% draw fixation marker
plot3([-7/2-xWC,7/2-xWC,7/2-xWC,-7/2-xWC,-7/2-xWC],...
    [-7/2-yWC,-7/2-yWC,7/2-yWC,7/2-yWC,-7/2-yWC],...
    [1,1,1,1,1],'-m');
% draw x and y axes to check origin placement
line([0 0],[-yMax,yMax],[1 1],'Color','k')  
line([-xMax,xMax],[0 0],[1 1],'Color','k')
% draw other relevant points
legS(1) = plot3(xWC-xWC,yWC-yWC,1,'*c','LineWidth',0.7);   % weighted centroid
legS(2) = plot3(xPk-xWC,yPk-yWC,1,'sb','LineWidth',0.7);   % peak of histogram
legS(3) = plot3(xMean-xWC,yMean-yWC,1,'og','LineWidth',0.7);   % mean of fixation points
l1 = legend(legS,{'weighted centroid','histogram peak','mean of fixation'});
hold off
title('Centered on weighted centroid')

clear('legS')

% figure with cone mosaic and heatmap overlay
f4 = figure;
% create truecolor image of cone mosaic by stacking 3 copies (for plotting)
coneImgRGB = cat(3,coneMap.refImage,coneMap.refImage,coneMap.refImage); 
a1 = imagesc([min(x),max(x)],[min(y),max(y)],coneImgRGB);
daspect([1 1 1])    % set aspect ratio to 1:1
axis([-xMax xMax -yMax yMax])
hold on

% heatmap overlay
a2 = surf(XH_aligned,YH_aligned,h1NormClipped);    % histogram data
c2 = colormap('jet');       % colormap for histogram
view(2)
shading interp
a2.FaceAlpha = 0.3;

% label important points on the mosaic
legS(1) = plot3(PRL_Xarcmin,PRL_Yarcmin,1,'*','Color','b','LineWidth',1,'MarkerSize',8);
legS(2) =plot3(PCD_Xarcmin,PCD_Yarcmin,1,'^','Color','k','LineWidth',1,'MarkerSize',6);
legS(3) =plot3(CDC_Xarcmin,CDC_Yarcmin,1,'d','Color','m','LineWidth',1,'MarkerSize',6);
l2 = legend(legS,{'PRL','PCD','CDC'});
l2.AutoUpdate = 'off';

% add labels for retinal orientation (assumes right eye, otherwise N-T are
% incorrect)
fColor = [0.90 0.05 0.10];    % font color for labels
text(0,-0.85*yMax,1,'S','FontWeight','bold','FontSize',16,'Color',fColor)
text(0,0.85*yMax,1,'I','FontWeight','bold','FontSize',16,'Color',fColor)
text(0.85*xMax,0,1,'N','FontWeight','bold','FontSize',16,'Color',fColor)
text(-0.85*xMax,0,1,'T','FontWeight','bold','FontSize',16,'Color',fColor)

hold off
title('Cone mosaic with heatmap overlay')


%%



% % generate figure to check the appearance of the eyetrace data
% bins = 80;
% axisVal = 40;
% figure
% [result, limitz] = generateHeatMapSimple( ...
%     (dataAll.Scotoma.allXCleanFIX+PRL_Xarcmin), ... 
%     (dataAll.Scotoma.allYCleanFIX+PRL_Yarcmin), ... % don't filp y yet (monitor coordinate frame)
%     'Bins', bins,...
%     'StimulusSize', 5,...
%     'AxisValue', axisVal,...
%     'Uncrowded', 0,...
%     'Borders', 1);
% daspect([1 1 1])
% rectangle('Position',[-7/2,-7/2,7,7]);
% title('Heat map of gaze on monitor')
% 
% 
% [xS,yS]=find(result==1);
% zerozeroMatrixCoord = floor(length(result)/2); %% 0,0 in matrix


%1    %%Heatmap Option 1
%%% bin size may be different for X and Y
% binToArcminX = (length([limitz.xmin:1:limitz.xmax]))/(bins); %(Bins-1)*(AxisValue*2)
% binToArcminY = (length([limitz.ymin:1:limitz.ymax]))/(bins); %(Bins-1)*(AxisValue*2)
% newX = (xS - zerozeroMatrixCoord)*binToArcminX;
% newY = (yS - zerozeroMatrixCoord)*binToArcminY;
% hold on
% circleAMC(newX,newY, 5/2);


%2   %% Centroid of Mode
% modeX = median(dataAll.Scotoma.allXCleanFIX+PRL_Xarcmin);
% modeY = median(dataAll.Scotoma.allYCleanFIX+PRL_Yarcmin);
% 
% legs(2) = plot(modeX,modeY,'kd')
% modeDistC = eucDist(xC,yC);


%3   %% Centroid of 68% Elipse
%     subplot(2,2,[3 4])
%     [tempC]=ellipseXY(dataAll.Scotoma.allXCleanFIX+PRL_Xarcmin',...
%         dataAll.Scotoma.allYCleanFIX+PRL_Yarcmin', 68, [150 150 150]/255,0);
%     
%     x1 = [tempC.Position(1) tempC.Position(1) ...
%         tempC.Position(1)+tempC.Position(3) tempC.Position(1)+tempC.Position(3)];
%     y1= [tempC.Position(2)+tempC.Position(4) tempC.Position(2)  ...
%         tempC.Position(2) tempC.Position(2)+tempC.Position(4)];
%     hold on
%     polyin = polyshape(x1,y1);
%     [x,y] = centroid(polyin);


%4%% Centroid of Probability Map 
% *currently best one*

%     s = regionprops3(result,"Centroid");
%     xC = s.Centroid(2)- zerozeroMatrixCoord;
%     yC = s.Centroid(1)- zerozeroMatrixCoord;
% 
%     hold on 
%     legs(1) = plot(xC,yC,'kd','MarkerSize',10,'MarkerFaceColor','m');
%     centroidDistC = eucDist(xC,yC);



