

%% Script for Comparing cone density with performance
clear;clc;close all;
%% Load data



AplabData = 1;
imageID = 'Z055_R_2022_03_01';


% if AplabData
    pixPerdeg = 512;
    behavPerf = 0;
%     dataPath = 'C:\Users\sjenks\Desktop\research\rochester\AO\adaptiveoptics\ConeDesity\APLab_System\data\XC\';
    dataPath = 'C:\Users\ruccilab\Documents\adaptiveoptics\ConeDesity\APLab_System\data\XC\';
    dataList_OG = dir(dataPath);
    dataList = dataList_OG(3:end);
    
    for nfiles = 1:2:length(dataList)
        files(nfiles) = strcmp({dataList(nfiles).name(1:end-12)}, string(imageID));
   

    CD_subID = find(files);
    load([dataPath dataList(nfiles).name])
    subject = dataList(nfiles).name(1:4);
    
    if strcmp({dataList(nfiles).name(1:end-12)}, string(imageID))
        
        if behavPerf
            
            load('C:\Users\sjenks\Desktop\research\rochester\AO\adaptiveoptics\ConeDesity\FVP.mat')
            load('C:\Users\sjenks\Desktop\research\rochester\AO\adaptiveoptics\ConeDesity\APLab_System\data\distfromTarg_F.mat');
            FoealAnisoSubs = {'SJ','Z151','Z128', 'Z055', 'YZ','Z136', 'Z111','Z124','Z040','Z096','Z004', 'Z165'};
            subID = find(strcmp(FoealAnisoSubs, subject)==1);
        end
    end
    
% end





%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% PCD
%trim the edges of the matrix
hozrPCD_col = CD_conePerDegsq(oursPCD_y - 5: oursPCD_y + 5,:);
vertPCD_row = CD_conePerDegsq(:,oursPCD_x-5:oursPCD_x + 5)';
%convert the pixels to degrees (base off image)
pixX_PCDtrimed = [1:length(vertPCD_row)];
XaxisPCD_DEG = pixX_PCDtrimed/pixPerdeg;
pixY_PCDtrimed = [1:length(hozrPCD_col)];
YaxisPCD_DEG = pixY_PCDtrimed/pixPerdeg;
normPCD_Xaxis_deg = XaxisPCD_DEG - (oursPCD_y/pixPerdeg);
normPCD_Yaxis_deg = YaxisPCD_DEG - (oursPCD_x /pixPerdeg);

%%% CDC
%trim the edges of the matrix
hozrCDC_col = CD_conePerDegsq(oursCDC_y - 5: oursCDC_y + 5,:);
vertCDC_row = CD_conePerDegsq(:,oursCDC_x-5:oursCDC_x + 5)';
%convert the pixels to degrees (base off image)
pixX_CDCtrimed = [1:length(vertCDC_row)];
XaxisCDC_DEG = pixX_CDCtrimed/pixPerdeg;
pixY_CDCtrimed = [1:length(hozrCDC_col)];
YaxisCDC_DEG = pixY_CDCtrimed/pixPerdeg;
normCDC_Xaxis_deg = XaxisCDC_DEG - (oursCDC_y/pixPerdeg);
normCDC_Yaxis_deg = YaxisCDC_DEG - (oursCDC_x /pixPerdeg);


%%% PRL

%trim the edges of the matrix
hozrPRL_col = CD_conePerDegsq(PRL_Y- 5: PRL_Y + 5,:);
vertPRL_row = CD_conePerDegsq(:,PRL_X- 5 : PRL_X + 5)';
%convert the pixels to degrees (base off image)
pixX_PRLtrimed = [1:length(vertPRL_row)];
XaxisPRL_DEG = pixX_PRLtrimed/pixPerdeg;
pixY_PRLtrimed = [1:length(hozrPRL_col)];
YaxisPRL_DEG = pixY_PRLtrimed/pixPerdeg;

normPRL_Xaxis_deg = XaxisPRL_DEG - (PRL_Y/pixPerdeg);
normPRL_Yaxis_deg = YaxisPRL_DEG - (PRL_X/pixPerdeg);




%Plot Cone density around peak cone density
figure;
% plot(normPCD_Yaxis_deg,mean(hozrPCD_col), 'r');%horizontal meridian
hold on;
plot(normPCD_Xaxis_deg*60, mean(vertPCD_row), 'k'); %vertical meridian
hold on;
plot(normPRL_Xaxis_deg*60,mean(vertPRL_row), 'c');
hold on;
plot(normCDC_Xaxis_deg*60,mean(vertCDC_row), 'm');

title('Vertical Meridian Cone Density: PCD vs PRL')
xlabel('Degrees Eccentricity')
ylabel('Cone Density (cones/degree^2)')
ylim([6000, 18000])
legend('PCD', 'PRL', 'CDC')

% figure;
% % plot(normPCD_Yaxis_deg,mean(hozrPCD_col), 'r');%horizontal meridian
% hold on;
% plot(normCDC_Xaxis_deg*60,mean(vertCDC_row), 'm');
% hold on;
% plot(normPRL_Xaxis_deg*60,mean(vertPRL_row), 'c');
% 
% title('Vertical Meridian Cone Density: PCD vs PRL')
% xlabel('Degrees Eccentricity')
% ylabel('Cone Density (cones/degree^2)')
% ylim([6000, 18000])
% legend('CDC', 'PRL')

figure;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %%% PCD
subplot(1,3,1)
plot(normPCD_Yaxis_deg*60,mean(hozrPCD_col), 'r');%horizontal meridian
hold on;
plot(normPCD_Xaxis_deg*60, mean(vertPCD_row), 'g'); %vertical meridian


title('Cone Density centered around PCD')
xlabel('Degrees Eccentricity')
ylabel('Cone Density (cones/degree^2)')
ylim([6000, 18000])
legend('horizontal', 'vertical')



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %%% CDC


%Plot Cone density around peak cone density
subplot(1,3,2)
% plot(normPCD_Yaxis_deg,mean(hozrPCD_col), 'r');%horizontal meridian
hold on;
plot(normCDC_Xaxis_deg*60, mean(vertCDC_row), 'r'); %vertical meridian
hold on;
plot(normCDC_Yaxis_deg*60,mean(hozrCDC_col), 'g');

title('Cone Density centered around CDC')
xlabel('Degrees Eccentricity')
ylabel('Cone Density (cones/degree^2)')
ylim([6000, 18000])
legend('horizontal', 'vertical')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% PRL

%Plot Cone density around peak cone density
subplot(1,3,3)
plot(normPRL_Yaxis_deg, mean(hozrPRL_col), 'r'); %horizontal meridian
hold on;
plot(normPRL_Xaxis_deg,mean(vertPRL_row), 'g'); %vertical meridian
title('Cone Density centered around the PRL')
xlabel('Degrees Eccentricity')
ylabel('Cone Density (cones/degree^2)')
ylim([6000, 18000])
legend('horizontal', 'vertical')








%% get Cone density at 20 acrmin with an area of 2 arc min
% variables
targOffset = 0.33; % eccentricity from PRL in deg
area = 0.033; % how big you want to take the area (square)

ecc = round(targOffset * pixPerdeg,0); %eccentrictiy in pixels
boxLoc = round(area * pixPerdeg,0); % box area in pixels

boxdiag = sqrt(((boxLoc^2) + (boxLoc^2)));




if behavPerf
    Gaze = 1;
    indvAdist_F = cellfun(@(z) mean(z(:)), FvP.data(1).adist);
    
    
    Z055_Adist = indvAdist_F(4,:) * (pixPerdeg/60);
    Z151_Adist = indvAdist_F(2,:) * (pixPerdeg/60);
    
    Z055_CI = CIdist_F(4,:) * (pixPerdeg/60);
    Z151_CI = CIdist_F(2,:) * (pixPerdeg/60);
    
    Z055_Adist = [Z055_Adist(5) Z055_Adist(1) Z055_Adist(3) Z055_Adist(7)];
    Z151_Adist = [Z151_Adist(5) Z151_Adist(1) Z151_Adist(3) Z151_Adist(7)];
    
    Z055_CI = [Z055_CI(5) Z055_CI(1) Z055_CI(3) Z055_CI(7)];
    Z151_CI = [Z151_CI(5) Z151_CI(1) Z151_CI(3) Z151_CI(7)];
    
    
    for cc = 1:2 % prl then CDC
        center_X = [round(PRL_X,0) oursCDC_x];
        center_Y = [round(PRL_Y,0) oursCDC_y];
        rCenter_X = center_X(cc);
        rCenter_Y =center_Y(cc);
        
        SuperiorCenter_Loc = rCenter_Y - ecc;
        InferiorCenter_Loc=  rCenter_Y + ecc;
        NasalCenter_Loc =  rCenter_X + ecc;
        TempCenter_Loc = rCenter_X - ecc;
        
        
        if Gaze
            %location based on eyes
            if strcmp(subject, 'Z055')
                SuperiorCenter = rCenter_Y - Z055_Adist(1);
                InferiorCenter=  rCenter_Y + Z055_Adist(2);
                NasalCenter =  rCenter_X + Z055_Adist(3);
                TempCenter = rCenter_X - Z055_Adist(4);
                box = Z055_CI;
            elseif strcmp(subject, 'Z151')
                SuperiorCenter = rCenter_Y - Z151_Adist(1);
                InferiorCenter=  rCenter_Y + Z151_Adist(2);
                NasalCenter =  rCenter_X + Z151_Adist(3);
                TempCenter = rCenter_X - Z151_Adist(4);
                box = Z151_CI;
            end
        else
            %location of boxes
            SuperiorCenter = rCenter_Y - ecc;
            InferiorCenter=  rCenter_Y + ecc;
            NasalCenter =  rCenter_X + ecc;
            TempCenter = rCenter_X - ecc;
            
            
        end
        
        
        % %print out the schemeatic for the AO orientation
        % OrientSchem = imread('C:\Users\ruccilab\Documents\adaptiveoptics\ConeDesity\APLab_System\data\orientationOfVideoFrames_2022-06-22.png');
        % figure;
        % imshow(OrientSchem);
        
        if Gaze
            %set up the box locations
            superiorBox = CD_conePerDegsq(SuperiorCenter - box(1):SuperiorCenter + box(1),  rCenter_X - box(1): rCenter_X + box(1));
            superiorArea = mean(mean(superiorBox));
            
            inferiorBox = CD_conePerDegsq(InferiorCenter - box(2): InferiorCenter + box(2),  rCenter_X - box(2): rCenter_X + box(2));
            inferiorArea = mean(mean(inferiorBox));
            
            nasalBox = CD_conePerDegsq(rCenter_Y - box(3): rCenter_Y + box(3), NasalCenter- box(3) : NasalCenter +box(3));
            nasalArea = mean(mean(nasalBox));
            
            temporalBox = CD_conePerDegsq(rCenter_Y - box(4): rCenter_Y + box(4),TempCenter- box(4) : TempCenter + box(4));
            temporalArea = mean(mean(temporalBox));
            
            %black out where the physical boxes were on the monitor.
            viewTargLoc = CD_conePerDegsq;
            viewTargLoc(SuperiorCenter_Loc - boxLoc: SuperiorCenter_Loc + boxLoc, rCenter_X - boxLoc: rCenter_X + boxLoc)=0;
            viewTargLoc(InferiorCenter_Loc - boxLoc: InferiorCenter_Loc + boxLoc,  rCenter_X - boxLoc: rCenter_X + boxLoc)=0;
            
            
        else
            %set up the box locations
            superiorBox = CD_conePerDegsq(SuperiorCenter - boxLoc:SuperiorCenter + boxLoc,  rCenter_X - boxLoc: rCenter_X + boxLoc);
            superiorArea = mean(mean(superiorBox));
            
            inferiorBox = CD_conePerDegsq(InferiorCenter - boxLoc: InferiorCenter + boxLoc,  rCenter_X - boxLoc: rCenter_X + boxLoc);
            inferiorArea = mean(mean(inferiorBox));
            
            nasalBox = CD_conePerDegsq(rCenter_Y - boxLoc: rCenter_Y + boxLoc, NasalCenter- boxLoc : NasalCenter +boxLoc);
            nasalArea = mean(mean(nasalBox));
            
            temporalBox = CD_conePerDegsq(rCenter_Y - boxLoc: rCenter_Y + boxLoc,TempCenter- boxLoc : TempCenter + boxLoc);
            temporalArea = mean(mean(temporalBox));
            
            %black out where the box locations will be
            viewTargLoc = CD_conePerDegsq;
            viewTargLoc(SuperiorCenter - boxLoc: SuperiorCenter + boxLoc, rCenter_X - boxLoc: rCenter_X + boxLoc)=0;
            viewTargLoc(InferiorCenter - boxLoc: InferiorCenter + boxLoc,  rCenter_X - boxLoc: rCenter_X + boxLoc)=0;
            %         viewTargLoc(rCenter_Y - boxLoc: rCenter_Y + boxLoc, NasalCenter- boxLoc : NasalCenter +boxLoc)=0;
            %         viewTargLoc(rCenter_Y - boxLoc: rCenter_Y + boxLoc,TempCenter- boxLoc : TempCenter + boxLoc)=0;
            %
        end
        
        % black out the target locations on the heatmap to verify their location.
        figure;
        axc = gca;
        imagesc(viewTargLoc); hold on;
        a=plot(PRL_X,PRL_Y, 'g.', 'MarkerSize', 25);hold on;
        %     b=plot(oursPCD_x,oursPCD_y, 'r^');hold on;
        c=plot(oursCDC_x,oursCDC_y, 'b*');hold on;
        legend([a,c],'Preferred Retinal Locus','Cone Density Centroid')
        
        
        if strcmp(subject, 'Z055')
            hold on;
            viscircles([rCenter_X,SuperiorCenter],Z055_CI(1) + (boxdiag/2));hold on;%superior
            viscircles([rCenter_X,InferiorCenter],Z055_CI(2) + (boxdiag/2));hold on;%inferior
            %         viscircles([NasalCenter,rCenter_Y],Z055_CI(3) + (boxdiag/2));hold on;%nasal
            %         viscircles([TempCenter,rCenter_Y],Z055_CI(4) + (boxdiag/2));hold on;%temporal
            
            xlim([50 550]);
            ylim([50 550]);
            text((size(CD_conePerDegsq,1)/2)-80,80,'Superior Retina')
            text((size(CD_conePerDegsq,1)/2)-60,size(CD_conePerDegsq,2)-80,'Inferior Retina Retina')
            
            text(60,(size(CD_conePerDegsq,1)/2)-30,'Temporal Retina')
            text(size(CD_conePerDegsq,1)-150,(size(CD_conePerDegsq,1)/2)-30,'Nasal Retina')
            
            ws = (10/60)*pixPerdeg;    % 10 arcmin
            szI = size(CD_conePerDegsq);
            hs = 20;
            px = (rCenter_X - 230)+20;                % position in x
            py =(rCenter_Y + 225) - 20 - hs;   % position in y
            r = rectangle('Position',[px,py,ws,hs]);
            r.FaceColor = [0 0 0];
            t = text(px,py-1.5*hs,'10 arcmin');
            t.Color = [0 0 0];
            axc.CLim = [0 13000];
            colorbar
            axis off
            
        elseif strcmp(subject, 'Z151')
            hold on;
            viscircles([rCenter_X,SuperiorCenter],Z151_CI(1) + (boxdiag/2));hold on;%superior
            viscircles([rCenter_X,InferiorCenter],Z151_CI(2) + (boxdiag/2));hold on;%inferior
            %         viscircles([NasalCenter,rCenter_Y],Z151_CI(3) + (boxdiag/2));hold on;%nasal
            %         viscircles([TempCenter,rCenter_Y],Z151_CI(4) + (boxdiag/2));hold on;%temporal
            
            
            xlim([50 550]);
            ylim([50 550]);
            text((size(CD_conePerDegsq,1)/2)-80,80,'Superior Retina')
            text((size(CD_conePerDegsq,1)/2)-60,size(CD_conePerDegsq,2)-80,'Inferior Retina Retina')
            
            text(60,(size(CD_conePerDegsq,1)/2)-10,'Temporal Retina')
            text(size(CD_conePerDegsq,1)-150,(size(CD_conePerDegsq,1)/2)-10,'Nasal Retina')
            
            ws = (10/60)*pixPerdeg;    % 10 arcmin
            szI = size(CD_conePerDegsq);
            hs = 20;
            px = (rCenter_X - 230)+20;                % position in x
            py =(rCenter_Y + 225) - 20 - hs;   % position in y
            r = rectangle('Position',[px,py,ws,hs]);
            r.FaceColor = [0 0 0];
            t = text(px,py-1.5*hs,'10 arcmin');
            t.Color = [0 0 0];
            axc.CLim = [0 13000];
            colorbar
            axis off
            
        end
        
        supArea(cc) = superiorArea;
        infArea(cc) = inferiorArea;
        nasArea(cc) = nasalArea;
        temArea(cc) = temporalArea;
    end
else
    %% the heatmap mosaic if not behavioral
    
    figure;
    for cc = 1:2 % prl then CDC
        center_X = [round(PRL_X,0) oursCDC_x];
        center_Y = [round(PRL_Y,0) oursCDC_y];
        rCenter_X = center_X(cc);
        rCenter_Y =center_Y(cc);
        
        SuperiorCenter_Loc = rCenter_Y - ecc;
        InferiorCenter_Loc=  rCenter_Y + ecc;
        NasalCenter_Loc =  rCenter_X + ecc;
        TempCenter_Loc = rCenter_X - ecc;
        
        %location of boxes
        SuperiorCenter = rCenter_Y - ecc;
        InferiorCenter=  rCenter_Y + ecc;
        NasalCenter =  rCenter_X + ecc;
        TempCenter = rCenter_X - ecc;
        
        
        
        
        % %print out the schemeatic for the AO orientation
        % OrientSchem = imread('C:\Users\ruccilab\Documents\adaptiveoptics\ConeDesity\APLab_System\data\orientationOfVideoFrames_2022-06-22.png');
        % figure;
        % imshow(OrientSchem);
        
        
        %set up the box locations
        superiorBox = CD_conePerDegsq(SuperiorCenter - boxLoc:SuperiorCenter + boxLoc,  rCenter_X - boxLoc: rCenter_X + boxLoc);
        superiorArea = mean(mean(superiorBox));
        
        inferiorBox = CD_conePerDegsq(InferiorCenter - boxLoc: InferiorCenter + boxLoc,  rCenter_X - boxLoc: rCenter_X + boxLoc);
        inferiorArea = mean(mean(inferiorBox));
        
        nasalBox = CD_conePerDegsq(rCenter_Y - boxLoc: rCenter_Y + boxLoc, NasalCenter- boxLoc : NasalCenter +boxLoc);
        nasalArea = mean(mean(nasalBox));
        
        temporalBox = CD_conePerDegsq(rCenter_Y - boxLoc: rCenter_Y + boxLoc,TempCenter- boxLoc : TempCenter + boxLoc);
        temporalArea = mean(mean(temporalBox));
        
        %black out where the box locations will be
        viewTargLoc = CD_conePerDegsq;
        viewTargLoc(SuperiorCenter - boxLoc: SuperiorCenter + boxLoc, rCenter_X - boxLoc: rCenter_X + boxLoc)=0;
        viewTargLoc(InferiorCenter - boxLoc: InferiorCenter + boxLoc,  rCenter_X - boxLoc: rCenter_X + boxLoc)=0;
        %         viewTargLoc(rCenter_Y - boxLoc: rCenter_Y + boxLoc, NasalCenter- boxLoc : NasalCenter +boxLoc)=0;
        %         viewTargLoc(rCenter_Y - boxLoc: rCenter_Y + boxLoc,TempCenter- boxLoc : TempCenter + boxLoc)=0;
        %
        
        
        % black out the target locations on the heatmap to verify their location.
        subplot(1,2,cc)
        axc = gca;
        imagesc(viewTargLoc); hold on;
        a=plot(PRL_X,PRL_Y, 'g.', 'MarkerSize', 25);hold on;
        %     b=plot(oursPCD_x,oursPCD_y, 'r^');hold on;
        c=plot(oursCDC_x,oursCDC_y, 'b*');hold on;
        legend([a,c],'Preferred Retinal Locus','Cone Density Centroid')
        
        supArea(cc) = superiorArea;
        infArea(cc) = inferiorArea;
        nasArea(cc) = nasalArea;
        temArea(cc) = temporalArea;
    end
end

rAreas_PRL = [supArea(1)  infArea(1)  nasArea(1) temArea(1)];
n_rAreas_PRL = [supArea(1)  infArea(1)  nasArea(1) temArea(1)]/ mean(mean(CD_conePerDegsq));

rAreas_CDC = [supArea(2)  infArea(2)  nasArea(2) temArea(2)];
n_rAreas_CDC = [supArea(2)  infArea(2)  nasArea(2) temArea(2)]/ mean(mean(CD_conePerDegsq));

rAreasPRL_ALL(nfiles,:) = rAreas_PRL;
rAreasCDC_ALL(nfiles,:) = rAreas_CDC;

avgHM = mean([rAreas_PRL(:,3) rAreas_PRL(:,4)],2);
avgVM = mean([rAreas_PRL(:,1) rAreas_PRL(:,2)],2);
avgHM_CDC = mean([rAreas_CDC(:,3) rAreas_CDC(:,4)],2);
avgVM_CDC = mean([rAreas_CDC(:,1) rAreas_CDC(:,2)],2);

label = {'PRL','CDC' };

figure;
p1 = plot([1,2], [rAreas_PRL(1) rAreas_PRL(2)],'.-', 'Color', [0 1 0]); hold on;
p2 = plot([1,2], [rAreas_CDC(1) rAreas_CDC(2)],'.-', 'Color', [0 0 1]); hold on;
% errorbar([3,4], avg_reiniger(3:4),CI_XC(3:4),'.-', 'Color', [0 1 0]); hold on;
xticks(1:2)
xlim([.75 2.25])
ylim([8000 11000])
ylabel('Cone density (cones/deg^2)')
xticklabels({'Superior/lowerVF', 'Inferior/UpperVF'})
ax = gca;
if cc == 1
    ax.YAxis(1).Color =  [0 1 0];
else
    ax.YAxis(1).Color =  [0 0 1];
end
legend([p1 p2],{sprintf('%s Centered', label{1}), sprintf('%s Centered', label{2})} )





if behavPerf
    
    % Load in performance
    
    avg_perf = FvP.data(1).avgPerf(subID,:);
    
    FovHVAPerf = (FvP.data(1).perf_HVA(subID,:));
    % FovHVACI_XC = (FvP.data(1).HVA_stePerf(subID,:))*CI95_F(2);
    
   
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% Line Plots
    
    
    
    
    label = {'PRL','CDC' };
    for cc = 1:2
        
        figure;
        if cc == 1
            yyaxis left
            p1 = plot([1,2], [mean(avgHM) mean(avgVM)],'.-', 'Color', [0 1 0]); hold on;
            % errorbar([3,4], avg_reiniger(3:4),CI_XC(3:4),'.-', 'Color', [0 1 0]); hold on;
            xticks(1:2)
            xlim([.75 2.25])
            ylim([8000 11000])
            ylabel('Cone density (cones/deg^2)')
        elseif cc ==2
            yyaxis left
            p1 = plot([1,2],[mean(avgHM_CDC) mean(avgVM_CDC)],'.-', 'Color', [0 0 1]); hold on;
            % errorbar([3,4], avg_reiniger(3:4),CI_XC(3:4),'.-', 'Color', [0 1 0]); hold on;
            xticks(1:2)
            xlim([.75 2.25])
            ylim([8000 11000])
            ylabel('Cone density (cones/deg^2)')
        end
        
        yyaxis right
        p2 = plot([1,2], FovHVAPerf,'.-','Color',[1 0 1]); hold on;
        % errorbar([3, 4], FovPerf_XC(3:4),CIXC_F(3:4),'.-','Color',[1 0 1]); hold on;
        xticks(1:2)
        xlim([.75 2.25])
        ylim([0.4 .9])
        ylabel('Proportion Correct)')
        
        xticklabels({'Horizontal Meridian', 'Vertical Meridian'})
        legend([p1 p2],{sprintf('%s Centered', label{cc}),'Performance'})
        %     text(1,0.85,'Horizontal Vertical Meridian Asymmetry')
        % text(3,0.525,'Horizontal Meridian')
        ax = gca;
        if cc == 1
            ax.YAxis(1).Color =  [0 1 0];
        else
            ax.YAxis(1).Color =  [0 0 1];
        end
        ax.YAxis(2).Color = [1 0 1];
        
    end
    
    
end





    end
%%

locXcent = [rAreasPRL_ALL; rAreasCDC_ALL];

%Set this up correctly
[p,tableA,stats]=anova2(locXcent,length(rAreasPRL_ALL));

sup_PRL = rAreasPRL_ALL(:,1);
inf_PRL = rAreasPRL_ALL(:,2);
HM_PRL = mean(rAreasPRL_ALL(:,3:4),2);
VM_PRL = mean(rAreasPRL_ALL(:,1:2),2);

sup_CDC = rAreasCDC_ALL(:,1);
inf_CDC = rAreasCDC_ALL(:,2);
HM_CDC = mean(rAreasCDC_ALL(:,3:4),2);
VM_CDC = mean(rAreasCDC_ALL(:,1:2),2);

HVA_array = [HM_CDC VM_CDC;HM_PRL, VM_PRL];

VMA_array = [sup_CDC inf_CDC;sup_PRL, inf_PRL];


[p_CDhva,tableA_CDhva,stats_CDhva]=anova2(HVA_array,length(HM_CDC));

[p_CDvma,tableA_CDvma,stats_CDvma]=anova2(VMA_array,length(sup_CDC));



[hvaCDC_h, hvaCDC_p, hvaCDC_ci,hvaCDC_STATS] = ttest(HM_CDC, VM_CDC)
[hvaPRL_h, hvaPRL_p, hvaPRL_ci,hvaPRL_STATS] = ttest(HM_PRL, VM_PRL)

[vmaCDC_h, vmaCDC_p, vmaCDC_ci,vmaCDC_STATS] = ttest(sup_CDC, inf_CDC)
[vmaPRL_h, vmaPRL_p, vmaPRL_ci,vmaPRL_STATS] = ttest(sup_PRL, inf_PRL)



    