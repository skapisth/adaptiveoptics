
There are 3 scripts that are needed to run the cone density analysis. 

1. ProcessConeDensity - will automatically call the other 2 scripts
2. caculate_coneDensity
3. plot_coneDensity


to RUN Process ConeDensity -> change the rootDirectory to where you put the entire folder ConeDensity.

*optional processCD: set processCD = 1 if you want re run the conedensity function and set processCD to 0 if you just want to plot figures



*** An orientation note of the retinal cone mosaic the images saved from the videos are flipped (see Ben Diagram: Y:\Adaptive_Optics\APLab_System\imageOrientationTest)
Resulting in the top of the image being the inferior retina, and the bottom of the image being the supior retina. (nasal temporal depend on the eye: for the right eye nasal is on the left and temporal is on the right)

------------------------------------------------------------
If you run ProcessConeDenisty (with processCD = 1), then you loop through the script calculate_coneDensity
which will produce:
1. a figure of plotted cone cells on top of the retinal cone mosaic
2. the voronoi cells on top of the retinal cone mosaic 

Both of these figures are in the 'image_analysis/subject_ID/which_eye/Voronoi_Images' (eps, fig, and png)

Additionally, the calculate_coneDensity script will give one mat file:
avgAreaPerCone_subjectinfo.mat (i.e avgAreaPerCone_Z055_R_01March2022)
	This .mat file has two variables
	1. avgAreaPerCone
	2. avgAreaPerCone_ForSTAND_ALONE_PLOTTING

The avgAreaPerCone variable is a matrix used for plotting heatmaps ontop of the retinal cone mosaic, and the second one is for just plotting the heatmap alone.


-------------------------------------------------------------
plot_coneDensity - saves one figure and that is the heatmap with the peak cone denisty and the PRL on top of the retinal cone mosaic.

heatmap is saved as Overlayed_HeatMap_subinfo (i.e. cone_desnity\image_analysis\Z055\Right\Overlayed_HeatMap_Z055_R_01March2022)

-----------------------------------------------------------------------