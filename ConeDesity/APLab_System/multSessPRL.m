

clear;clc;close all
%% Load the data

XCPath = 'C:/Users/ruccilab/Documents/adaptiveoptics/ConeDesity/APLab_System/data/XC/';
mastSess = {'Z151_R_2022_07_29','A197_R_2022_12_07_840', 'Z055_R_2022_03_01', 'Z160_R_2022_04_26','Z190_R_2022_11_11' };


% sub = {'Z151_R_2022_07_29', 'Z151_R_2022_10_07'};%subIDYYYYMMDD
sub = {'Z190_R_2022_06_03', 'Z190_R_2022_11_11'};%subIDYYYYMMDD
% sub ={'A197_R_2022_11_21',  'A197_R_2022_12_07_680'};

for ss = 1:length(sub)
    %     clearvars -except XCPath sub ss
    Data{ss} = load(sprintf('%s%s_CD_data.mat',XCPath,sub{ss}));

end



%% plot figures

for ss = 1:length(sub)
    %variables
    pixPerdeg= Data{ss}.pixPerdeg;
    oursCDC_x(ss)= Data{ss}.oursCDC_x;
    oursCDC_y(ss) = Data{ss}.oursCDC_y;
    PRLX_0CDC(ss)= ((Data{ss}.PRL_X-oursCDC_x(ss))/pixPerdeg)*60;
    PRLY_0CDC(ss) = ((oursCDC_y(ss)- Data{ss}.PRL_Y)/pixPerdeg)*60;
    PRLloc_all{:,ss} = Data{ss}.PRLloc_all;
    PRLind_0CDC{:,ss}= Data{ss}.PRLind_0CDC;
    avgPRLind_0CDC{ss} = mean(vertcat(Data{ss}.PRLind_0CDC{:}));

    xPRLall_0CDC{ss} = ((PRLloc_all{ss}(:,1) - oursCDC_x(ss))/pixPerdeg)*60;%switch order of opperationsbecause image origin is top left and the plotting is
    yPRLall_0CDC{ss} = ((oursCDC_y(ss) - PRLloc_all{ss}(:,2))/pixPerdeg)*60;
    masterImage(ss) = any(strcmp(mastSess,sub{ss}));
end

figure;
%making the figure
for ss = 1:length(PRLind_0CDC)
    a=plot(0,0, '*r', 'MarkerSize', 10,'LineWidth',2);hold on;

    cmapRange = colormap(jet(length(PRLind_0CDC{ss})));
    for tt = 1:length(PRLind_0CDC{ss})
        %         plot(PRLX_0CDC, PRLY_0CDC,...
        %             'o', 'MarkerSize', 10,'LineWidth',2,'MarkerFaceColor',cmapRange(tt,:),'MarkerEdgeColor',cmapRange(tt,:));hold on;
        plot(PRLind_0CDC{ss}{tt}(:,1), PRLind_0CDC{ss}{tt}(:,2) ,...
            '.','MarkerFaceColor',cmapRange(tt,:),'MarkerEdgeColor',cmapRange(tt,:)); hold on;
        plot(xPRLall_0CDC{ss}(tt),yPRLall_0CDC{ss}(tt),...
            'square','MarkerFaceColor',cmapRange(tt,:), 'MarkerEdgeColor',cmapRange(tt,:),'MarkerSize', 10);hold on;
        ellipseXY(PRLind_0CDC{ss}{tt}(:,1), PRLind_0CDC{ss}{tt}(:,2), 80, cmapRange(tt,:), 0);hold on;
        %             input ''
    end
end
xlim([-30 30])
ylim([-30 30])
xlabel('arcmin')
ylabel('arcmin')
text(-3, -24,'Inferior Retina')
text(-4, 24,'Superior Retina')
text(29, 0, 'Nasal Retina')
text(-30, 0, 'Temporal Retina')
%%
cmapRange = colormap(jet(length(sub)));
figure;
%  a=plot(0,0, '*r', 'MarkerSize', 10,'LineWidth',2);hold on;
%  plot(PRLX_0CDC(ss), PRLY_0CDC(ss), 'square', 'MarkerEdgeColor',[0 0 0], 'MarkerFaceColor',cmapRange(1,:),'MarkerSize', 10 );hold on;
%  plot(PRLX_0CDC(ss), PRLY_0CDC(2), 'square', 'MarkerEdgeColor',[0 0 0], 'MarkerFaceColor',cmapRange(2,:),'MarkerSize', 10 );hold on;

for ss = 1:length(sub)
    a=plot(0,0, '*r', 'MarkerSize', 10,'LineWidth',2);hold on;
    plot(PRLX_0CDC(ss), PRLY_0CDC(ss), 'square', 'MarkerEdgeColor',[0 0 0], 'MarkerFaceColor',cmapRange(ss,:),'MarkerSize', 10 );hold on;
    sessPRL=[];
    for tt = 1:length(PRLind_0CDC{ss})
        plot(PRLind_0CDC{ss}{tt}(:,1), PRLind_0CDC{ss}{tt}(:,2),...
            '.','MarkerFaceColor',cmapRange(ss,:),'MarkerEdgeColor',cmapRange(ss,:)); hold on;
        sessPRL = [sessPRL PRLind_0CDC{ss}{tt}'];
        %         input '';
    end
    sessPRLc{ss} = sessPRL';
    ellipseXY(sessPRLc{ss}(:,1), sessPRLc{ss}(:,2), 80, cmapRange(ss,:), 0);hold on;

    xlim([-30 30])
    ylim([-30 30])
    xlabel('arcmin')
    ylabel('arcmin')
    text(-3, -24,'Inferior Retina')
    text(-4, 24,'Superior Retina')
    text(29, 0, 'Nasal Retina')
    text(-30, 0, 'Temporal Retina')
end


% xLim = 20;
% yLim = 20;
% binWidth = 2;

xMax = 30;      % maximum value in x (arcmin)
yMax = 30;      % maximum value in y (arcmin)
binWidth = 1;   % bin width for histogram (arcmin)
xHist = (-xMax:binWidth:xMax);  % x values for histogram bin centers
yHist = (-yMax:binWidth:yMax);  % y values for histogram bin centers
[XH,YH] = meshgrid(xHist,yHist);    % generate grid of x and y points
xEdges = xHist-binWidth/2;      % x values for histogram bin edges
xEdges(end+1) = xEdges(end)+binWidth;   % extra value for last edge
yEdges = yHist-binWidth/2;      % y values for histogram bin edges
yEdges(end+1) = yEdges(end)+binWidth;   % extra value for last edge



for ss = 1:length(sub)
    figure;
    % h1 = histogram2(dataAll.Scotoma.allXCleanFIX,dataAll.Scotoma.allYCleanFIX,...
    %     xEdges,yEdges,'DisplayStyle','tile','ShowEmptyBins','off',...
    %     'Normalization','probability','EdgeColor','none');
    h1 = histogram2(sessPRLc{ss}(:,1), sessPRLc{ss}(:,2),...
        xEdges,yEdges,'DisplayStyle','tile','ShowEmptyBins','off',...
        'Normalization','probability','EdgeColor','none');hold on;


    % h1 = histogram2(sessPRLc{ss}(:,1), sessPRLc{ss}(:,2), ...
    %     'BinWidth',[binWidth, binWidth], 'DisplayStyle','tile', 'XBinLimits', [-xLim xLim], 'YBinLimits',[-yLim,yLim], ...
    %     'ShowEmptyBins','off','Normalization','probability','EdgeColor','none');hold on;
    plot(PRLX_0CDC(ss), PRLY_0CDC(ss), 'square', 'MarkerFaceColor',[0 0 0], 'MarkerEdgeColor',cmapRange(ss,:),'MarkerSize', 10 );hold on;
    figure;
    plot(0,0, '*r', 'MarkerSize', 10,'LineWidth',2);hold on;
    %in countour X verticies are first parameter and x-coordinates of the vertices corresponding to column indices
    contour(YH,XH, h1.Values, 'showText', 'on');hold on;
    plot(PRLX_0CDC(ss), PRLY_0CDC(ss), 'square', 'MarkerFaceColor',[0 0 0], 'MarkerEdgeColor',cmapRange(ss,:),'MarkerSize', 10 );hold on;

end

for ss = 1: length(sub)
    sPRL_x = sessPRLc{ss}(:,1);
    sPRL_y = sessPRLc{ss}(:,2);

    eucDist_iPRL{ss} = sqrt((sPRL_x - 0).^2 + (sPRL_y - 0).^2);

end

figure; histogram(eucDist_iPRL{1});hold on;
histogram(eucDist_iPRL{2});hold on;



%% plotting the two sesssion on the global reference image
% ALL prl session are aligned to the CDC of the master image

m_sess = find(masterImage ==1 );%finding which session is used as the master image
M_refImage = Data{m_sess}.refImage; % denoting which images is the master image
PRLall_mastSess = (vertcat(Data{m_sess}.PRLind_arc{:})/60)*pixPerdeg;% getting the PRLs from the master image in pixels
m_CDCx = oursCDC_x(m_sess);%saving master CDCx
m_CDCy = oursCDC_y(m_sess);%saving master CDCy


figure;
image(M_refImage);colormap gray; hold on;
plot(PRLall_mastSess(:,1),PRLall_mastSess(:,2), '.c');hold on;
plot(Data{m_sess}.PRL_X,Data{m_sess}.PRL_Y, 'cs', 'MarkerFaceColor', 'c', 'MarkerEdgeCOlor', 'k', 'Markersize', 10); hold on;
for dd = 1:length(Data)
    if  ~any(strcmp(mastSess,sub(dd)))
        %adding the cdc cooridinates to the CDC centerd prls of the other sessions.
        PRLall_sessN{dd}(:,1) = m_CDCx+ (sessPRLc{dd}(:,1)/60)*pixPerdeg;
        PRLall_sessN{dd}(:,2) = m_CDCy + (sessPRLc{dd}(:,2)/60)*pixPerdeg;

        shiftX(dd) = m_CDCx - oursCDC_x(dd);
        shiftY(dd) = m_CDCy - oursCDC_y(dd);
        plot(PRLall_sessN{dd}(:,1),PRLall_sessN{dd}(:,2), '.b');hold on;
        plot(m_CDCx +PRLX_0CDC(dd),m_CDCy +PRLY_0CDC(dd), 'bs', 'MarkerFaceColor', 'b', 'MarkerEdgeCOlor', 'k', 'Markersize', 10); hold on;

        PRLpixDiff(dd) = sqrt((Data{m_sess}.PRL_X -  m_CDCx +PRLX_0CDC(dd))^2 + (Data{m_sess}.PRL_Y - m_CDCy +PRLY_0CDC(dd))^2);

    end
end

PRLarcDiff = (PRLpixDiff/pixPerdeg)*60;
plot(m_CDCx, m_CDCy, 'r*', 'MarkerSize', 10, 'LineWidth',2); hold on;

%text and sapce key
    text(m_CDCx-50, m_CDCy + 250,'Inferior Retina', 'Color' ,'w')
    text(m_CDCx-50, m_CDCy - 250,'Superior Retina', 'Color' ,'w')
    text(m_CDCx + 200, m_CDCy, 'Nasal Retina', 'Color' ,'w')
    text(m_CDCx - 300, m_CDCy, 'Temporal Retina', 'Color' ,'w')

    ws = (10/60)*pixPerdeg;    % 10 arcmin
    szI = size(M_refImage);
    hs = 20;
    px = (m_CDCx - 300)+20;                % position in x
    py =(m_CDCy + 300) - 50 - hs;   % position in y
    r = rectangle('Position',[px,py,ws,hs]);
    r.FaceColor = [1 1 1];
    t = text(px,py-1.5*hs,'10 arcmin');
    t.Color = [1 1 1];

   text(m_CDCx+100, 50, sprintf('%s', sub{m_sess}), 'Color' ,'g')
   text(m_CDCx+100, 100, sprintf('PRLdist Pixels %s \n PRLdist arcmin : %s', PRLpixDiff,PRLarcDiff), 'Color' ,'g')

%%

figure;plot([PRLind_0CDC{1}{1}; PRLind_0CDC{1}{2};PRLind_0CDC{1}{3}; PRLind_0CDC{1}{4}; PRLind_0CDC{1}{5}])


allPRL_0CDC = cellfun(@(z) vertcat(z{:}), PRLind_0CDC, 'UniformOutput', false);

allPRL_mastImage = vertcat(PRLall_mastSess, PRLall_sessN{:});
prlDistCdc_Marc = m_CDCx+ (allPRL_mastImage/60)*pixPerdeg;

figure;
histogram(prlDistCdc_Marc)
