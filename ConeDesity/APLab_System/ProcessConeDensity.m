
clear;clc;close all;

%%% Change this path to where the cone_desnity folder is located (don't include 'cone_density' in the root path)
%%% the slahsed need to be back slashes "/"


% RootPath_Reiniger = 'C:/Users/sjenks/Desktop/research/rochester/AO/adaptiveoptics/ConeDesity/ReinigerConeDensity/';
RootPath_Reiniger = 'C:/Users/ruccilab/Documents/adaptiveoptics/ConeDesity/ReinigerConeDensity/';
% RootPath = 'C:/Users/sjenks/Desktop/research/rochester/AO/adaptiveoptics/ConeDesity/APLab_System/data/';
RootPath = 'C:/Users/ruccilab/Documents/adaptiveoptics/ConeDesity/APLab_System/data/';


%----------------------------------------------------------------------
%subject info
%Z055 right eye March 1,2022
Data(1).DataFileName    = 'Z055_R_2022_03_01';
Data(1).PRLFileName     = 'Z055_PRL_analysis_2022_03_01.mat';
Data(1).TomeSavePath    = 'Y:/Adaptive_Optics/APLab_System/Z055_2022_03_01/ConeDensity/';

%Z160 right eye March 1,2022
Data(2).DataFileName    = 'Z160_R_2022_03_01';
Data(2).PRLFileName     = 'Z160_PRL_analysis_2022_03_01.mat';
Data(2).TomeSavePath    = 'Y:/Adaptive_Optics/APLab_System/Z160_2022_03_01/ConeDensity/';

%Z160 right eye April 26, 2022
Data(3).DataFileName    = 'Z160_R_2022_04_26';
Data(3).PRLFileName     = 'Z160_PRL_analysis_2022_04_26.mat';
Data(3).TomeSavePath    = 'Y:/Adaptive_Optics/APLab_System/Z160_2022_04_26/ConeDensity/';

%Z190 right eye June 3, 2022
Data(4).DataFileName    = 'Z190_R_2022_06_03';
Data(4).PRLFileName     = 'Z190_840nm_PRL_analysis_2022_06_03.mat';%840 was the imaging wave, but the file is referring to what they were fixating on.. the 680 was just the center of the red box.
Data(4).TomeSavePath    = 'Y:/Adaptive_Optics/APLab_System/Z190_2022_06_03/ConeDensity/';

%Z024 right eye July 28, 2022
Data(5).DataFileName    = 'Z024_R_2022_07_28';
Data(5).PRLFileName     = 'Z024_PRL_analysis_2022_07_28.mat';%840 was the imaging wave, but the file is referring to what they were fixating on.. the 680 was just the center of the red box.
Data(5).TomeSavePath    = 'Y:/Adaptive_Optics/APLab_System/Z024_2022_07_28/ConeDensity/';

%Z151 right eye July 29, 2022
Data(6).DataFileName    = 'Z151_R_2022_07_29';
Data(6).PRLFileName     = 'Z151_PRL_analysis_2022_07_29.mat';%840 was the imaging wave, but the file is referring to what they were fixating on.. the 680 was just the center of the red box.
Data(6).TomeSavePath    = 'Y:/Adaptive_Optics/APLab_System/Z151_2022_07_29/ConeDensity/';

%Z151 right eye October 10, 2022
Data(7).DataFileName    = 'Z151_R_2022_10_07';
Data(7).PRLFileName     = 'Z151_PRL_analysis_2022_10_07.mat';%840 was the imaging wave, but the file is referring to what they were fixating on.. the 680 was just the center of the red box.
Data(7).TomeSavePath    = 'Y:/Adaptive_Optics/APLab_System/Z151_2022_10_07/ConeDensity/';

%A188 right eye October 17, 2022
Data(8).DataFileName    = 'A188_R_2022_10_17';
Data(8).PRLFileName     = 'A188_PRL_analysis_2022_10_17.mat';%840 was the imaging wave, but the file is referring to what they were fixating on.. the 680 was just the center of the red box.
Data(8).TomeSavePath    = 'Y:/Adaptive_Optics/APLab_System/A188_2022_10_17/ConeDensity/';

%Z126 right eye November 03, 2022
Data(9).DataFileName    = 'Z126_R_2022_11_03';
Data(9).PRLFileName     = 'Z126_PRL_analysis_2022_11_03.mat';%840 was the imaging wave, but the file is referring to what they were fixating on.. the 680 was just the center of the red box.
Data(9).TomeSavePath    = 'Y:/Adaptive_Optics/APLab_System/Z126_2022_11_03/ConeDensity/';

%Z190 right eye November 11, 2022
Data(10).DataFileName    = 'Z190_R_2022_11_11';
Data(10).PRLFileName     = 'Z190_PRL_analysis_2022_11_11.mat';%840 was the imaging wave, but the file is referring to what they were fixating on.. the 680 was just the center of the red box.
Data(10).TomeSavePath    = 'Y:/Adaptive_Optics/APLab_System/Z190_2022_11_11/ConeDensity/';

%A197 right eye December 07, 2022 %imaged with 680nm light
Data(11).DataFileName    = 'A197_R_2022_12_07_680';
Data(11).PRLFileName     = 'A197_PRL_analysis_2022_12_07_680.mat';%840 was the imaging wave, but the file is referring to what they were fixating on.. the 680 was just the center of the red box.
Data(11).TomeSavePath    = 'Y:/Adaptive_Optics/APLab_System/A197_2022_12_07/ConeDensity/';

%A197 right eye November 21, 2022 %imaged with 680
Data(12).DataFileName    = 'A197_R_2022_11_21';
Data(12).PRLFileName     = 'A197_PRL_analysis_2022_11_21.mat';%840 was the imaging wave, but the file is referring to what they were fixating on.. the 680 was just the center of the red box.
Data(12).TomeSavePath    = 'Y:/Adaptive_Optics/APLab_System/A197_2022_11_21/ConeDensity/';
% 
% %A197 right eye November 21, 2022 %imaged with 840
% Data(13).DataFileName    = 'A197_R_2022_12_07_840';
% Data(13).PRLFileName     = 'A197_PRL_analysis_2022_12_07_840.mat';%840 was the imaging wave, but the file is referring to what they were fixating on.. the 680 was just the center of the red box.
% Data(13).TomeSavePath    = 'Y:/Adaptive_Optics/APLab_System/A197_2022_12_07/ConeDensity/';

%A144 right eye December 14, 2022
Data(13).DataFileName    = 'A144_R_2022_12_14';
Data(13).PRLFileName     = 'A144_PRL_analysis_2022_12_14.mat';
Data(13).TomeSavePath    = 'Y:/Adaptive_Optics/APLab_System/A144_2022_12_14/ConeDensity/';

%A144 right eye December 14, 2022
Data(14).DataFileName    = 'Z104_R_2023_02_24';
Data(14).PRLFileName     = 'Z104_2023_02_24_PRL_analysis';
Data(14).TomeSavePath    = 'Y:/Adaptive_Optics/APLab_System/Z104_2023_02_24/ConeDensity/';

%A144 right eye December 14, 2022
Data(15).DataFileName    = 'A122_R_2023_02_08';
Data(15).PRLFileName     = '';
Data(15).TomeSavePath    = 'Y:/Adaptive_Optics/APLab_System/A122_2023_02_08/ConeDensity/';



subject = 6%4:14;%7:length(Data); 



AplabData = 1;
%Process Cone Density?
processCD = 0;
autoUdateTable = 0;


if ~AplabData
DataFilePath = sprintf('%sConeLocations/',RootPath_Reiniger); 
LIST = dir(DataFilePath);
LIST = LIST(3:end);
    for nfiles = 1:length(LIST)
        load([DataFilePath LIST(nfiles).name]);
        DataFileName = LIST(nfiles).name;
        ImageName = imagename;
        SaveFigPath = sprintf('%sConeDesity/ReinigerConeDensity/Figures/',RootPath_Reiniger);
        pixPerdeg = 600;
        if processCD
            caculate_coneDensity_2(DataFilePath, DataFileName, pixPerdeg);
%             caculate_coneDensity(DataFilePath, DataFileName, pixPerdeg,AplabData);
            close all;
            sprintf('finished with %s',ImageName);
        else
            CDPath = sprintf('%sAvgConesPerPix/712X712/',RootPath_Reiniger); 
            CDList = dir(CDPath);
%             CDList = CDList(4:end-4);
            CDList = CDList(3:end);
            CDName = CDList(nfiles).name;
            CDImage = [DataFilePath LIST(nfiles).name];
            
%             plot_coneDensity(RootPath, DataFileName,Data(ii).PRLFileName,Data(ii).TomeSavePath,AplabData)
%             
            plot_coneDensity(CDPath,CDName,CDImage,SaveFigPath,pixPerdeg,AplabData,nfiles,0);
            close all;
        end

    end
else
    %Plot the orientation of the AO system images as of 7/22
    OrientSchem = imread([RootPath 'orientationOfVideoFrames_2022-06-22.png']);
    figure;
    imshow(OrientSchem);
    
     pixPerdeg = 512;
    for ii = subject
        if processCD
            caculate_coneDensity_2(RootPath,Data(ii).DataFileName, pixPerdeg)
%             caculate_coneDensity(RootPath,Data(ii).DataFileName, pixPerdeg,AplabData)
%             caculate_coneDensity(Data(ii).DataFilePath, Data(ii).ImageName, Data(ii).ConeLocName,Data(ii).SaveVoroniFigPath,AplabData);
            close all;
            sprintf('finished with %s', Data(ii).DataFileName)
        end
        if strcmp(Data(ii).DataFileName, '')
            warning('enter a data name')
            break;
        else
            plot_coneDensity(RootPath,Data(ii).DataFileName,Data(ii).PRLFileName,Data(ii).TomeSavePath,pixPerdeg,AplabData,ii,autoUdateTable)
%             plot_coneDensity(Data(ii).DataFilePath,Data(ii).DataFileName,Data(ii).ImageName,Data(ii).SaveFigPath,AplabData);
        end
    end
end



