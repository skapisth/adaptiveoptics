
%% this is a script to see how well you tagged! 

% steps: 
% 1) change the DataFilePath folder. Make sure they are backslashes and that the path ends with a back slash.
% 2) make sure the Imagefile is the correct one 
% 3) Change the textfile for Imagename (leave out any extensions)
%     a) ***Note you have to have only 2 collumns for the text files. 

%% Parameters 
DataFilePath = 'C:/Users/ruccilab/Box/APLab-Projects/AO/coneTagTraining/For Meeting 6-30-23/';

Imagefile = ['P01_L_refImage' '.tif'];% Reniger image 
ImageName = 'Ashley_Attempt1_P01_L';% name of your text file without the .txt 
nSubjects = 1; 

%% beginning of the script 

pixPerdeg = 600;% reiniger images are 600pix per deg

for ii = nSubjects
%     ImageName = sprintf('tagger%i',ii);
    % function caculate_coneDensity_2(DataFilePath,ImageName,pixPerdeg)
    warning(['The main output of this function (avgAreaPerCone) is a matrix that is designed to be plotted onto the retinal cone mosaic...' ...
        'If you want to plot just the cone density by itself the data must be fliped up-down (udflip()) or use the other output of this function ' ...
        '(avgAreaPerCone_ForSTAND_ALONE_PLOTTING)'])
    % roorda -are cone locations from roorda's gui, yes = 1
    %run either Reineger data or script that gets the cone coordinates
    
    %load image, and then load the cone locations.
    pname_1 = DataFilePath;
    fname_1 = Imagefile;


    coneMosaic = imread([DataFilePath Imagefile]);
    ConeSize = [size(coneMosaic,1) size(coneMosaic,2)];
    fid = fopen([DataFilePath ImageName '.txt'],'r');
    cone_data = textscan(fid, '%f %f');
    ConeCoord = [cone_data{2} cone_data{1}]; %NOTE the inversion (see above)




    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%
    figure;
    colormap('gray');
    imshow(coneMosaic)% Imshow will plot the figure in correct orientation
    hold on;

    %%% plot the cone coordinates on the retinal cone image (before and after rotation)
    figure;
    colormap('gray');
    imshow(coneMosaic)% Imshow will plot the figure in correct orientation
    hold on;
    plot(ConeCoord(:,1),ConeCoord(:,2), '.r')
    extensions = {'eps','fig','png'};
    for k = 1:length(extensions)
        saveas(gcf, sprintf('%s_ConeTags_PJ',[DataFilePath '/Voronoi_Images/' ImageName]), extensions{k})
    end


    DT = delaunayTriangulation(ConeCoord); %triangulate an area with cones a verticies
    [V,r] = voronoiDiagram(DT);%calculates the voronoi regions (3-7 verticies) based off the triangulateion
    Xpoints = DT.Points(:,1); %get the cone coordiantes horizontal
    Ypoints = DT.Points(:,2);%get the cone coordiantes Vertical

    %visualize the delauanyTriangulation on top of the retinal cone image
    hold on;
    gg = triplot(DT);
    hold on ;
    voronoi(Xpoints,Ypoints,DT.ConnectivityList);% plot the voronoi cells (cones at center)


    %%%
    %Another plot to verify that the voronoi cells are on top of the cones.
    figure;
    colormap('gray');
    imshow(coneMosaic)% Imshow will plot the figure in correct orientation
    hold on;
    voronoi(Xpoints,Ypoints,DT.ConnectivityList);% plot the voronoi cells (cones at center)
    legend(sprintf('%s',Imagefile ))

    extensions = {'eps','fig','png'};
    for k = 1:length(extensions)
        saveas(gcf, sprintf('%s_Voronoi_PJ',[DataFilePath '/Voronoi_Images/' ImageName]), extensions{k})
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %http://www.qhull.org/download/  ==>Just use a reference
    [v,c] = voronoin(DT.Points); %list the voronoi cells vertices and its regions


    xVorCell = v(:,1);% horizontal cordinates for voronoi cell
    yVorCell = v(:,2);% vertical cordinates for voronoi cell


    %
    %
    % xVorCell_Deg = v(:,1);
    % yVorCell = v(:,2);
    % %grabing the points in v from indicies c
    % %gets the x and y cordinates for each vertice of the voronoi cell
    % VoroniCoor = cellfun(@(z) [xVorCell(z(:)) yVorCell(z(:))], c(:), 'UniformOutput', false);


    %for each pixel get the 150 nearest cone cells and caculate the average for
    %each 150 cones and then summ the average up and divide by 150 cells
    %the input of this loop should be pixels and then you get pixels squared
    %per 150 cones.

    %%% Code from PJ June 5, 2023
    tic
    vc = cellfun(@(x) v(x,:),c,'UniformOutput',false);
    areas = cellfun(@(x) polyarea(x(:,1), x(:,2)),vc,'UniformOutput',false);
    [PixR,PixC] = ndgrid(1:ConeSize(2),1:ConeSize(1));
    idxs = knnsearch(DT.Points,[PixR(:),PixC(:)] , 'k', 150);
    v_areas = areas(idxs);
    avgArea = nanmean(cell2mat(v_areas)')';
    avgAreaPerCone = reshape(avgArea,ConeSize(2),[])';
    toc
    %%%


    avgAreaPerCone_flipUD = flipud(avgAreaPerCone);

    Inferior_MaxY = avgAreaPerCone; %superior retina at bottom of image, therefore superior retina is at the maximum Y value
    Superior_MaxY = avgAreaPerCone_flipUD;% flip so,superior retina at top of image, therefore inferior retina is at the maximum Y value

    avgAreaPerCone = Inferior_MaxY;
    supRet_topImage = avgAreaPerCone;
    refImage = coneMosaic;



    %%% Get avgerage area per cone and convert the pixels to degrees.
    CD_conePerPixsq = 1./supRet_topImage;
    CD_conePerDegsq= CD_conePerPixsq * (pixPerdeg).^2;
    CD_conePerDegsq(isinf(CD_conePerDegsq)) = 0;

    ConeCoord(:,2) = (size(CD_conePerDegsq,1) - cone_data{1}) -1;

    figure;
    colormap('gray');
    imshow(refImage);hold on;

    plot(ConeCoord(:,1),ConeCoord(:,2),'r.')
    save(sprintf('%s/avgAreaPerCone_PJ_%s.mat',DataFilePath,ImageName),'avgAreaPerCone', 'CD_conePerDegsq','refImage','ConeCoord')



    CDC_percentile = 20;

    %%% Get Peak Cone Density
    %getting the maximum cones to compare to reinigers peak cone densitys
    [PCD  PCD_index]= max(max(CD_conePerDegsq));
    big_idx = find(CD_conePerDegsq == PCD );
    [oursPCD_y,oursPCD_x] = ind2sub(size(CD_conePerDegsq),big_idx);

    APC_Prctile = prctile(avgAreaPerCone,CDC_percentile,1);
    threshCDC = prctile(APC_Prctile,CDC_percentile,2);

    APC = avgAreaPerCone ;

    APC(APC<threshCDC)=1;
    APC(APC>=threshCDC)=0;

    measurements = regionprops(APC, refImage, 'WeightedCentroid');
    centerOfMass = measurements.WeightedCentroid;
    oursCDC_x = centerOfMass(1);
    oursCDC_y = centerOfMass(2);
    %%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%
    CD_nyq = ((1/2)*sqrt((2/sqrt(3)).*CD_conePerDegsq));



    %%

    % if AplabData
    figure;imshow(APC);
    hold on;
    plot(oursCDC_x, oursCDC_y, 'r*', 'LineWidth', 2, 'MarkerSize', 16);
    % end
    extensions = {'eps','fig','png'};
    for k = 1:length(extensions)
        saveas(gcf, sprintf('%s_CDC',[DataFilePath 'OverlayImages/' ImageName]), extensions{k})
    end

    %https://www.mathworks.com/help/thingspeak/create-heatmap-overlay-image.html
    figure;
    image(refImage);
    hold on
    OverlayImage = imagesc(CD_conePerDegsq,[min(CD_conePerDegsq(:)) max(CD_conePerDegsq(:))]);
    %set(gca,'xticklabel',xt,'yticklabel',yt)

    % xticklabels([1:size(CD_conePerDegsq,2)/6:size(CD_conePerDegsq,2)]/pixPerdeg)
    hold on
    alpha(0.3)
    colorbar;
    hold on


    a=plot(oursPCD_x,oursPCD_y,'^k'); hold on;
    c=plot(oursCDC_x,oursCDC_y,'*b'); hold on;
    halfFrame =   (.85*pixPerdeg)/2;
    

    caxis([0 18000])
    legend(sprintf('%s\n PCD',ImageName),'PRL','CDC')

    extensions = {'eps','fig','png'};
    for k = 1:length(extensions)
        saveas(gcf, sprintf('%s_OverlayedImage',[DataFilePath '/OverlayImages/' ImageName]), extensions{k})
    end
    %%

    %%% PCD %%%
    hozrPCD_col = CD_conePerDegsq(oursPCD_y - 5: oursPCD_y + 5,:);
    vertPCD_row = CD_conePerDegsq(:,oursPCD_x-5:oursPCD_x + 5)';
    %convert the pixels to degrees (base off image)
    pixX_PCDtrimed = [1:length(vertPCD_row)];
    XaxisPCD_DEG = pixX_PCDtrimed/pixPerdeg;
    pixY_PCDtrimed = [1:length(hozrPCD_col)];
    YaxisPCD_DEG = pixY_PCDtrimed/pixPerdeg;
    normPCD_Xaxis_deg = XaxisPCD_DEG - (oursPCD_y/pixPerdeg);
    normPCD_Yaxis_deg = YaxisPCD_DEG - (oursPCD_x /pixPerdeg);


    %%% CDC %%%
    hozrCDC_col = CD_conePerDegsq(oursCDC_y - 5: oursCDC_y + 5,:);
    vertCDC_row = CD_conePerDegsq(:,oursCDC_x-5:oursCDC_x + 5)';
    %convert the pixels to degrees (base off image)
    pixX_CDCtrimed = [1:length(vertCDC_row)];
    XaxisCDC_DEG = pixX_CDCtrimed/pixPerdeg;
    pixY_CDCtrimed = [1:length(hozrCDC_col)];
    YaxisCDC_DEG = pixY_CDCtrimed/pixPerdeg;
    normCDC_Xaxis_deg = XaxisCDC_DEG - (oursCDC_y/pixPerdeg);
    normCDC_Yaxis_deg = YaxisCDC_DEG - (oursCDC_x /pixPerdeg);


    figure;
    % plot(normPCD_Yaxis_deg,mean(hozrPCD_col), 'r');%horizontal meridian
    hold on;
    plot(normPCD_Xaxis_deg*60, mean(vertPCD_row), 'k'); %vertical meridian
    hold on;
    plot(normCDC_Xaxis_deg*60,mean(vertCDC_row), 'c');
    legend('PCD', 'CDC')

    text(-25,15000,'Superior Retina')
    text(15,15000,'Inferior Retina')

    title('Cone Density centered around PCD')
    xlabel('Eccentricity (arcmin)')
    ylabel('Cone Density (cones/degree^2)')
    ylim([6000, 18000])

    extensions = {'eps','fig','png'};
    for k = 1:length(extensions)
        saveas(gcf, sprintf('%s_VertDist',[DataFilePath '/Distribution/' ImageName]), extensions{k})
    end
end
