

%%

pixPerdeg = 512;

% ConeCoord = the cone coordinates of a given area


DT = delaunayTriangulation(ConeCoord);
figure;
gg = triplot(DT);

% dist = sqrt((x2 - x1).^2+ (Y2-y1)^2);
col1 = DT.Points(DT.ConnectivityList(:,1),:);

col2 = DT.Points(DT.ConnectivityList(:,2),:);

col3 = DT.Points(DT.ConnectivityList(:,3),:);

dist1_pix = sqrt((col2(:,1) - col1(:,1)).^2+ (col2(:,2)-col1(:,2)).^2);
dist2_pix = sqrt((col3(:,1) - col2(:,1)).^2+ (col3(:,2)-col2(:,2)).^2);

dist1_arc = (dist1_pix/pixPerdeg)*60;
dist2_arc = (dist2_pix/pixPerdeg)*60;

avgDist_c2c = mean([dist1_arc; dist2_arc]);