

%% Script for Comparing cone density with performance
clear;clc;close all;
%% Load data
subject = 'Z055';

CDC_percentile = 20;

%loading average distance
load('C:\Users\sjenks\Desktop\research\rochester\AO\adaptiveoptics\ConeDesity\APLab_System\data\distfromTarg_F.mat');

if strcmp(subject, 'Z055')%4
    %load Matrix
    % load('C:\Users\ruccilab\Documents\adaptiveoptics\ConeDesity\APLab_System\data\Z055\Right\avgAreaPerCone_Z055_R_01March2022.mat');
    load('C:\Users\sjenks\Desktop\research\rochester\AO\adaptiveoptics\ConeDesity\APLab_System\data\Z055_R_2022_03_01\avgAreaPerCone_Z055_R_2022_03_01.mat');
    %load PRL
    % load('C:\Users\ruccilab\Documents\adaptiveoptics\ConeDesity\APLab_System\data\Z055\Right\Z055_PRL_analysis_2022_03_01.mat');
    load('C:\Users\sjenks\Desktop\research\rochester\AO\adaptiveoptics\ConeDesity\APLab_System\data\Z055_R_2022_03_01\Z055_PRL_analysis_2022_03_01.mat');
    %behavioral
    % load('C:\Users\ruccilab\Documents\adaptiveoptics\ConeDesity\APLab_System\data\Z055\Right\Z055_RGB100-DDPI_ALL3Sess_N0_combined_summary_dist10.mat');
    load('C:\Users\sjenks\Desktop\research\rochester\AO\adaptiveoptics\ConeDesity\APLab_System\data\Z055_R_2022_03_01\Z055_RGB100-DDPI_ALL3Sess_N0_combined_summary_dist10.mat');
    %  refImage = imread('C:\Users\ruccilab\Documents\adaptiveoptics\ConeDesity\APLab_System\data\Z055\Right\Z055_R_01March2022.tif');
    refImage = imread('C:\Users\sjenks\Desktop\research\rochester\AO\adaptiveoptics\ConeDesity\APLab_System\data\Z055_R_2022_03_01\Z055_R_2022_03_01.tif');
    subID = 4;
    
    % load('C:\Users\sjenks\Desktop\research\rochester\AO\adaptiveoptics\ConeDesity\APLab_System\data\Z160\Right\Z160_PRL_analysis_2022_03_01.mat');
elseif strcmp(subject, 'Z151') %2
    load('C:\Users\sjenks\Desktop\research\rochester\AO\adaptiveoptics\ConeDesity\APLab_System\data\Z151_R_2022_07_29\avgAreaPerCone_Z151_R_2022_07_29.mat');
    load('C:\Users\sjenks\Desktop\research\rochester\AO\adaptiveoptics\ConeDesity\APLab_System\data\Z151_R_2022_07_29\Z151_PRL_analysis_2022_07_29.mat');
    load('C:\Users\sjenks\Desktop\research\rochester\AO\adaptiveoptics\ConeDesity\APLab_System\data\Z151_R_2022_07_29\Z151_RGB40-DDPI_ALL4Sess_N0ST4_combined_summary.mat')
    refImage = imread('C:\Users\sjenks\Desktop\research\rochester\AO\adaptiveoptics\ConeDesity\APLab_System\data\Z151_R_2022_07_29\Z151_R_2022_07_29.tif');
    subID = 2;
    %     load('C:\Users\ruccilab\Documents\adaptiveoptics\ConeDesity\APLab_System\data\Z151\Right\avgAreaPerCone_Z151_R_29July2022.mat');
    %     load('C:\Users\ruccilab\Documents\adaptiveoptics\ConeDesity\APLab_System\data\Z151\Right\Z151_PRL_analysis_2022_07_29.mat');
    %
end

load('C:\Users\sjenks\Desktop\research\rochester\AO\adaptiveoptics\ConeDesity\FVP.mat')


% rename the variable
Superior_Up = avgAreaPerCone; % Use when plotting origin is top bottom on y axis: avgAreaPerCone is a matrix that has inferior saved up, but since eye candy has inferior up use this matrix for proper orientation (for stand alone heatmap)
Inferior_Down = avgAreaPerCone_flipUD;%ForSTAND_ALONE_PLOTTING; % Use when plotting origin is top left on y axis: a matrix that has superior saved up  but since eye candy has inferior up use this matrix to plot on top of the reintal cone mosaic

avgAreaPerCone = Inferior_Down;
avgAreaPerCone_flipUD = Superior_Up;

pixPerdeg = 512;

%% Convert Matrix to number of Cones per degree squared

%%% Get avgerage area per cone and convert the pixels to degrees.
CD_conePerPixsq = 1./avgAreaPerCone;
CD_conePerDegsq= CD_conePerPixsq * (pixPerdeg).^2;
CD_conePerDegsq(isinf(CD_conePerDegsq)) = 0;

%%% Do the same for a stand alone plot
% the triangulation that the avgAreaPerCone is based off, is flipped over the x axis, so it is correctly oriented when placed on the retinal cone mosaic.
% if you want to plot a stand alone heat map use below
CD_conePerPixsq_flipUD = 1./avgAreaPerCone_flipUD;
CD_conePerDegsq_flipUD= CD_conePerPixsq_flipUD * (pixPerdeg).^2;
CD_conePerDegsq_flipUD(isinf(CD_conePerDegsq_flipUD)) = 0;

%%% Get Peak Cone Density
[PCD  PCD_index]= max(max(CD_conePerDegsq));
big_idx = find(CD_conePerDegsq == PCD );
[oursPCD_y,oursPCD_x] = ind2sub(size(CD_conePerDegsq),big_idx);



figure;imagesc(avgAreaPerCone)
APC_Prctile = prctile(avgAreaPerCone,CDC_percentile,1);
threshCDC = prctile(APC_Prctile,CDC_percentile,2);

APC =avgAreaPerCone ;

APC(APC<threshCDC)=1;
APC(APC>=threshCDC)=0;
gray_CD = mat2gray(CD_conePerDegsq);

figure;imshow(APC);
measurements = regionprops(APC,gray_CD , 'WeightedCentroid');
oursCDC = measurements.WeightedCentroid;
oursCDC_x = oursCDC(1);
oursCDC_y = oursCDC(2);
hold on;
plot(oursCDC_x, oursCDC_y, 'r*', 'LineWidth', 2, 'MarkerSize', 16);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% PCD
%trim the edges of the matrix
hozrPCD_col = CD_conePerDegsq(oursPCD_y - 5: oursPCD_y + 5,:);
vertPCD_row = CD_conePerDegsq(:,oursPCD_x-5:oursPCD_x + 5)';
%convert the pixels to degrees (base off image)
pixX_PCDtrimed = [1:length(vertPCD_row)];
XaxisPCD_DEG = pixX_PCDtrimed/pixPerdeg;
pixY_PCDtrimed = [1:length(hozrPCD_col)];
YaxisPCD_DEG = pixY_PCDtrimed/pixPerdeg;
normPCD_Xaxis_deg = XaxisPCD_DEG - (oursPCD_y/pixPerdeg);
normPCD_Yaxis_deg = YaxisPCD_DEG - (oursPCD_x /pixPerdeg);




% %%% PRL
% 
% if exist('PRLx','var') == 1
%     size(avgAreaPerCone)
%     
%     PRL_X = round(PRLx,0);
%     PRL_Y = round(size(avgAreaPerCone,1) - PRLy,0);
% elseif exist('PRL_all','var') == 1
%     PRL_X = round(PRL_all(1),0);
%     PRL_Y = round(size(avgAreaPerCone,1) - PRL_all(2),0);
% else
%     PRL_X = round(PRL_x,0);
%     PRL_Y = round(size(avgAreaPerCone,1) - PRL_y,0);
% end

%trim the edges of the matrix
hozrPRL_col = CD_conePerDegsq(PRL_Y- 5: PRL_Y + 5,:);
vertPRL_row = CD_conePerDegsq(:,PRL_X- 5 : PRL_X + 5)';
%convert the pixels to degrees (base off image)
pixX_PRLtrimed = [1:length(vertPRL_row)];
XaxisPRL_DEG = pixX_PRLtrimed/pixPerdeg;
pixY_PRLtrimed = [1:length(hozrPRL_col)];
YaxisPRL_DEG = pixY_PRLtrimed/pixPerdeg;

normPRL_Xaxis_deg = XaxisPRL_DEG - (PRL_Y/pixPerdeg);
normPRL_Yaxis_deg = YaxisPRL_DEG - (PRL_X/pixPerdeg);




%Plot Cone density around peak cone density
figure;
% plot(normPCD_Yaxis_deg,mean(hozrPCD_col), 'r');%horizontal meridian
hold on;
plot(normPCD_Xaxis_deg*60, mean(vertPCD_row), 'c'); %vertical meridian
hold on;
plot(normPRL_Xaxis_deg*60,mean(vertPRL_row), 'k'); 

title('Cone Density centered around PCD')
xlabel('Degrees Eccentricity')
ylabel('Cone Density (cones/degree^2)')
ylim([6000, 18000])
legend('horizontal', 'vertical')


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %%% CDC
% CDC_centerDeg_x = degX_trimed - (CDC_x/pixPerdeg);
% CDC_centerDeg_y = degY_trimed - (CDC_y/pixPerdeg);
%
%
% %Plot Cone density around peak cone density
% figure;
% plot(CDC_centerDeg_y, xPCD_row, 'r'); %horizontal meridian
% hold on;
% plot(CDC_centerDeg_x,yPCD_col, 'g'); %vertical meridian
% title('Cone Density centered around the CDC')
% xlabel('Degrees Eccentricity')
% ylabel('Cone Density (cones/degree^2)')
% ylim([6000, 18000])

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% PRL

% if exist('PRLx','var') == 1
%     size(avgAreaPerCone)
%     
%     PRL_X = round(PRLx,0);
%     PRL_Y = round(size(avgAreaPerCone,1) - PRLy,0);
% elseif exist('PRL_all','var') == 1
%     PRL_X = round(PRL_all(1),0);
%     PRL_Y = round(size(avgAreaPerCone,1) - PRL_all(2),0);
% else
%     PRL_X = round(PRL_x,0);
%     PRL_Y = round(size(avgAreaPerCone,1) - PRL_y,0);
% end

%trim the edges of the matrix
hozrPRL_col = CD_conePerDegsq(PRL_Y- 5: PRL_Y + 5,:);
vertPRL_row = CD_conePerDegsq(:,PRL_X- 5 : PRL_X + 5)';
%convert the pixels to degrees (base off image)
pixX_PRLtrimed = [1:length(vertPRL_row)];
XaxisPRL_DEG = pixX_PRLtrimed/pixPerdeg;
pixY_PRLtrimed = [1:length(hozrPRL_col)];
YaxisPRL_DEG = pixY_PRLtrimed/pixPerdeg;

normPRL_Xaxis_deg = XaxisPRL_DEG - (PRL_Y/pixPerdeg);
normPRL_Yaxis_deg = YaxisPRL_DEG - (PRL_X/pixPerdeg);


%Plot Cone density around peak cone density
figure;
plot(normPRL_Yaxis_deg, mean(hozrPRL_col), 'r'); %horizontal meridian
hold on;
plot(normPRL_Xaxis_deg,mean(vertPRL_row), 'g'); %vertical meridian
title('Cone Density centered around the PRL')
xlabel('Degrees Eccentricity')
ylabel('Cone Density (cones/degree^2)')
ylim([6000, 18000])
legend('horizontal', 'vertical')

% 
% %%% HVA analysis
% 
% %remove inf and nan
% trimCD_CpDeg = CD_conePerDegsq;
% trimCD_CpDeg(isinf( trimCD_CpDeg)) = 0;
% trimCD_CpDeg(isnan( trimCD_CpDeg)) = 0;
% 
% %plot cone density across eccentricity
% figure;
% plot(trimCD_CpDeg);% plot the data per pixel.
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %%% PCD
% %trim the edges of the matrix
% hozrPCD_col = trimCD_CpDeg(oursPCD_x - 5: oursPCD_x + 5,:);
% vertPCD_row = trimCD_CpDeg(:,oursPCD_y-5:oursPCD_y + 5)';
% %convert the pixels to degrees (base off image)
% pixX_PCDtrimed = [1:length(vertPCD_row)];
% XaxisPCD_DEG = pixX_PCDtrimed/pixPerdeg;
% pixY_PCDtrimed = [1:length(hozrPCD_col)];
% YaxisPCD_DEG = pixY_PCDtrimed/pixPerdeg;
% normPCD_Xaxis_deg = XaxisPCD_DEG - (oursPCD_x/pixPerdeg);
% normPCD_Yaxis_deg = YaxisPCD_DEG - (oursPCD_y /pixPerdeg);
% 
% 
% %Plot Cone density around peak cone density
% figure;
% plot(normPCD_Yaxis_deg,mean(hozrPCD_col), 'r');%horizontal meridian
% hold on;
% plot(normPCD_Xaxis_deg, mean(vertPCD_row), 'g'); %vertical meridian
% title('Cone Density centered around PCD')
% xlabel('Degrees Eccentricity')
% ylabel('Cone Density (cones/degree^2)')
% ylim([6000, 18000])
% legend('horizontal', 'vertical')
% 
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % %%% CDC
% 
% %trim the edges of the matrix
% hozrCDC_col = trimCD_CpDeg(oursCDC_x - 5: oursCDC_x + 5,:);
% vertCDC_row = trimCD_CpDeg(:,oursCDC_y-5:oursCDC_y + 5)';
% %convert the pixels to degrees (base off image)
% pixX_CDCtrimed = [1:length(vertCDC_row)];
% XaxisCDC_DEG = pixX_CDCtrimed/pixPerdeg;
% pixY_CDCtrimed = [1:length(hozrCDC_col)];
% YaxisCDC_DEG = pixY_CDCtrimed/pixPerdeg;
% normCDC_Xaxis_deg = XaxisCDC_DEG - (oursCDC_x/pixPerdeg);
% normCDC_Yaxis_deg = YaxisCDC_DEG - (oursCDC_y /pixPerdeg);
% 
% 
% %Plot Cone density around peak cone density
% figure;
% plot(normCDC_Yaxis_deg, mean(hozrCDC_col), 'r'); %horizontal meridian
% hold on;
% plot(normCDC_Xaxis_deg,mean(vertCDC_row), 'g'); %vertical meridian
% title('Cone Density centered around the CDC')
% xlabel('Degrees Eccentricity')
% ylabel('Cone Density (cones/degree^2)')
% ylim([6000, 18000])
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %%% PRL
% 
% %trim the edges of the matrix
% hozrPRL_col = trimCD_CpDeg(PRL_X- 5: PRL_X + 5,:);
% vertPRL_row = trimCD_CpDeg(:,PRL_Y- 5 : PRL_Y + 5)';
% %convert the pixels to degrees (base off image)
% pixX_PRLtrimed = [1:length(vertPRL_row)];
% XaxisPRL_DEG = pixX_PRLtrimed/pixPerdeg;
% pixY_PRLtrimed = [1:length(hozrPRL_col)];
% YaxisPRL_DEG = pixY_PRLtrimed/pixPerdeg;
% 
% normPRL_Xaxis_deg = XaxisPRL_DEG - (PRL_X/pixPerdeg);
% normPRL_Yaxis_deg = YaxisPRL_DEG - (PRL_Y/pixPerdeg);
% 
% 
% %Plot Cone density around peak cone density
% figure;
% plot(normPRL_Yaxis_deg, mean(hozrPRL_col), 'r'); %horizontal meridian
% hold on;
% plot(normPRL_Xaxis_deg,mean(vertPRL_row), 'g'); %vertical meridian
% title('Cone Density centered around the PRL')
% xlabel('Degrees Eccentricity')
% ylabel('Cone Density (cones/degree^2)')
% ylim([6000, 18000])
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% 


%% get Cone density at 20 acrmin with an area of 2 arc min


Gaze = 1;

Z055_Adist = indvAdist_F(4,:) * (pixPerdeg/60);
Z151_Adist = indvAdist_F(2,:) * (pixPerdeg/60);

Z055_CI = CIdist_F(4,:) * (pixPerdeg/60);
Z151_CI = CIdist_F(2,:) * (pixPerdeg/60);

Z055_Adist = [Z055_Adist(5) Z055_Adist(1) Z055_Adist(3) Z055_Adist(7)];
Z151_Adist = [Z151_Adist(5) Z151_Adist(1) Z151_Adist(3) Z151_Adist(7)];

Z055_CI = [Z055_CI(5) Z055_CI(1) Z055_CI(3) Z055_CI(7)];
Z151_CI = [Z151_CI(5) Z151_CI(1) Z151_CI(3) Z151_CI(7)];

% variables
targOffset = 0.33; % eccentricity from PRL in deg
area = 0.033; % how big you want to take the area (square)

ecc = round(targOffset * pixPerdeg,0); %eccentrictiy in pixels
boxLoc = round(area * pixPerdeg,0); % box area in pixels

boxdiag = sqrt(((boxLoc^2) + (boxLoc^2)));

for cc = 1:2 % prl then CDC
    center_X = [round(PRL_X,0) oursCDC_x];
    center_Y = [round(PRL_Y,0) oursCDC_y];
    rCenter_X = center_X(cc);
    rCenter_Y =center_Y(cc);
    
    SuperiorCenter_Loc = rCenter_Y - ecc;
    InferiorCenter_Loc=  rCenter_Y + ecc;
    NasalCenter_Loc =  rCenter_X + ecc;
    TempCenter_Loc = rCenter_X - ecc;
    
    
    if Gaze
        %location based on eyes
        if strcmp(subject, 'Z055')
            SuperiorCenter = rCenter_Y - Z055_Adist(1);
            InferiorCenter=  rCenter_Y + Z055_Adist(2);
            NasalCenter =  rCenter_X + Z055_Adist(3);
            TempCenter = rCenter_X - Z055_Adist(4);
            box = Z055_CI;
        elseif strcmp(subject, 'Z151')
            SuperiorCenter = rCenter_Y - Z151_Adist(1);
            InferiorCenter=  rCenter_Y + Z151_Adist(2);
            NasalCenter =  rCenter_X + Z151_Adist(3);
            TempCenter = rCenter_X - Z151_Adist(4);
            box = Z151_CI;
        end
    else
        %location of boxes
        SuperiorCenter = rCenter_Y - ecc;
        InferiorCenter=  rCenter_Y + ecc;
        NasalCenter =  rCenter_X + ecc;
        TempCenter = rCenter_X - ecc;
        
        
    end
    
    
    % %print out the schemeatic for the AO orientation
    % OrientSchem = imread('C:\Users\ruccilab\Documents\adaptiveoptics\ConeDesity\APLab_System\data\orientationOfVideoFrames_2022-06-22.png');
    % figure;
    % imshow(OrientSchem);
    
    if Gaze
        %set up the box locations
        superiorBox = CD_conePerDegsq(SuperiorCenter - box(1):SuperiorCenter + box(1),  rCenter_X - box(1): rCenter_X + box(1));
        superiorArea = mean(mean(superiorBox));
        
        inferiorBox = CD_conePerDegsq(InferiorCenter - box(2): InferiorCenter + box(2),  rCenter_X - box(2): rCenter_X + box(2));
        inferiorArea = mean(mean(inferiorBox));
        
        nasalBox = CD_conePerDegsq(rCenter_Y - box(3): rCenter_Y + box(3), NasalCenter- box(3) : NasalCenter +box(3));
        nasalArea = mean(mean(nasalBox));
        
        temporalBox = CD_conePerDegsq(rCenter_Y - box(4): rCenter_Y + box(4),TempCenter- box(4) : TempCenter + box(4));
        temporalArea = mean(mean(temporalBox));
        
        %black out where the physical boxes were on the monitor.
        viewTargLoc = CD_conePerDegsq;
        viewTargLoc(SuperiorCenter_Loc - boxLoc: SuperiorCenter_Loc + boxLoc, rCenter_X - boxLoc: rCenter_X + boxLoc)=0;
        viewTargLoc(InferiorCenter_Loc - boxLoc: InferiorCenter_Loc + boxLoc,  rCenter_X - boxLoc: rCenter_X + boxLoc)=0;
%         viewTargLoc(rCenter_Y - boxLoc: rCenter_Y + boxLoc, NasalCenter_Loc- boxLoc : NasalCenter_Loc +boxLoc)=0;
%         viewTargLoc(rCenter_Y - boxLoc: rCenter_Y + boxLoc,TempCenter_Loc- boxLoc : TempCenter_Loc + boxLoc)=0;
        
        %             %black out where the box locations will be
        %             viewTargLoc = CD_conePerDegsq;
        %             viewTargLoc(SuperiorCenter - box(1): SuperiorCenter + box(1), rCenter_X - box(1): rCenter_X + box(1))=0;
        %             viewTargLoc(InferiorCenter - box(2): InferiorCenter + box(2),  rCenter_X - box(2): rCenter_X + box(2))=0;
        %             viewTargLoc(rCenter_Y - box(3): rCenter_Y + box(3), NasalCenter- box(3) : NasalCenter +box(3))=0;
        %             viewTargLoc(rCenter_Y - box(4): rCenter_Y + box(4),TempCenter- box(4) : TempCenter + box(4))=0;
        %
        
        
    else
        %set up the box locations
        superiorBox = CD_conePerDegsq(SuperiorCenter - boxLoc:SuperiorCenter + boxLoc,  rCenter_X - boxLoc: rCenter_X + boxLoc);
        superiorArea = mean(mean(superiorBox));
        
        inferiorBox = CD_conePerDegsq(InferiorCenter - boxLoc: InferiorCenter + boxLoc,  rCenter_X - boxLoc: rCenter_X + boxLoc);
        inferiorArea = mean(mean(inferiorBox));
        
        nasalBox = CD_conePerDegsq(rCenter_Y - boxLoc: rCenter_Y + boxLoc, NasalCenter- boxLoc : NasalCenter +boxLoc);
        nasalArea = mean(mean(nasalBox));
        
        temporalBox = CD_conePerDegsq(rCenter_Y - boxLoc: rCenter_Y + boxLoc,TempCenter- boxLoc : TempCenter + boxLoc);
        temporalArea = mean(mean(temporalBox));
        
        %black out where the box locations will be
        viewTargLoc = CD_conePerDegsq;
        viewTargLoc(SuperiorCenter - boxLoc: SuperiorCenter + boxLoc, rCenter_X - boxLoc: rCenter_X + boxLoc)=0;
        viewTargLoc(InferiorCenter - boxLoc: InferiorCenter + boxLoc,  rCenter_X - boxLoc: rCenter_X + boxLoc)=0;
%         viewTargLoc(rCenter_Y - boxLoc: rCenter_Y + boxLoc, NasalCenter- boxLoc : NasalCenter +boxLoc)=0;
%         viewTargLoc(rCenter_Y - boxLoc: rCenter_Y + boxLoc,TempCenter- boxLoc : TempCenter + boxLoc)=0;
%         
    end
    
    % black out the target locations on the heatmap to verify their location.
    figure;
    axc = gca;
    imagesc(viewTargLoc); hold on;
    a=plot(PRL_X,PRL_Y, 'g.', 'MarkerSize', 25);hold on;
%     b=plot(oursPCD_x,oursPCD_y, 'r^');hold on;
    c=plot(oursCDC_x,oursCDC_y, 'b*');hold on;
   legend([a,c],'Preferred Retinal Locus','Cone Density Centroid')

    
    if strcmp(subject, 'Z055')
        hold on;
        viscircles([rCenter_X,SuperiorCenter],Z055_CI(1) + (boxdiag/2));hold on;%superior
        viscircles([rCenter_X,InferiorCenter],Z055_CI(2) + (boxdiag/2));hold on;%inferior
%         viscircles([NasalCenter,rCenter_Y],Z055_CI(3) + (boxdiag/2));hold on;%nasal
%         viscircles([TempCenter,rCenter_Y],Z055_CI(4) + (boxdiag/2));hold on;%temporal
        
        xlim([50 550]);
        ylim([50 550]);
        text((size(CD_conePerDegsq,1)/2)-80,80,'Superior Retina')
        text((size(CD_conePerDegsq,1)/2)-60,size(CD_conePerDegsq,2)-80,'Inferior Retina Retina')
        
        text(60,(size(CD_conePerDegsq,1)/2)-30,'Temporal Retina')
        text(size(CD_conePerDegsq,1)-150,(size(CD_conePerDegsq,1)/2)-30,'Nasal Retina')
        
        ws = (10/60)*pixPerdeg;    % 10 arcmin
        szI = size(CD_conePerDegsq);
        hs = 20;
        px = (rCenter_X - 230)+20;                % position in x
        py =(rCenter_Y + 225) - 20 - hs;   % position in y
        r = rectangle('Position',[px,py,ws,hs]);
        r.FaceColor = [0 0 0];
        t = text(px,py-1.5*hs,'10 arcmin');
        t.Color = [0 0 0];
        axc.CLim = [0 13000];
        colorbar
        axis off
        
    elseif strcmp(subject, 'Z151')
        hold on;
        viscircles([rCenter_X,SuperiorCenter],Z151_CI(1) + (boxdiag/2));hold on;%superior
        viscircles([rCenter_X,InferiorCenter],Z151_CI(2) + (boxdiag/2));hold on;%inferior
%         viscircles([NasalCenter,rCenter_Y],Z151_CI(3) + (boxdiag/2));hold on;%nasal
%         viscircles([TempCenter,rCenter_Y],Z151_CI(4) + (boxdiag/2));hold on;%temporal
        
        
        xlim([50 550]);
        ylim([50 550]);
        text((size(CD_conePerDegsq,1)/2)-80,80,'Superior Retina')
        text((size(CD_conePerDegsq,1)/2)-60,size(CD_conePerDegsq,2)-80,'Inferior Retina Retina')
        
        text(60,(size(CD_conePerDegsq,1)/2)-10,'Temporal Retina')
        text(size(CD_conePerDegsq,1)-150,(size(CD_conePerDegsq,1)/2)-10,'Nasal Retina')
        
        ws = (10/60)*pixPerdeg;    % 10 arcmin
        szI = size(CD_conePerDegsq);
        hs = 20;
        px = (rCenter_X - 230)+20;                % position in x
        py =(rCenter_Y + 225) - 20 - hs;   % position in y
        r = rectangle('Position',[px,py,ws,hs]);
        r.FaceColor = [0 0 0];
        t = text(px,py-1.5*hs,'10 arcmin');
        t.Color = [0 0 0];
        axc.CLim = [0 13000];
        colorbar
        axis off
        
    end
    
    supArea(cc) = superiorArea;
    infArea(cc) = inferiorArea;
    nasArea(cc) = nasalArea;
    temArea(cc) = temporalArea;
end

% Load in performance

trial_types = {'neutral_1', 'neutral_2', ...
    'neutral_3', 'neutral_4', 'neutral_5', 'neutral_6', ...
    'neutral_7', 'neutral_8'};
for trial_idx = 1:length(trial_types)
    cur_trial = trial_types{trial_idx};
    avg_perf(trial_idx) = sum(ft.(cur_trial).perf)/length(ft.(cur_trial).perf);
    
end

FovHVAPerf = (FvP.data(1).perf_HVA(subID,:));
% FovHVACI_XC = (FvP.data(1).HVA_stePerf(subID,:))*CI95_F(2);

rAreas_PRL = [supArea(1)  infArea(1)  nasArea(1) temArea(1)];
n_rAreas_PRL = [supArea(1)  infArea(1)  nasArea(1) temArea(1)]/ mean(mean(CD_conePerDegsq));

rAreas_CDC = [supArea(2)  infArea(2)  nasArea(2) temArea(2)];
n_rAreas_CDC = [supArea(2)  infArea(2)  nasArea(2) temArea(2)]/ mean(mean(CD_conePerDegsq));

avgHM = mean([rAreas_PRL(:,3) rAreas_PRL(:,4)],2);
avgVM = mean([rAreas_PRL(:,1) rAreas_PRL(:,2)],2);
avgHM_CDC = mean([rAreas_CDC(:,3) rAreas_CDC(:,4)],2);
avgVM_CDC = mean([rAreas_CDC(:,1) rAreas_CDC(:,2)],2);

% comparing normalized cone density to performance
figure;
axis on
bar([rAreas_PRL(1) avg_perf(5); rAreas_PRL(2) avg_perf(1); rAreas_PRL(3) avg_perf(7); rAreas_PRL(4) avg_perf(3)]); hold on;
xticklabels({'Superior/lowerVF', 'Inferior/UpperVF', 'Nasal/LeftVF', 'Temporal/RightVF'})
ylabel('Normalized Cone Density (divide by mean)')
legend(sprintf('Ecc = %s',targOffset))



figure;
yyaxis left
p1 = plot([1,2], [rAreas_PRL(1) rAreas_PRL(2)],'.-', 'Color', [0 1 0], 'MarkerSize', 18); hold on;
plot([3,4], [rAreas_PRL(3) rAreas_PRL(4)],'.-', 'Color', [0 1 0], 'MarkerSize', 18); hold on;
xticks(1:4)
xlim([.75 4.25])
ylim([6000 10000])
ylabel('Cone density (cones/deg^2)')

yyaxis right
p2 = plot([1,2], [avg_perf(5) avg_perf(1)],'.-','Color',[1 0 1], 'MarkerSize', 18); hold on;
plot([3, 4], [avg_perf(3) avg_perf(7)],'.-','Color',[1 0 1], 'MarkerSize', 18); hold on;
xticks(1:4)
xlim([.75 4.25])
ylim([0.5 1])
ylabel('Proportion Correct)')
xticklabels({'Superior/lowerVF', 'Inferior/UpperVF', 'Nasal','Temporal'})
legend([p1 p2],{'PRL Centered','Performance'})

text(1,0.525,'Vertical Meridian')
text(3,0.525,'Horizontal Meridian')

ax = gca;
ax.YAxis(1).Color =  [0 1 0];
ax.YAxis(2).Color = [1 0 1];




figure;
yyaxis left
p1 = plot([1,2], [rAreas_CDC(1) rAreas_CDC(2)],'.-', 'Color', [0 0 1], 'MarkerSize', 18); hold on;
plot([3,4], [rAreas_CDC(3) rAreas_CDC(4)],'.-', 'Color', [0 0 1], 'MarkerSize', 18); hold on;
xticks(1:4)
xlim([.75 4.25])
ylim([6000 10000])
ylabel('Cone density (cones/deg^2)')

yyaxis right
p2 = plot([1,2], [avg_perf(5) avg_perf(1)],'.-','Color',[1 0 1], 'MarkerSize', 18); hold on;
plot([3, 4], [avg_perf(3) avg_perf(7)],'.-','Color',[1 0 1], 'MarkerSize', 19); hold on;
xticks(1:4)
xlim([.75 4.25])
ylim([0.5 1])
ylabel('Proportion Correct)')
xticklabels({'Superior/lowerVF', 'Inferior/UpperVF', 'Nasal','Temporal'})
legend([p1 p2],{'CDC Centered','Performance'})
text(1,0.525,'Vertical Meridian')
text(3,0.525,'Horizontal Meridian')


ax = gca;
ax.YAxis(1).Color =  [0 0 1];
ax.YAxis(2).Color = [1 0 1];


%%

label = {'PRL','CDC' };
for cc = 1:2
    
    figure;
    if cc == 1
        yyaxis left
        p1 = plot([1,2], [rAreas_PRL(1) rAreas_PRL(2)],'.-', 'Color', [0 1 0]); hold on;
        % errorbar([3,4], avg_reiniger(3:4),CI_XC(3:4),'.-', 'Color', [0 1 0]); hold on;
        xticks(1:2)
        xlim([.75 2.25])
        ylim([8000 11000])
        ylabel('Cone density (cones/deg^2)')
    elseif cc ==2
        yyaxis left
        p1 = plot([1,2], [rAreas_CDC(1) rAreas_CDC(2)],'.-', 'Color', [0 0 1]); hold on;
        % errorbar([3,4], avg_reiniger(3:4),CI_XC(3:4),'.-', 'Color', [0 1 0]); hold on;
        xticks(1:2)
        xlim([.75 2.25])
        ylim([8000 11000])
        ylabel('Cone density (cones/deg^2)')
    end
    
    yyaxis right
    p2 = plot([1,2], [avg_perf(5) avg_perf(1)],'.-','Color',[1 0 1]); hold on;
    % errorbar([3, 4], FovPerf_XC(3:4),CIXC_F(3:4),'.-','Color',[1 0 1]); hold on;
    xticks(1:2)
    xlim([.75 2.25])
    ylim([0.4 .9])
    ylabel('Proportion Correct)')

    xticklabels({'Superior/lowerVF', 'Inferior/UpperVF'})
    legend([p1 p2],{sprintf('%s Centered', label{cc}),'Performance'})
%     text(1,0.85,'Vertical Meridian Anisotropy')
    % text(3,0.525,'Horizontal Meridian')
    ax = gca;
    if cc == 1
    ax.YAxis(1).Color =  [0 1 0];
    else 
       ax.YAxis(1).Color =  [0 0 1]; 
    end
    ax.YAxis(2).Color = [1 0 1];
 
end




label = {'PRL','CDC' };
for cc = 1:2
    
    figure;
    if cc == 1
        yyaxis left
        p1 = plot([1,2], [mean(avgHM) mean(avgVM)],'.-', 'Color', [0 1 0]); hold on;
        % errorbar([3,4], avg_reiniger(3:4),CI_XC(3:4),'.-', 'Color', [0 1 0]); hold on;
        xticks(1:2)
        xlim([.75 2.25])
        ylim([8000 11000])
        ylabel('Cone density (cones/deg^2)')
    elseif cc ==2
        yyaxis left
        p1 = plot([1,2],[mean(avgHM_CDC) mean(avgVM_CDC)],'.-', 'Color', [0 0 1]); hold on;
        % errorbar([3,4], avg_reiniger(3:4),CI_XC(3:4),'.-', 'Color', [0 1 0]); hold on;
        xticks(1:2)
        xlim([.75 2.25])
        ylim([8000 11000])
        ylabel('Cone density (cones/deg^2)')
    end
    
    yyaxis right
    p2 = plot([1,2], FovHVAPerf,'.-','Color',[1 0 1]); hold on;
    % errorbar([3, 4], FovPerf_XC(3:4),CIXC_F(3:4),'.-','Color',[1 0 1]); hold on;
    xticks(1:2)
    xlim([.75 2.25])
    ylim([0.4 .9])
    ylabel('Proportion Correct)')

    xticklabels({'Horizontal Meridian', 'Vertical Meridian'})
    legend([p1 p2],{sprintf('%s Centered', label{cc}),'Performance'})
%     text(1,0.85,'Horizontal Vertical Meridian Asymmetry')
    % text(3,0.525,'Horizontal Meridian')
    ax = gca;
    if cc == 1
    ax.YAxis(1).Color =  [0 1 0];
    else 
       ax.YAxis(1).Color =  [0 0 1]; 
    end
    ax.YAxis(2).Color = [1 0 1];
 
end


%% 















