

%This script takes in cone locations and then caculates the voronoi cell so
%that only one cone is in a given cell. Then the average density will be
%caculated in pixels by
%By SJ March 31, 2022

%Reinger data has a separate script called ProcessReingerData
%C:\Users\ruccilab\Documents\AO\coneDensity\ReingerConeDensity

%%% Reasoning for flipping the cone coordinates.
% when the text file is loaded and then plotted, the orgin of the
% coordinates is (0,0) and matlab will put that at the bottom left by default.
% By rotating them, you get the cone data to be correctly oriented, but in the IV quadrant. 
% If you are just plotting the cones all you would have to do is shift the
% y coordinates up by addition. (brining the orgin to the top left; the cone counting default)

% Just plotting example: 
%   rotCones = (rotatePoint(cone_data(:,1), cone_data(:,2) , -90))'; %rotates coordinates 90 degrees clockwise
%   %flip over the y axis
%   ConeCoord= [rotCones(:,1) -rotCones(:,2) ];

%However, if you want to plot the cones on the image, imshow will have the
%xaxis origin at the bottom left and the y axis orgin at the top left
%just rotating the cone data 90 degrees will take care of the the x
%coordinates, but you must invert the y coordinates over the x axis to get
%proper orientation. 
% Plotting ontop of cone image example 
%   %rotates coordinates 90 degrees clockwise 
%   rotCones = (rotatePoint(cone_data(:,1), cone_data(:,2) , -90))';
%   %shifts the x axis upwards
%   ConeCoord= [rotCones(:,1) rotCones(:,2) + refImage(2) ];



%%
function caculate_coneDensity_OLD(DataFilePath,ImageName, ConeLocName, SaveFigPath)
% roorda -are cone locations from roorda's gui, yes = 1
%run either Reineger data or script that gets the cone coordinates

%load image, and then load the cone locations.
pname_1 = DataFilePath;
fname_1 = ImageName;
image = imread([DataFilePath ImageName]);
refImage = [size(image,1) size(image,2)];
fid = fopen([DataFilePath ConeLocName],'r');%
cone_data = textscan(fid, '%f %f');
cone_data = [cone_data{1} cone_data{2}];

%roate the cone corrdinates -90 degrees (clockwise) see above comments for
%reasoning. %rotating function from SK 6/8/2022
rotCones = (rotatePoint(cone_data(:,1), cone_data(:,2) , -90))'; %rotates coordinates 90 degrees clockwise
%flip over the y axis
ConeCoord= [rotCones(:,1) -rotCones(:,2) ]; %shifts the y back up in to the proper quadrant %+ refImage(2) on the Y

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% plot the cone coordinates on the retinal cone image (before and after rotation)
figure;
coneimage = imread([DataFilePath ImageName]); imagesc(rot90(coneimage,1)); colormap('gray');
imshow(coneimage)% Imshow will plot the figure in correct orientation
hold on;
plot(cone_data(:,1),cone_data(:,2), '.r')
hold on;
plot(ConeCoord(:,1),ConeCoord(:,2), '.g')
axis on
legend(sprintf('%s',ImageName ),'before rotation', 'rotated')

extensions = {'eps','fig','png'};
for k = 1:length(extensions)
    saveas(gcf, sprintf('%s_rotation',[SaveFigPath ImageName(1:end-4)]), extensions{k})
end

%%%
figure;
coneimage = imread([DataFilePath ImageName]); imagesc(rot90(coneimage,1)); colormap('gray');
imshow(coneimage)% Imshow will plot the figure in correct orientation
hold on;
DT = delaunayTriangulation(ConeCoord); %triangulate an area with cones a verticies
[V,r] = voronoiDiagram(DT);%calculates the voronoi regions (3-7 verticies) based off the triangulateion
Xpoints = DT.Points(:,1); %get the cone coordiantes horizontal
Ypoints = DT.Points(:,2);%get the cone coordiantes Vertical

%visualize the delauanyTriangulation on top of the retinal cone image
hold on;
gg = triplot(DT);
hold on ;
voronoi(Xpoints,Ypoints,DT.ConnectivityList);% plot the voronoi cells (cones at center)


%%%
%Another plot to verify that the voronoi cells are on top of the cones.
figure; 
coneimage = imread([DataFilePath ImageName]); imagesc(rot90(coneimage,1)); colormap('gray');
imshow(coneimage)% Imshow will plot the figure in correct orientation
hold on;
voronoi(Xpoints,Ypoints,DT.ConnectivityList);% plot the voronoi cells (cones at center)
legend(sprintf('%s',ImageName ))

extensions = {'eps','fig','png'};
for k = 1:length(extensions)
    saveas(gcf, sprintf('%s_Voronoi',[SaveFigPath ImageName(1:end-4)]), extensions{k})
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[v,c] = voronoin(DT.Points); %list the voronoi cells vertices and its regions


xVorCell = v(:,1);% horizontal cordinates for voronoi cell
yVorCell = v(:,2);% vertical cordinates for voronoi cell



xVorCell_Deg = v(:,1);
yVorCell = v(:,2);
%grabing the points in v from indicies c
%gets the x and y cordinates for each vertice of the voronoi cell
VoroniCoor = cellfun(@(z) [xVorCell(z(:)) yVorCell(z(:))], c(:), 'UniformOutput', false);


%for each pixel get the 150 nearest cone cells and caculate the average for
%each 150 cones and then summ the average up and divide by 150 cells
%the input of this loop should be pixels and then you get pixels squared
%per 150 cones.
if ~isfile(sprintf('avgAreaPerCone_%s.mat',ConeLocName))
    for nPixR = 1:refImage(1)
        for nPixC = 1:refImage(2)
            idx = knnsearch(DT.Points,[nPixR, nPixC] , 'k', 150);
            nn_Coord = c(idx);
            %gets the coordinates for each vertice
            nn_VoroniCoor = cellfun(@(z) [xVorCell(z(:)) yVorCell(z(:))], nn_Coord, 'UniformOutput', false);

            for nn = 1: length(nn_VoroniCoor)
                %caculates the area (pix^2)
                areaVoroniCell(nn) = polyarea(nn_VoroniCoor{nn}(:,1), nn_VoroniCoor{nn}(:,2));
            end
            %sum the areas of 150 cells then divide by the number of cells
            %output units are pixels^2/cones.
            avgAreaPerCone(nPixR,nPixC) = mean(areaVoroniCell);
        end
    end

    %since the voronoi cells are flipped over the horizontal axis 
    % (see the saved image we need to flip the avgAreaPerCone before saving it).

    %avgAreaPerCone = flipud(avgAreaPerCone);

    %takes a long time, so save the data.
    save(sprintf('%savgAreaPerCone_%s.mat',DataFilePath,ConeLocName(1:end-4)),'avgAreaPerCone')
end




