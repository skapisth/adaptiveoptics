




%This script takes in cone locations and then caculates the voronoi cell so
%that only one cone is in a given cell. Then the average density will be
%caculated in pixels by
%By SJ November 11, 2022

%Reinger data has a separate script called ProcessReingerData
%C:\Users\ruccilab\Documents\AO\adaptiveoptics\ConeDesity\ReinigerConeDensity

%at the beginning of this script the cone mosaic image can be flipped if
%the image was taken before AUG3. This script accounts for that and by the
%end of the script,everything that is saved has the superior retina up. 

%%% OUTPUT
% avgAreaPerCone - a matrix the size of the image. In each cell, there is a
% number that corresponds to the amount of pixels one voronoi cell takes up
% (one Voronoi cell is the area of one cone cell that doesn't have any other cone cells near by)
% this output is designed to be turned into a heat map and plotted ontop of
% the cone cells. Since the retinal cone mosaic is an imported image and
% has its origin at the top left, the matrix is left with the inerior
% retina being in the first row, and the superior retina in the last row of
% the matrix (if it is a 500x 600 matrix the superior retina would be in
% row 600).

% CD_conePerDegsq - is the equivalent of avgAreaPerCone, but the units are
% in cones per degree squared. 

%refImage - this is the matlab data file of the loaded cone mosaic that has
%been processed



%%
function caculate_coneDensity(DataFilePath,ImageName,pixPerdeg,AplabData)
warning(['The main output of this function (avgAreaPerCone) is a matrix that is designed to be plotted onto the retinal cone mosaic...' ...
    'If you want to plot just the cone density by itself the data must be fliped up-down (udflip()) or use the other output of this function ' ...
    '(avgAreaPerCone_ForSTAND_ALONE_PLOTTING)'])
% roorda -are cone locations from roorda's gui, yes = 1
%run either Reineger data or script that gets the cone coordinates
Imagefile = [ImageName '.tif'];
%load image, and then load the cone locations.
pname_1 = DataFilePath;
fname_1 = Imagefile;

if AplabData
    coneMosaic = imread([DataFilePath ImageName '/' Imagefile]);
    ConeSize = [size(coneMosaic,1) size(coneMosaic,2)];
    fid = fopen([DataFilePath ImageName '/' ImageName '.txt'],'r');
    cone_data = textscan(fid, '%f %f');
    ConeCoord = [cone_data{2} cone_data{1}]; %NOTE the inversion (see above)
else
    load([DataFilePath ImageName]);
    refImage_OG =refImage;
    coneMosaic = refImage_OG;
    
    ConeCoord = cone_data;
end




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%
figure;
colormap('gray');
imshow(coneMosaic)% Imshow will plot the figure in correct orientation
hold on;

%%% plot the cone coordinates on the retinal cone image (before and after rotation)
figure;
colormap('gray');
imshow(coneMosaic)% Imshow will plot the figure in correct orientation
hold on;
plot(ConeCoord(:,1),ConeCoord(:,2), '.r')


%removing impossible cones based on distance from other cones 
% dist = sqrt((x2 - x1).^2+ (Y2-y1)^2);
% DT_Before = delaunayTriangulation(ConeCoord);
% col1 = DT_Before.Points(DT_Before.ConnectivityList(:,1),:);
% 
% col2 = DT_Before.Points(DT_Before.ConnectivityList(:,2),:);
% 
% col3 = DT_Before.Points(DT_Before.ConnectivityList(:,3),:);
% 
% dist1_pix = sqrt((col2(:,1) - col1(:,1)).^2+ (col2(:,2)-col1(:,2)).^2);
% dist2_pix = sqrt((col3(:,1) - col2(:,1)).^2+ (col3(:,2)-col2(:,2)).^2);
% dist3_pix = sqrt((col3(:,1) - col1(:,1)).^2+ (col3(:,2)-col1(:,2)).^2);
% 
% idxD1 = find(dist1_pix <= 3);
% idxD2 = find(dist2_pix <= 3);
% idxD3 = find(dist3_pix <= 3);
% 
% sDist_idxConn = [idxD1' idxD2'  idxD3'];
% sDist_idxPts = unique(DT_Before.ConnectivityList(sDist_idxConn,:));
% 
% hold on;
% plot(ConeCoord(sDist_idxPts,1),ConeCoord(sDist_idxPts,2), 'g+')
% 
% 
% %adding back one of the points.
% remCones_dist = diff([inf sDist_idxPts']);
% remCone_idx = find(remCones_dist == 1);
% 
% sDist_idxPts(remCone_idx)=[];
% 
% 
% hold on;
% plot(ConeCoord(sDist_idxPts,1),ConeCoord(sDist_idxPts,2), 'xg')
% ConeCoord(sDist_idxPts,:) = [];








extensions = {'eps','fig','png'};
for k = 1:length(extensions)
saveas(gcf, sprintf('%s_ConeTags',[DataFilePath ImageName '/Voronoi_Images/' ImageName]), extensions{k})
end




DT = delaunayTriangulation(ConeCoord); %triangulate an area with cones a verticies
[V,r] = voronoiDiagram(DT);%calculates the voronoi regions (3-7 verticies) based off the triangulateion
Xpoints = DT.Points(:,1); %get the cone coordiantes horizontal
Ypoints = DT.Points(:,2);%get the cone coordiantes Vertical

%visualize the delauanyTriangulation on top of the retinal cone image
hold on;
gg = triplot(DT);
hold on ;
voronoi(Xpoints,Ypoints,DT.ConnectivityList);% plot the voronoi cells (cones at center)


%%%
%Another plot to verify that the voronoi cells are on top of the cones.
figure;
colormap('gray');
imshow(coneMosaic)% Imshow will plot the figure in correct orientation
hold on;
voronoi(Xpoints,Ypoints,DT.ConnectivityList);% plot the voronoi cells (cones at center)
legend(sprintf('%s',Imagefile ))

extensions = {'eps','fig','png'};
for k = 1:length(extensions)
saveas(gcf, sprintf('%s_Voronoi',[DataFilePath ImageName '/Voronoi_Images/' ImageName]), extensions{k})
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%http://www.qhull.org/download/  ==>Just use a reference
[v,c] = voronoin(DT.Points); %list the voronoi cells vertices and its regions


xVorCell = v(:,1);% horizontal cordinates for voronoi cell
yVorCell = v(:,2);% vertical cordinates for voronoi cell

if ~AplabData
    CC_out = find(ConeCoord > 712);%get the indexes of cones corrdinates that are greater than 712
    ConeCoord(CC_out,:) = [];
    
    minCC = floor(min(ConeCoord));
    if minCC< 0
        minCC = 0;
    end
    maxCC = round(max(ConeCoord),0);
    if maxCC >712
        maxCC  = 712;
    end
    
    ConeSize = refImage_OG(minCC(1):maxCC(1), minCC(2):maxCC(2));
    
    ConeSize = [size(ConeSize,1), size(ConeSize,2)];
end


%
%
% xVorCell_Deg = v(:,1);
% yVorCell = v(:,2);
% %grabing the points in v from indicies c
% %gets the x and y cordinates for each vertice of the voronoi cell
% VoroniCoor = cellfun(@(z) [xVorCell(z(:)) yVorCell(z(:))], c(:), 'UniformOutput', false);


%for each pixel get the 150 nearest cone cells and caculate the average for
%each 150 cones and then summ the average up and divide by 150 cells
%the input of this loop should be pixels and then you get pixels squared
%per 150 cones.

for nPixR = 1:ConeSize(1)
    for nPixC = 1:ConeSize(2)
        %DT.points puts the collum of image as x in the first row, and row in the second.
        idx = knnsearch(DT.Points,[nPixC,nPixR] , 'k', 150);
        nn_Coord = c(idx);
        %gets the coordinates for each vertice
        nn_VoroniCoor = cellfun(@(z) [xVorCell(z(:)) yVorCell(z(:))], nn_Coord, 'UniformOutput', false);
        
        for nn = 1: length(nn_VoroniCoor)
            %caculates the area (pix^2)
            areaVoroniCell(nn) = polyarea(nn_VoroniCoor{nn}(:,1), nn_VoroniCoor{nn}(:,2));
        end
        %sum the areas of 150 cells then divide by the number of cells
        %output units are pixels^2/cones.
        avgAreaPerCone(nPixR,nPixC) = nanmean(areaVoroniCell);
        sprintf('(%i, %i)',nPixR,nPixC)
    end
    
    
    
    
    
    %takes a long time, so save the dataas you go along.
    
    
    if AplabData
        
        save(sprintf('%s%s/avgAreaPerCone_temp_%s.mat',DataFilePath,ImageName,ImageName),'avgAreaPerCone')
    else
        
        %reiniger
        save(sprintf('%s/AvgConesPerPix/avgAreaPerCone_temp_%s',DataFilePath(1:end-15),ImageName),'avgAreaPerCone')
    end
    
end

avgAreaPerCone_flipUD = flipud(avgAreaPerCone);

imageFlipDate = datetime(2022,08,03); %changed hardware in AO system so Superior retina up.
%     raiseTableDate = datetime(2022,08,20); %raised table and changed 940nm collumated to 680nm.
sessDate = datetime(str2double(ImageName(end -9:end-6)),str2double(ImageName(end -4:end-3)),str2double(ImageName(end -1:end)));


if sessDate>imageFlipDate
    if sessDate>datetime(2023,02,01)
        PRL_Y = PRL_all(2);
    else
        PRL_Y = round(size(CD_conePerDegsq,1) - PRLy,0)-1;
    end
else
    PRL_Y = round(PRLy,0);

end



if AplabData
    
    if sessDate<=imageFlipDate
        Superior_MaxY = avgAreaPerCone; %superior retina at bottom of image, therefore superior retina is at the maximum Y value
        Inferior_MaxY = avgAreaPerCone_flipUD;% flip so,superior retina at top of image, therefore inferior retina is at the maximum Y value
        
        avgAreaPerCone = Inferior_MaxY;
        supRet_topImage = avgAreaPerCone;
        refImage = flipud(coneMosaic);

        ConeCoord(:,2) = (size(CD_conePerDegsq,1) - cone_data{1}) -1;
        
    else
        Inferior_MaxY = avgAreaPerCone; %superior retina at bottom of image, therefore superior retina is at the maximum Y value
        Superior_MaxY = avgAreaPerCone_flipUD;% flip so,superior retina at top of image, therefore inferior retina is at the maximum Y value
        
        avgAreaPerCone = Inferior_MaxY;
        supRet_topImage = avgAreaPerCone;
        refImage = coneMosaic;


    end
    
    
end

%%% Get avgerage area per cone and convert the pixels to degrees.
CD_conePerPixsq = 1./supRet_topImage;
CD_conePerDegsq= CD_conePerPixsq * (pixPerdeg).^2;
CD_conePerDegsq(isinf(CD_conePerDegsq)) = 0;




figure;
colormap('gray');
imshow(refImage);hold on;

plot(ConeCoord(:,1),ConeCoord(:,2),'r.')

if AplabData
    save(sprintf('%s%s/avgAreaPerCone_%s.mat',DataFilePath,ImageName,ImageName),'avgAreaPerCone', 'CD_conePerDegsq','refImage','ConeCoord')
else
    save(sprintf('%s/AvgConesPerPix/avgAreaPerCone_temp_%s',DataFilePath(1:end-15),ImageName),'avgAreaPerCone', 'CD_conePerDegsq','refImage','ConeCoord')
end





