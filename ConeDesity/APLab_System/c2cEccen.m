

%This script was created by SJ - 11/18/2022 
% The goal of this script is to get the center to center cone spacing of
% a given region of cones. 

% this script requries the cone coordinates and the PRL with a given image.



%% Load the files 

RootPath = 'C:/Users/ruccilab/Documents/adaptiveoptics/ConeDesity/APLab_System/data/';
% DataFileName    = 'A188_R_2022_10_17';
% DataFileName    = 'Z024_R_2022_07_28';
DataFileName    = 'Z151_R_2022_07_29';

%loading the summary file (you technically only need the PRL and the image from this file)
load(sprintf('%sXC/%s_CD_data.mat',RootPath,DataFileName))

% % open up the cone coordinates
% fid = fopen([RootPath DataFileName '/' DataFileName '.txt'],'r');
% cone_data = textscan(fid, '%f %f');
% ConeCoord = [cone_data{2} cone_data{1}];% this is not a mistake; the cone tagging software puts the X coordinates in the 2nd collumn; 



%% Define/find the ROI
%%% parameters
eccenX = 0;%arcmin
eccenY = 0;%arcmin
radius = 5; %2.5;%arcmin

%calculate the center of the region you want
Xcenter = PRL_X + (eccenX/60)*pixPerdeg;
Ycenter = PRL_Y + (eccenY/60)*pixPerdeg;

box = (radius/60)*pixPerdeg;
minXbox = Xcenter-box;
maxXbox = Xcenter+box;
minYbox = Ycenter-box;
maxYbox = Ycenter+box;

X_Coords = ConeCoord(:,1);
Y_Coords = ConeCoord(:,2);

% find index that is within that region
xROI_idx = intersect(find(X_Coords> minXbox),find(X_Coords<=maxXbox));
yROI_idx = intersect(find(Y_Coords> minYbox),find(Y_Coords<=maxYbox));

ROI_idx = intersect(xROI_idx,yROI_idx);

ROI_ConeCoord = [ConeCoord(ROI_idx,1) ConeCoord(ROI_idx,2)];

%% plotting full image cones and sampled image cones

%sanity check
figure;
colormap('gray');
imshow(refImage)% Imshow will plot the figure in correct orientation

hold on;
plot(ConeCoord(ROI_idx,1),ConeCoord(ROI_idx,2), '+c')
hold on;
plot(ConeCoord(:,1),ConeCoord(:,2), '.r')




%% Calc center to center cone spacing 

DT = delaunayTriangulation(ROI_ConeCoord);
figure;
gg = triplot(DT);

% dist = sqrt((x2 - x1).^2+ (Y2-y1)^2);
col1 = DT.Points(DT.ConnectivityList(:,1),:);

col2 = DT.Points(DT.ConnectivityList(:,2),:);

col3 = DT.Points(DT.ConnectivityList(:,3),:);

dist1_pix = sqrt((col2(:,1) - col1(:,1)).^2+ (col2(:,2)-col1(:,2)).^2);
dist2_pix = sqrt((col3(:,1) - col2(:,1)).^2+ (col3(:,2)-col2(:,2)).^2);
dist3_pix = sqrt((col3(:,1) - col1(:,1)).^2+ (col3(:,2)-col1(:,2)).^2);

dist1_arc = (dist1_pix/pixPerdeg)*60;
dist2_arc = (dist2_pix/pixPerdeg)*60;
dist3_arc = (dist3_pix/pixPerdeg)*60;

%as a sanity check... count the number of pixels between the red dots in the previous ROI image. 
avgDist_c2c_pix = mean([dist1_pix; dist2_pix]);
avgDist_c2c_pix = mean([dist1_pix; dist2_pix; dist3_pix]);

nCones = length([dist1_pix; dist2_pix]);

avgDist_c2c_arc = mean([dist1_arc; dist2_arc]); %acrmin
avgDist_c2c_arc = mean([dist1_arc; dist2_arc; dist3_arc]); %acrmin

fprintf('Average Center to center spacing = %d pixels; \n',avgDist_c2c_pix)
fprintf('Average Center to center spacing = %d arcmin; \n',avgDist_c2c_arc)
fprintf('Number of Cones Stimulated = %d ; \n',nCones)




