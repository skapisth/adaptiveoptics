function ellipseXY(allX, allY, p, clr, sze)


% Compute CI using percentile method
CIx = prctile(allX, [(100-p)/2, p+(100-p)/2]); % x CI [left, right]
CIy = prctile(allY, [(100-p)/2, p+(100-p)/2]); % y CI [lower, upper]
CIrng(1) = CIx(2)-CIx(1); % CI range (x)
CIrng(2) = CIy(2)-CIy(1); % CI range (y)

meanX = mean(allX);
meanY = mean(allY);

% Draw ellipses
% llc = [CIx(1), CIy(1)]; % (x,y) lower left corners
llc = ([(-CIrng(1)/2)+meanX (-CIrng(2)/2)+meanY]);
if sze == 0
    rectangle('Position',[llc,CIrng],'Curvature',[1,1], 'EdgeColor', clr(1,:));
else
    rectangle('Position',[llc,CIrng],'Curvature',[1,1], 'EdgeColor', clr(1,:),...
        'LineWidth',sze);
end
end